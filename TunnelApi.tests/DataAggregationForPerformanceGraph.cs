﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TunnelAPI.repository.tunnel;
using TunelAPI.repository;
using TunnelAPI.repository.project;
using TunnelAPI.services.services.tunnel.aggregator;
using TunnelAPI.services.services.tunnel.aggregator.export;
using AutoMapper;
using TunnelAPI.services.mapper;
using TunnelAPI.services.services.tunnel;
using TunnelAPI.services.model;
using TunnelAPI.services.services.project;

namespace TunnelApi.tests
{
    [TestClass]
    public class DataAggregationForPerformanceGraph
    {
        private Dictionary<decimal, DateTime> expectedOfferOfferResults;
        private PerformanceOfferOfferAggregator performanceOfferOfferAggregator;

        private Dictionary<decimal, DateTime> expectedRealOfferResults;
        private PerformanceRealOfferAggregator performanceRealOfferAggregator;

        private Dictionary<int, decimal> expectedRealPerformanceResults;
        private RealPerformance realPerformance;

        private Dictionary<decimal, DateTime> expectedRealRealResults;
        private PerformanceRealRealAggregator performanceRealRealAggregator;

        [TestInitialize]
        public void TestInitialize() {
            InitializeDatesPerPk();

            TunnelArtsEntities entities = CreateEntities();
            IMapper mapper = CreateMapperInstance();
            Project project = InitializeProject(entities, mapper);
            Tunnel tunnel = InitializeTunnel(entities, mapper);
            CreatePerformanceOfferOfferAggregator(project, tunnel);

            ICollection<Mapping> mappings = InitializeMappings(entities, tunnel);
            CreatePerformanceRealOfferAggregator(project, mappings);
            CreateRealPerformance(project, mappings);
            CreatePerformanceRealRealAggregator(mappings);
        }

        private void InitializeDatesPerPk()
        {
            InitializeExpectedOfferOfferResults();
            InitializeExpectedRealOfferResults();
            InitializeRealPerformanceResults();
            InitializeExpectedRealRealResults();
        }

        private void InitializeExpectedOfferOfferResults()
        {
            expectedOfferOfferResults = new Dictionary<decimal, DateTime>() {
                { 0.3m, new DateTime(2016, 11, 14, 23, 00, 00, 000) }, { 3.3m, new DateTime(2016, 11, 17, 23, 00, 00, 000) },
                { 6.3m, new DateTime(2016, 11, 18, 13, 24, 00, 000) }, { 9.3m, new DateTime(2016, 11, 20, 01, 24, 00, 000) },
                { 12.3m, new DateTime(2016, 11, 21, 01, 24, 00, 000) }, { 15.3m, new DateTime(2016, 11, 22, 16, 45, 36, 000) },
                { 23.5m, new DateTime(2016, 11, 23, 04, 45, 36, 000) }, { 26.5m, new DateTime(2016, 11, 23, 22, 45, 36, 000) },
                { 29.5m, new DateTime(2016, 11, 26, 22, 45, 36, 000) }, { 32.5m, new DateTime(2016, 11, 27, 05, 57, 36, 000) },
                { 34.0m, new DateTime(2016, 11, 29, 22, 45, 36, 000) }, { 39.4m, new DateTime(2016, 12, 01, 10, 45, 36, 000) },
                { 42.4m, new DateTime(2016, 12, 02, 10, 45, 36, 000) }, { 45.4m, new DateTime(2016, 12, 02, 22, 45, 36, 000) },
                { 48.4m, new DateTime(2016, 12, 03, 16, 09, 36, 000) }, { 51.3m, new DateTime(2016, 12, 06, 16, 09, 36, 000) },
                { 54.3m, new DateTime(2016, 12, 06, 21, 21, 36, 000) }, { 55.6m, new DateTime(2016, 12, 07, 11, 45, 36, 000) },
                { 58.6m, new DateTime(2016, 12, 08, 22, 33, 36, 000) }, { 61.5m, new DateTime(2016, 12, 09, 21, 45, 36, 000) },
                { 64.4m, new DateTime(2016, 12, 10, 09, 21, 36, 000) }, { 67.3m, new DateTime(2016, 12, 11, 04, 33, 36, 000) },
                { 70.5m, new DateTime(2016, 12, 11, 16, 33, 36, 000) }, { 73.5m, new DateTime(2016, 12, 12, 09, 57, 36, 000) },
                { 76.4m, new DateTime(2016, 12, 15, 09, 57, 36, 000) }, { 79.4m, new DateTime(2016, 12, 16, 00, 21, 36, 000) },
                { 82.4m, new DateTime(2016, 12, 17, 12, 21, 36, 000) }, { 85.4m, new DateTime(2016, 12, 18, 05, 09, 36, 000) },
                { 86.1m, new DateTime(2016, 12, 19, 05, 09, 36, 000) }, { 89.1m, new DateTime(2016, 12, 19, 16, 45, 36, 000) },
                { 92.0m, new DateTime(2016, 12, 20, 10, 09, 36, 000) }, { 94.9m, new DateTime(2016, 12, 23, 07, 45, 36, 000) },
                { 97.8m, new DateTime(2016, 12, 26, 07, 45, 36, 000) }
            };
        }

        private void InitializeExpectedRealOfferResults()
        {
            expectedRealOfferResults = new Dictionary<decimal, DateTime>() {
                { 1.0m, new DateTime(2016, 11, 14, 18, 48, 00, 000) }, { 3.3m, new DateTime(2016, 11, 15, 18, 48, 00, 000) },
                { 6.3m, new DateTime(2016, 11, 17, 06, 48, 00, 000) }, { 9.3m, new DateTime(2016, 11, 18, 06, 48, 00, 000) },
                { 12.3m, new DateTime(2016, 11, 19, 06, 48, 00, 000) }, { 15.3m, new DateTime(2016, 11, 22, 00, 24, 00, 000) },
                { 23.5m, new DateTime(2016, 11, 23, 00, 24, 00, 000) }, { 26.5m, new DateTime(2016, 11, 24, 00, 24, 00, 000) },
                { 29.5m, new DateTime(2016, 11, 24, 14, 48, 00, 000) }, { 32.5m, new DateTime(2016, 11, 25, 02, 48, 00, 000) },
                { 34.0m, new DateTime(2016, 11, 26, 22, 00, 00, 000) }, { 39.4m, new DateTime(2016, 11, 27, 16, 00, 00, 000) },
                { 42.4m, new DateTime(2016, 11, 28, 10, 00, 00, 000) }, { 45.4m, new DateTime(2016, 12, 01, 10, 00, 00, 000) },
                { 48.4m, new DateTime(2016, 12, 02, 09, 12, 00, 000) }, { 51.3m, new DateTime(2016, 12, 03, 03, 12, 00, 000) },
                { 54.3m, new DateTime(2016, 12, 03, 11, 00, 00, 000) }, { 55.6m, new DateTime(2016, 12, 04, 05, 00, 00, 000) },
                { 58.6m, new DateTime(2016, 12, 04, 22, 24, 00, 000) }, { 61.5m, new DateTime(2016, 12, 05, 21, 36, 00, 000) },
                { 64.4m, new DateTime(2016, 12, 08, 19, 12, 00, 000) }, { 67.3m, new DateTime(2016, 12, 12, 00, 00, 00, 000) },
                { 70.5m, new DateTime(2016, 12, 13, 00, 00, 00, 000) }, { 73.5m, new DateTime(2016, 12, 15, 21, 36, 00, 000) },
                { 76.4m, new DateTime(2016, 12, 16, 21, 36, 00, 000) }, { 79.4m, new DateTime(2016, 12, 19, 21, 36, 00, 000) },
                { 82.4m, new DateTime(2016, 12, 21, 09, 36, 00, 000) }, { 85.4m, new DateTime(2016, 12, 21, 15, 12, 00, 000) },
                { 86.1m, new DateTime(2016, 12, 24, 15, 12, 00, 000) }, { 89.1m, new DateTime(2016, 12, 25, 14, 24, 00, 000) },
                { 92.0m, new DateTime(2016, 12, 28, 12, 00, 00, 000) }, { 94.9m, new DateTime(2016, 12, 31, 09, 36, 00, 000) },
                { 97.8m, new DateTime(2017, 01, 01, 21, 36, 00, 000) }, { 100.8m, new DateTime(2017, 01, 02, 20, 48, 00, 000) }
            };
        }

        private void InitializeRealPerformanceResults() {
            expectedRealPerformanceResults = new Dictionary<int, decimal>() {
                { 0, 0.0m }, { 1, 1.441441441m }, { 2, 1.896610282m },
                { 3, 3.956802271m }, { 4, 2.531908226m }, { 5, 2.517174514m }
            };
        }

        private void InitializeExpectedRealRealResults()
        {
            expectedRealRealResults = new Dictionary<decimal, DateTime>() {
                { 1.0m, new DateTime(2016, 11, 15, 05, 00, 00, 000) }, { 3.3m, new DateTime(2016, 11, 15, 22, 00, 00, 000) },
                { 6.3m, new DateTime(2016, 11, 16, 14, 30, 00, 000) }, { 9.3m, new DateTime(2016, 11, 17, 16, 30, 00, 000) },
                { 12.3m, new DateTime(2016, 11, 18, 09, 25, 00, 000) }, { 15.3m, new DateTime(2016, 11, 18, 23, 30, 00, 000) },
                { 23.5m, new DateTime(2016, 11, 20, 15, 33, 00, 000) }, { 26.5m, new DateTime(2016, 11, 21, 11, 15, 00, 000) },
                { 29.5m, new DateTime(2016, 11, 23, 13, 12, 00, 000) }, { 32.5m, new DateTime(2016, 11, 23, 18, 16, 00, 000) },
                { 34.0m, new DateTime(2016, 11, 25, 20, 12, 00, 000) }, { 39.4m, new DateTime(2016, 11, 27, 07, 06, 00, 000) },
                { 42.4m, new DateTime(2016, 11, 28, 09, 40, 00, 000) }, { 45.4m, new DateTime(2016, 11, 30, 15, 18, 00, 000) },
                { 48.4m, new DateTime(2016, 12, 01, 10, 20, 00, 000) }, { 51.3m, new DateTime(2016, 12, 02, 12, 50, 00, 000) },
                { 54.3m, new DateTime(2016, 12, 03, 17, 11, 00, 000) }, { 55.6m, new DateTime(2016, 12, 05, 18, 22, 00, 000) },
                { 58.6m, new DateTime(2016, 12, 08, 21, 53, 00, 000) }, { 61.5m, new DateTime(2016, 12, 10, 09, 10, 00, 000) },
                { 64.4m, new DateTime(2016, 12, 11, 16, 20, 00, 000) }, { 67.3m, new DateTime(2016, 12, 12, 10, 15, 00, 000) },
                { 70.5m, new DateTime(2016, 12, 12, 20, 30, 00, 000) }, { 73.5m, new DateTime(2016, 12, 13, 16, 45, 00, 000) },
                { 76.4m, new DateTime(2016, 12, 15, 19, 30, 00, 000) }, { 79.4m, new DateTime(2016, 12, 16, 11, 40, 00, 000) },
                { 82.4m, new DateTime(2016, 12, 18, 12, 17, 00, 000) }, { 85.4m, new DateTime(2016, 12, 19, 14, 32, 00, 000) },
                { 86.1m, new DateTime(2016, 12, 21, 11, 40, 00, 000) }, { 89.1m, new DateTime(2016, 12, 22, 10, 30, 00, 000) },
                { 92.0m, new DateTime(2016, 12, 24, 07, 44, 00, 000) }, { 94.9m, new DateTime(2016, 12, 26, 15, 23, 00, 000) },
                { 97.8m, new DateTime(2016, 12, 28, 08, 30, 00, 000) }, { 100.8m, new DateTime(2016, 12, 30, 18, 20, 00, 000) }
            };
        }

        private TunnelArtsEntities CreateEntities()
        {
            return new TunnelArtsEntities();
        }

        private IMapper CreateMapperInstance()
        {
            var AutoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperConfiguration());
            });
            return AutoMapperConfig.CreateMapper();
        }

        private Project InitializeProject(TunnelArtsEntities entities, IMapper mapper)
        {
            ProjectRepository projectRepository = CreateProjectRepository(entities);
            ProjectService projectService = new ProjectService(mapper, projectRepository);
            Guid projectId = CreateProjectGuid();
            ProjectDTO project = FetchProject(projectService, projectId);
            return mapper.Map<ProjectDTO, Project>(project);
        }

        private ProjectRepository CreateProjectRepository(TunnelArtsEntities entities)
        {
            return new ProjectRepository(entities);
        }

        private Guid CreateProjectGuid()
        {
            return new Guid("2acdfa3c-d336-4e25-9e26-ef70bab289ba");
        }

        private ProjectDTO FetchProject(ProjectService projectService, Guid projectId)
        {
            return projectService.findOne(projectId);
        }

        private Tunnel InitializeTunnel(TunnelArtsEntities entities, IMapper mapper) {
            TunnelRepository tunnelRepository = CreateTunnelRepository(entities);
            TunnelService tunnelService = new TunnelService(mapper, tunnelRepository);
            Guid tunnelId = CreateTunnelGuid();
            TunnelDTO tunnel = FetchTunnel(tunnelService, tunnelId);
            return mapper.Map<TunnelDTO, Tunnel>(tunnel);
        }

        private TunnelRepository CreateTunnelRepository(TunnelArtsEntities entities)
        {
            return new TunnelRepository(entities);
        }

        private Guid CreateTunnelGuid()
        {
            return new Guid("F7DEF4B8-B7A9-4B72-900D-EE602002D967");
        }

        private TunnelDTO FetchTunnel(TunnelService tunnelService, Guid tunnelId) {
            return tunnelService.findOne(tunnelId);
        }

        private void CreatePerformanceOfferOfferAggregator(Project project, Tunnel tunnel) {
            this.performanceOfferOfferAggregator = new PerformanceOfferOfferAggregator(project, tunnel);
        }

        private ICollection<Mapping> InitializeMappings(TunnelArtsEntities entities, Tunnel tunnel) {
            TunnelRepository tunnelRepository = CreateTunnelRepository(entities);
            return tunnelRepository.findMappings(tunnel.id);
        }

        private void CreatePerformanceRealOfferAggregator(Project project, ICollection<Mapping> mappings) {
            performanceRealOfferAggregator = new PerformanceRealOfferAggregator(project, mappings);
        }

        private void CreateRealPerformance(Project project, ICollection<Mapping> mappings) {
            realPerformance = new RealPerformance(project, mappings);
        }

        private void CreatePerformanceRealRealAggregator(ICollection<Mapping> mappings) {
            performanceRealRealAggregator = new PerformanceRealRealAggregator(mappings);
        }

        [TestMethod]
        public void OfferOfferCanAssociatePKAndDateTest() {
            AssertPKAndDatesAreCorrectlyAssociated(expectedOfferOfferResults, performanceOfferOfferAggregator);
        }

        private void AssertPKAndDatesAreCorrectlyAssociated(IDictionary<decimal, DateTime> expectedResults, IPerformanceAggregator performanceAggregator) {
            foreach (KeyValuePair<decimal, DateTime> expectedResult in expectedResults)
            {
                AssertDateOfProgressIsCorrect(expectedResult.Key, expectedResult.Value, performanceAggregator);
            }
        }

        private void AssertDateOfProgressIsCorrect(decimal progress, DateTime expectedTime, IPerformanceAggregator performanceAggregator) {
            Assert.AreEqual(expectedTime, performanceAggregator.getProjectedTimeOfProgress(progress));
        }

        [TestMethod]
        public void RealOfferCanAssociatePKAndDateTest() {
            AssertPKAndDatesAreCorrectlyAssociated(expectedRealOfferResults, performanceRealOfferAggregator);
        }

        [TestMethod]
        public void CanCalculateRealPerformanceTest() {
            foreach (KeyValuePair<int, decimal> expectedResult in expectedRealPerformanceResults) {
                Assert.AreEqual((double)expectedResult.Value, (double)realPerformance.getPerformanceOfCode(expectedResult.Key), 0.00000001);
            }
        }

        [TestMethod]
        public void RealRealCanAssociatePKAndDateTest() {
            AssertPKAndDatesAreCorrectlyAssociated(expectedRealRealResults, performanceRealRealAggregator);
        }

        [TestMethod]
        public void OfferOfferCanExportToJsonTest() {
            AssertCanExportToJson(performanceOfferOfferAggregator);
        }

        private void AssertCanExportToJson(IPerformanceAggregator performanceAggregator) {
            string serializedString = JsonPerformanceExporter.serialize(performanceAggregator);
            Assert.IsTrue(serializedString.Length > 2);
        }

        [TestMethod]
        public void RealOfferCanExportToJsonTest()
        {
            AssertCanExportToJson(performanceRealOfferAggregator);
        }

        [TestMethod]
        public void RealRealCanExportToJsonTest()
        {
            AssertCanExportToJson(performanceRealRealAggregator);
        }
    }
}
