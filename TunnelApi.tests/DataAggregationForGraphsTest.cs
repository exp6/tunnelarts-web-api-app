﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TunnelAPI.repository.tunnel;
using TunnelAPI.repository.project;
using TunnelAPI.repository.mapping;
using TunelAPI.repository;
using TunnelAPI.services.services.tunnel.aggregator;
using TunnelAPI.services.services.tunnel.aggregator.export;
using TunnelAPI.services.model;
using TunnelAPI.services.services.tunnel;
using TunnelAPI.services.mapper;
using AutoMapper;
using TunnelAPI.services.services.project;
using Moq;

namespace TunnelAPI.tests
{
    [TestClass]
    public class DataAggregationForGraphsTest
    {
        private OfferRockTypeAggregator offerRockTypeAggregator;
        private Dictionary<RockQuality, int> offerRockTypeHasAmount;
        private Dictionary<RockQuality, double> offerRockTypeHasPercentage;

        private RealRockTypeAggregator realRockTypeAggregator;
        private Dictionary<RockQuality, int> realRockTypeHasAmount;
        private Dictionary<RockQuality, double> realRockTypeHasPercentage;

        [TestInitialize]
        public void TestInitialize() {
            Project project = InitializeObjectsToTest();
            InitializeTestSuite(project);
        }

        private Project InitializeObjectsToTest() {
            TunnelArtsEntities entities = InitializeEntities();
            IMapper mapper = CreateMapperInstance();
            Project project = CreateProject(entities, mapper);
            Tunnel tunnel = CreateTunnel(entities, mapper);
            TunnelRepository tunnelRepository = InitializeTunnelRepository(entities);
            ICollection<QValue> qValues = CreateQValues(tunnel, tunnelRepository);

            offerRockTypeAggregator = InitializeOfferRockTypeAggregator(tunnel, project);

            realRockTypeAggregator = InitializeRealRockTypeAggregator(project, qValues);
            return project;
        }

        private TunnelArtsEntities InitializeEntities() {
            return new TunnelArtsEntities();
        }

        private IMapper CreateMapperInstance()
        {
            var AutoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperConfiguration());
            });
            return AutoMapperConfig.CreateMapper();
        }

        private Project CreateProject(TunnelArtsEntities entities, IMapper mapper)
        {
            ProjectRepository projectRepository = InitializeProjectRepository(entities);
            ProjectService projectService = new ProjectService(mapper, projectRepository);
            Guid projectId = new Guid("2acdfa3c-d336-4e25-9e26-ef70bab289ba");
            ProjectDTO project = projectService.findOne(projectId);
            return mapper.Map<ProjectDTO, Project>(project);
        }

        private ProjectRepository InitializeProjectRepository(TunnelArtsEntities entities)
        {
            return new ProjectRepository(entities);
        }

        private Tunnel CreateTunnel(TunnelArtsEntities entities, IMapper mapper)
        {
            TunnelRepository tunnelRepository = InitializeTunnelRepository(entities);
            TunnelService tunnelService = new TunnelService(mapper, tunnelRepository);
            Guid tunnelId = new Guid("F7DEF4B8-B7A9-4B72-900D-EE602002D967");
            TunnelDTO tunnel = tunnelService.findOne(tunnelId);
            return mapper.Map<TunnelDTO, Tunnel>(tunnel);
        }

        private TunnelRepository InitializeTunnelRepository(TunnelArtsEntities entities)
        {
            return new TunnelRepository(entities);
        }

        private List<QValue> CreateQValues(Tunnel tunnel, TunnelRepository tunnelRepository)
        { 
            ICollection<Mapping> mappings = tunnelRepository.findMappings(tunnel.id);
            List<QValue> qValues = new List<QValue>();
            foreach (Mapping mapping in mappings)
            {
                addQValue(mapping, qValues);
            }
            return qValues;
        }

        private void addQValue(Mapping mapping, List<QValue> qValues) {
            IEnumerator<QValue> qValuesEnumerator = mapping.QValues.GetEnumerator();
            qValuesEnumerator.MoveNext();
            QValue current = qValuesEnumerator.Current;
            qValues.Add(current);
        }

        private TunnelDTO CreateTunnelDTO(IMapper mapper, TunnelRepository tunnelRepository, Guid id) {
            TunnelService tunnelService = new TunnelService(mapper, tunnelRepository);
            return tunnelService.findOne(id);
        }

        private ProjectDTO CreateProjectDTO(IMapper mapper, ProjectRepository projectRepository, Guid id)
        {
            ProjectService projectService = new ProjectService(mapper, projectRepository);
            return projectService.findOne(id);
        }

        private OfferRockTypeAggregator InitializeOfferRockTypeAggregator(Tunnel tunnel, Project project)
        {
            return new OfferRockTypeAggregator(tunnel, project.RockQualities);
        }

        private OfferRockTypeAggregator InitializeOfferRockTypeAggregatorDTO(TunnelDTO tunnel, ProjectDTO project)
        {
            return new OfferRockTypeAggregator(tunnel, project.RockQualities);
        }

        private RealRockTypeAggregator InitializeRealRockTypeAggregator(Project project, ICollection<QValue> qValues)
        {
            return new RealRockTypeAggregator(project.RockQualities, qValues);
        }

        private void InitializeTestSuite(Project project) {
            InitializeOfferRockTypeTests(project);
            InitializeRealRockTypeTests(project);
        }

        private void InitializeOfferRockTypeTests(Project project) {
            offerRockTypeHasAmount = InitializeRockTypeHasAmount();
            offerRockTypeHasPercentage = InitializeRockTypeHasPercentage();
            Dictionary<int, int> offerTestSet = CreateOfferTestSet();
            InitializeAmountTestSetToCompareWith(project, offerTestSet, offerRockTypeHasAmount);
            InitializePercentageTestSetToCompareWith(offerRockTypeHasAmount, offerTestSet, offerRockTypeHasPercentage);
        }

        private Dictionary<RockQuality, int> InitializeRockTypeHasAmount()
        {
            return new Dictionary<RockQuality, int>();
        }

        private Dictionary<RockQuality, double> InitializeRockTypeHasPercentage()
        {
            return new Dictionary<RockQuality, double>();
        }

        private Dictionary<int, int> CreateOfferTestSet()
        {
            // {Código de tipo de roca, cantidad ofertada}
            return new Dictionary<int, int>() {
                { 0, 6 }, { 1, 5 }, { 2, 6 },
                { 3, 4 }, { 4, 5 }, { 5, 7 }
            };
        }

        private void InitializeAmountTestSetToCompareWith(Project project, IDictionary<int, int> testSet, IDictionary<RockQuality, int> rockQualityTests)
        {
            IEnumerator<RockQuality> rockQualityIterator = getRockQualityEnumerator(project);
            while (rockQualityIterator.MoveNext())
            {
                LoadIntoRockTypeHasAmount(rockQualityIterator.Current, testSet, rockQualityTests);
            }
        }

        private IEnumerator<RockQuality> getRockQualityEnumerator(Project project) {
            ICollection<RockQuality> rockQualities = project.RockQualities;
            return rockQualities.GetEnumerator();
        }

        private void LoadIntoRockTypeHasAmount(RockQuality rockQuality, IDictionary<int, int> valuesToLookupInto, IDictionary<RockQuality, int> rockTypeHasAmount)
        {
            rockTypeHasAmount.Add(rockQuality, valuesToLookupInto[(int)rockQuality.Code]);
        }

        private void InitializePercentageTestSetToCompareWith(IDictionary<RockQuality, int> rockQualityTests, IDictionary<int, int> testSet, IDictionary<RockQuality, double> percentageTests)
        {
            int total = CountTotalAmountOfValues(rockQualityTests);
            foreach (KeyValuePair<RockQuality, int> amountOfQuantities in rockQualityTests)
            {
                double percentage = calculatePercentage((double)amountOfQuantities.Value, total);
                AddToPercentageTest(percentage, amountOfQuantities.Key, percentageTests);
            }
        }

        private int CountTotalAmountOfValues(IDictionary<RockQuality, int> rockTypeHasAmount)
        {
            int total = 0;
            foreach (KeyValuePair<RockQuality, int> amountOfOffers in rockTypeHasAmount)
            {
                total += amountOfOffers.Value;
            }
            return total;
        }

        private double calculatePercentage(double amount, double total) {
            return amount / total;
        }
        private void AddToPercentageTest(double percentage, RockQuality rockQuality, IDictionary<RockQuality, double> percentageTests) {
            percentageTests.Add(rockQuality, percentage);
        }

        private void InitializeRealRockTypeTests(Project project)
        {
            realRockTypeHasAmount = InitializeRockTypeHasAmount();
            realRockTypeHasPercentage = InitializeRockTypeHasPercentage();
            Dictionary<int, int> realTestSet = CreateRealTestSet();
            InitializeAmountTestSetToCompareWith(project, realTestSet, realRockTypeHasAmount);
            InitializePercentageTestSetToCompareWith(realRockTypeHasAmount, realTestSet, realRockTypeHasPercentage);
        }
        
        private Dictionary<int, int> CreateRealTestSet() {
            // {Código de roca, cantidad real}
            return new Dictionary<int, int>() {
                { 0, 0 }, { 1, 1 }, { 2, 7 },
                { 3, 15 }, { 4, 3 }, { 5, 8 }
            };
        }
        
        [TestMethod]
        public void CanAggregateOffersTest() {
            TestAggregationOfData(offerRockTypeHasAmount, offerRockTypeAggregator);
        }

        private void TestAggregationOfData(Dictionary<RockQuality, int> rockQualityAmounts, IRockTypeAggregator rockTypeAggregator) {
            foreach (KeyValuePair<RockQuality, int> rockQualityAmount in rockQualityAmounts) {
                AssertHasAmountOfRockType(rockQualityAmount.Key, rockQualityAmount.Value, rockTypeAggregator);
            }
        }

        private void AssertHasAmountOfRockType(RockQuality rockQuality, int amount, IRockTypeAggregator rockTypeAggregator) {
            Assert.AreEqual(amount, rockTypeAggregator.amountOfRockType(rockQuality));
        }

        [TestMethod]
        public void CanCalculatePercentageOfOffersTest()
        {
            TestPercentageOfData(offerRockTypeHasPercentage, offerRockTypeAggregator);
        }

        private void TestPercentageOfData(Dictionary<RockQuality, double> rockQualityPercentages, IRockTypeAggregator rockTypeAggregator) {
            foreach (KeyValuePair<RockQuality, double> rockQualityPercentage in rockQualityPercentages)
            {
                AssertHasPercentageOfRockType(rockQualityPercentage.Key, rockQualityPercentage.Value, rockTypeAggregator);
            }
        }

        private void AssertHasPercentageOfRockType(RockQuality rockType, double percentage, IRockTypeAggregator rockTypeAggregator)
        {
            Assert.AreEqual(percentage, rockTypeAggregator.percentageOfRockType(rockType));
        }
        
        [TestMethod]
        public void AggregatesMappingsCorrectlyTest() {
            TestAggregationOfData(realRockTypeHasAmount, realRockTypeAggregator);
        }
        

        [TestMethod]
        public void CanCalculatePercentageOfMappingsTest()
        {
            TestPercentageOfData(realRockTypeHasPercentage, realRockTypeAggregator);
        }

        [TestMethod]
        public void CanExportOffersToJSONTest() {
            AssertIsJsonNotEmpty(offerRockTypeAggregator);
        }

        private void AssertIsJsonNotEmpty(IRockTypeAggregator rockTypeAggregator) {
            string serializedString = JsonRockTypeExporter.serialize(rockTypeAggregator);
            Assert.IsTrue(serializedString.Length > 2);
        }

        [TestMethod]
        public void CanExportRealsToJSONTest()
        {
            AssertIsJsonNotEmpty(realRockTypeAggregator);
        }
    }
}
