﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.repository.prospectionholegroup;
using TunnelAPI.repository.face;
using TunnelAPI.repository.mapping;
using TunnelAPI.repository.project;
using TunnelAPI.repository.tunnel;
using TunnelAPI.services.mapper;
using TunnelAPI.services.model;
using TunnelAPI.services.services.face;
using TunnelAPI.services.services.mapping;
using TunnelAPI.services.services.project;
using TunnelAPI.services.services.prospection;
using TunnelAPI.services.services.prospection.aggregator;
using TunnelAPI.services.services.prospection.aggregator.exporter;
using TunnelAPI.services.services.tunnel;

namespace TunnelApi.tests
{
    [TestClass]
    public class DataAggregationForProspectionTest
    {
        // <<mappingChainageStart, faceName, faceId>, <rodPosition, <rockQualityName, aggregatedDistance>>>
        private Dictionary<Tuple<decimal, string, string>, Dictionary<int, Tuple<string, decimal>>> prospectionTestSuite;
        private ProspectionAggregator prospectionAggregator;

        [TestInitialize]
        public void TestInitialize()
        {
            InitializeTestSuite();
            InitializeAggregator();
        }

        private void InitializeTestSuite() {
            prospectionTestSuite = new Dictionary<Tuple<decimal, string, string>, Dictionary<int, Tuple<string, decimal>>>()
                {
                    {
                        new Tuple<decimal, string, string>(12.5m, "ADDS", "7BCC7A02-D1BE-45D1-BD15-4C941AB63578"),
                        new Dictionary<int, Tuple<string, decimal>>() {
                            { 0, new Tuple<string, decimal>("II", 15.4344428m) },
                            { 1, new Tuple<string, decimal>("II", 18.3688856m) },
                            { 2, new Tuple<string, decimal>("II", 21.3033284m) },
                            { 3, new Tuple<string, decimal>("II", 24.2377712m) }
                        }
                    },
                    {
                        new Tuple<decimal, string, string>(2482m, "ADUS", "FC95F1A5-5E8E-4029-8BDF-727D2ADFFC44"),
                        new Dictionary<int, Tuple<string, decimal>>() {

                        }
                    }
                };
        }

        private void InitializeAggregator() {
            IMapper mapper = CreateMapperInstance();
            TunnelArtsEntities entities = new TunnelArtsEntities();
            Tunnel tunnel = InitializeTunnel(mapper, entities);

            prospectionAggregator = new ProspectionAggregator(tunnel);
        }

        private IMapper CreateMapperInstance()
        {
            var AutoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperConfiguration());
            });
            return AutoMapperConfig.CreateMapper();

        }

        private Tunnel InitializeTunnel(IMapper mapper, TunnelArtsEntities entities)
        {
            TunnelLoader tunnelLoader = CreateTunnelLoader(mapper, entities);
            TunnelService tunnelService = CreateTunnelService(mapper, entities);
            TunnelDTO tunnelDTO = tunnelService.findOne(new Guid("79367018-93E5-4C86-8E5B-6F8CFCC726EE"));
            Tunnel tunnel = mapper.Map<TunnelDTO, Tunnel>(tunnelDTO);

            
            return tunnelLoader.buildTunnel(tunnel);
        }

        private TunnelLoader CreateTunnelLoader(IMapper mapper, TunnelArtsEntities entities) {
            TunnelLoaderBuilder tunnelLoaderBuilder = new TunnelLoaderBuilder() {
                mapper = mapper,
                prospectionHoleGroupService = CreateProspectionHoleGroupService(mapper, entities),
                mappingService = CreateMappingService(mapper, entities),
                faceService = CreateFaceService(mapper, entities)
            };

            return tunnelLoaderBuilder.buildTunnelLoader();
        }

        private ProspectionHoleGroupService CreateProspectionHoleGroupService(IMapper mapper, TunnelArtsEntities entities)
        {
            ProspectionHoleGroupRepository prospectionHoleGroupRepository = new ProspectionHoleGroupRepository(entities);
            return new ProspectionHoleGroupService(mapper, prospectionHoleGroupRepository);
        }

        private FaceService CreateFaceService(IMapper mapper, TunnelArtsEntities entities)
        {
            FaceRepository faceRepository = new FaceRepository(entities);
            return new FaceService(mapper, faceRepository);
        }

        private TunnelService CreateTunnelService(IMapper mapper, TunnelArtsEntities entities)
        {
            TunnelRepository tunnelRepository = new TunnelRepository(entities);
            return new TunnelService(mapper, tunnelRepository);
        }

        private MappingService CreateMappingService(IMapper mapper, TunnelArtsEntities entities)
        {
            MappingRepository mappingRepository = new MappingRepository(entities);
            return new MappingService(mapper, mappingRepository);
        }

        [TestMethod]
        public void CanAggregateProspectionsCorrectlyTest()
        {
            AssertProspectionCardinalitiesAreCorrect(prospectionTestSuite, prospectionAggregator);
            foreach (KeyValuePair<Tuple<decimal, string, string>, Dictionary<int, Tuple<string, decimal>>> prospectionTestForFace in prospectionTestSuite) {
                Tuple<decimal, string, string> mappingInformation = prospectionTestForFace.Key;
                AssertMappingInformationIsCorrect(mappingInformation, prospectionAggregator);

                string faceId = mappingInformation.Item3;
                ProspectionFaceAggregator prospectionFaceAggregator = prospectionAggregator.findFaceAggregator(faceId);
                Dictionary<int, Tuple<string, decimal>> rodTests = prospectionTestForFace.Value;
                AssertAllRodTestsAreCorrect(rodTests, prospectionFaceAggregator);
            }
        }

        private void AssertProspectionCardinalitiesAreCorrect(Dictionary<Tuple<decimal, string, string>, Dictionary<int, Tuple<string, decimal>>> prospectionTests, ProspectionAggregator prospectionAggregator) {
            Assert.AreEqual(prospectionTests.Count, prospectionAggregator.ProspectionFaceAggregators.Count);
        }

        private void AssertMappingInformationIsCorrect(Tuple<decimal, string, string> mappingInformation, ProspectionAggregator prospectionAggregator) {
            decimal chainageStart = mappingInformation.Item1;
            string faceName = mappingInformation.Item2;
            string faceId = mappingInformation.Item3;
            ProspectionFaceAggregator prospectionFaceAggregator = prospectionAggregator.findFaceAggregator(faceId);
            Assert.AreEqual(chainageStart, prospectionFaceAggregator.PK);
            Assert.AreEqual(faceName, prospectionFaceAggregator.FaceName);
        }

        private void AssertAllRodTestsAreCorrect(Dictionary<int, Tuple<string, decimal>> rodTests, ProspectionFaceAggregator prospectionFaceAggregator) {
            AssertRodCardinalityIsCorrect(rodTests, prospectionFaceAggregator);
            foreach (KeyValuePair<int, Tuple<string, decimal>> rodTest in rodTests)
            {
                int rodPosition = rodTest.Key;
                ProspectionRodAggregator prospectionRodAggregator = prospectionFaceAggregator.findRodAggregator(rodPosition);
                AssertProspectionRodAggregatorPositionIsValid(rodPosition, prospectionRodAggregator);

                Tuple<string, decimal> rodData = rodTest.Value;
                AssertProspectionRodAggregatorDataIsCorrect(rodData, prospectionRodAggregator);
            }
        }

        private void AssertRodCardinalityIsCorrect(Dictionary<int, Tuple<string, decimal>> rodTests, ProspectionFaceAggregator prospectionFaceAggregator) {
            Assert.AreEqual(rodTests.Count, prospectionFaceAggregator.ProspectionRodAggregators.Count);
        }

        private void AssertProspectionRodAggregatorPositionIsValid(int rodPosition, ProspectionRodAggregator prospectionRodAggregator) {
            Assert.IsNotNull(prospectionRodAggregator);
            Assert.AreEqual(rodPosition, prospectionRodAggregator.RodPosition);
        }

        private void AssertProspectionRodAggregatorDataIsCorrect(Tuple<string, decimal> rodData, ProspectionRodAggregator prospectionRodAggregator) {
            string rockQualityName = rodData.Item1;
            AssertRockQualityNamesAreEqual(rockQualityName, prospectionRodAggregator);

            decimal aggregatedDistance = rodData.Item2;
            AssertProjectedDistanceAreEqual(aggregatedDistance, prospectionRodAggregator);
            
        }

        private void AssertRockQualityNamesAreEqual(string rockQualityName, ProspectionRodAggregator prospectionRodAggregator) {
            Assert.AreEqual(rockQualityName, prospectionRodAggregator.RockQualityName);
        }

        private void AssertProjectedDistanceAreEqual(decimal aggregatedDistance, ProspectionRodAggregator prospectionRodAggregator) {
            double DELTA = 0.000001;
            Assert.AreEqual((double)aggregatedDistance, (double)prospectionRodAggregator.Distance, DELTA);
        }

        [TestMethod]
        public void CanExportToJSONTest()
        {
            string serializedString = JsonProspectionExporter.serialize(prospectionAggregator);
            AssertJSONIsNotEmpty(serializedString);
        }

        private void AssertJSONIsNotEmpty(string jsonString) {
            Assert.IsTrue(jsonString.Length > 2);
        }
    }
}
