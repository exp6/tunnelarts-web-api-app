﻿using System;

namespace TunnelAPI.services.exceptions
{
    public class OperationNotAllowedException : Exception
    {
        public OperationNotAllowedException()
        {
        }

        public OperationNotAllowedException(string message) : base(message)
        {
        }

        public OperationNotAllowedException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}