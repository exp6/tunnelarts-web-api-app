﻿using System;
using System.Collections.Generic;
using System.IO;
using TunelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.tunnel
{
    public interface ITunnelService
    {
        IEnumerable<TunnelDTO> find(TunnelFilter filter);
        int findAmount(TunnelFilter filter);
        TunnelDTO findOne(Guid tunnelId);
        void update(TunnelDTO tunnel);
        void create(TunnelDTO tunnel);
        void remove(Guid tunnelId);
        float findTunnelProgress(Guid tunnelId);
        bool saveOfferRockTypes(Guid tunnelId, FileStream file);
    }
}
