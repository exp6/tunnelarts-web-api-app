﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.face;

namespace TunnelAPI.services.services.tunnel
{
    public class TunnelLoader
    {
        private IFaceService faceService;
        private IMapper mapper;
        private FaceLoader faceLoader;
        public TunnelLoader(FaceLoader faceLoader, IFaceService faceService, IMapper mapper)
        {
            this.faceService = faceService;
            this.mapper = mapper;
            this.faceLoader = faceLoader;
        }

        public Tunnel buildTunnel(Tunnel tunnel) {
            ICollection<Face> faces = findAllFaces(tunnel);
            List<Face> facesToAdd = new List<Face>();
            foreach (Face face in faces) {
                Face faceToAdd = faceLoader.buildFace(face);
                facesToAdd.Add(faceToAdd);
            }
            tunnel.Faces = facesToAdd;
            return tunnel;
        }

        private ICollection<Face> findAllFaces(Tunnel tunnel) {
            FaceFilter faceFilter = new FaceFilter() {
                tunnelId = tunnel.id,
                range = 0
            };
            ICollection<FaceDTO> facesDTO = faceService.find(faceFilter);
            return mapper.Map<ICollection<FaceDTO>, ICollection<Face>>(facesDTO);
        }
    }
}
