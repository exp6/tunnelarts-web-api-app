﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunnelAPI.services.services.face;
using TunnelAPI.services.services.mapping;
using TunnelAPI.services.services.prospection;

namespace TunnelAPI.services.services.tunnel
{
    public class TunnelLoaderBuilder
    {
        public IMapper mapper { get; set; }
        public IProspectionHoleGroupService prospectionHoleGroupService { get; set; }
        public IMappingService mappingService { get; set; }
        public IFaceService faceService { get; set; }

        public TunnelLoader buildTunnelLoader() {
            if (objectsHaveNotBeenYetLoaded()) {
                throw new ArgumentException("Faltan datos por cargar.");
            }
            MappingLoader mappingLoader = new MappingLoader(prospectionHoleGroupService, mapper);
            FaceLoader faceLoader = new FaceLoader(mappingLoader, mappingService, mapper);
            return new TunnelLoader(faceLoader, faceService, mapper);
        }

        private bool objectsHaveNotBeenYetLoaded() {
            return (
                mapper == null ||
                prospectionHoleGroupService == null ||
                mappingService == null ||
                faceService == null
            );
        }
    }
}
