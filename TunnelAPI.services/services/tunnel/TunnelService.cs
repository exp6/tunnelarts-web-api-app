﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.repository.tunnel;
using TunnelAPI.services.model;
using TunnelAPI.services.services.face;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.OpenXml4Net.OPC;
using NPOI.XSSF.UserModel;

namespace TunnelAPI.services.services.tunnel
{
    public class TunnelService : ITunnelService
    {
        private readonly ITunnelRepository tunnelRepository;
        private readonly IMapper mapper;

        public TunnelService(IMapper mapper, ITunnelRepository tunnelRepository)
        {
            this.mapper = mapper;
            this.tunnelRepository = tunnelRepository;
        }

        public IEnumerable<TunnelDTO> find(TunnelFilter filter)
        {
            var tunnels = tunnelRepository.find(filter);

            return mapper.Map<IEnumerable<Tunnel>, IEnumerable<TunnelDTO>>(tunnels);
        }

        public int findAmount(TunnelFilter filter)
        {
            return tunnelRepository.findAmount(filter);
        }

        public TunnelDTO findOne(Guid tunnelId)
        {
            var tunnel = tunnelRepository.findOne(tunnelId);
            return mapper.Map<Tunnel, TunnelDTO>(tunnel);
        }

        public void update(TunnelDTO tunnel)
        {
            var tunnelEntity = tunnelRepository.findOne(tunnel.id);            
            tunnelEntity.updatedAt = DateTime.Now;
            tunnelEntity.IdEsr = tunnel.IdEsr;
            tunnelEntity.Name = tunnel.Name;
            tunnelEntity.Span = tunnel.Span;
            tunnelEntity.Perimeter = tunnel.Perimeter;
            tunnelEntity.IdTunnelProfile = tunnel.TunnelProfile.id;

            tunnelRepository.saveChanges();
        }

        public void create(TunnelDTO tunnel)
        {
            var tunnelEntity = new Tunnel();
            tunnelEntity.createdAt = DateTime.Now;
            tunnelEntity.updatedAt = DateTime.Now;
            tunnelEntity.deleted = false;
            tunnelEntity.id = tunnel.id;
            tunnelEntity.Name = tunnel.Name;
            tunnelEntity.Span = tunnel.Span;
            tunnelEntity.Perimeter = tunnel.Perimeter;
            tunnelEntity.CreatedBy = tunnel.CreatedBy;
            tunnelEntity.IdEsr = tunnel.Esr.IdEsr;
            tunnelEntity.IdProject = tunnel.Project.id;
            tunnelEntity.IdTunnelProfile = tunnel.TunnelProfile.id;

            tunnelRepository.create(tunnelEntity);
            tunnelRepository.saveChanges();
        }

        public void remove(Guid tunnelId)
        {
            tunnelRepository.remove(tunnelId);
            tunnelRepository.saveChanges();
        }

        public float findTunnelProgress(Guid tunnelId)
        {
            ICollection<Face> faces = tunnelRepository.findFaces(tunnelId);
            double progress = 0;
            foreach (Face f in faces) progress += f.Progress ?? 0;

            return (float)progress;
        }

        public bool saveOfferRockTypes(Guid tunnelId, FileStream file)
        {
            XSSFWorkbook hssfwb;
            using (FileStream _file = file)
            {
                hssfwb = new XSSFWorkbook(_file);
            }

            ISheet sheet = hssfwb.GetSheetAt(0);
            double initialProgress, finalProgress, rockType;
            List<double> initialProgresses = new List<double>();
            List<double> finalProgresses = new List<double>();
            List<int> rocktypes = new List<int>();
            for (int row = 1; row < sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
                {
                    initialProgress = sheet.GetRow(row).GetCell(0).NumericCellValue;
                    finalProgress = sheet.GetRow(row).GetCell(1).NumericCellValue;
                    rockType = sheet.GetRow(row).GetCell(2).NumericCellValue;

                    initialProgresses.Add(initialProgress);
                    finalProgresses.Add(finalProgress);
                    rocktypes.Add((int) rockType);
                }
            }
            return tunnelRepository.saveOfferRockTypes(tunnelId, initialProgresses, finalProgresses, rocktypes);
        }
    }
}
