﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class PerformanceRealRealAggregator : IPerformanceAggregator
    {
        private Dictionary<decimal, DateTime> datesByProgress;

        public PerformanceRealRealAggregator(ICollection<Mapping> mappings) {
            datesByProgress = initializeDatesByProgress();
            fillDatesByProgress(datesByProgress, mappings);
        }

        private Dictionary<decimal, DateTime> initializeDatesByProgress() {
            return new Dictionary<decimal, DateTime>();
        }

        private void fillDatesByProgress(Dictionary<decimal, DateTime> datesByProgress, ICollection<Mapping> mappings) {
            foreach (Mapping mapping in mappings)
            {
                addMapping(datesByProgress, mapping);
            }
        }

        private void addMapping(Dictionary<decimal, DateTime> datesByProgress, Mapping mapping) {
            try {
                datesByProgress.Add((decimal)mapping.ChainageStart, mapping.MappingTime.Value);
            }
            catch (ArgumentException e) {
            }
        }

        public DateTime getProjectedTimeOfProgress(decimal progress)
        {
            return datesByProgress[progress];
        }

        public Dictionary<decimal, DateTime> getProgresses()
        {
            return datesByProgress;
        }
    }
}
