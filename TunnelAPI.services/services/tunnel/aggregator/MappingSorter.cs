﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel
{
    public abstract class MappingSorter
    {
        public static ICollection<Mapping> sortMappings(ICollection<Mapping> mappings) {
            IOrderedEnumerable<Mapping> mappingOrderedEnumerator = mappings.OrderBy(x => x.ChainageStart);
            return mappingOrderedEnumerator.ToList();
        }
    }
}
