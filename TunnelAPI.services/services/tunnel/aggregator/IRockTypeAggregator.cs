﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public interface IRockTypeAggregator
    {
        int amountOfRockType(RockQuality rockType);
        double percentageOfRockType(RockQuality rockQuality);
        Dictionary<RockQuality, double> percentagesOfRockTypes();
    }
}
