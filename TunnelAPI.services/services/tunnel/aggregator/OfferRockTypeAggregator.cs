﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class OfferRockTypeAggregator : IRockTypeAggregator
    {
        private Tunnel tunnel;
        private Dictionary<RockQuality, int> countOfRockType;
        private int totalOfOffers;

        public OfferRockTypeAggregator(TunnelDTO tunnel, ICollection<RockQualityDTO> rockQualities)
        {
            Tunnel realTunnel = Mapper.Map<TunnelDTO, Tunnel>(tunnel);
            ICollection<RockQuality> realRockQualities = Mapper.Map<ICollection<RockQualityDTO>, ICollection<RockQuality>>(rockQualities);
            construct(realTunnel, realRockQualities);
        }

        public OfferRockTypeAggregator(Tunnel tunnel, ICollection<RockQuality> rockQualities) {
            construct(tunnel, rockQualities);
        }

        private void construct(Tunnel tunnel, ICollection<RockQuality> rockQualities) {
            this.tunnel = tunnel;
            totalOfOffers = tunnel.OfferRockTypes.Count;
            countOfRockType = countRockTypes(tunnel, rockQualities);
        }

        private Dictionary<RockQuality, int> countRockTypes(Tunnel tunnel, ICollection<RockQuality> rockQualities)
        {
            Dictionary<RockQuality, int> countTable = new Dictionary<RockQuality, int>();
            foreach (RockQuality rockQuality in rockQualities)
            {
                int totalOfOffers = countAmountOfOfferedRockType(rockQuality, tunnel);
                countTable.Add(rockQuality, totalOfOffers);
            }
            return countTable;
        }

        private int countAmountOfOfferedRockType(RockQuality rockType, Tunnel tunnel)
        {
            int totalOfRockType = 0;
            foreach (OfferRockType offerRockType in tunnel.OfferRockTypes)
            {
                totalOfRockType += increaseIfOfferHasRockType(offerRockType, rockType);
            }
            return totalOfRockType;
        }

        private int increaseIfOfferHasRockType(OfferRockType offerRockType, RockQuality rockType)
        {
            int? rockTypeOfMapping = offerRockType.RockType;
            if (rockTypeOfMapping == rockType.Code)
            {
                return 1;
            }
            return 0;
        }

        public int amountOfRockType(RockQuality rockType)
        {
            return countOfRockType[rockType];
        }

        public double percentageOfRockType(RockQuality rockType)
        {
            return (totalOfOffers == 0) ? 0.0d : countOfRockType[rockType] / (double)totalOfOffers;
        }

        public Dictionary<RockQuality, double> percentagesOfRockTypes() {
            Dictionary<RockQuality, double> percentagesOfRockTypes = new Dictionary<RockQuality, double>();
            foreach (KeyValuePair<RockQuality, int> count in countOfRockType) {
                percentagesOfRockTypes.Add(count.Key, percentageOfRockType(count.Key));
            }
            return percentagesOfRockTypes;
        }
    }
}