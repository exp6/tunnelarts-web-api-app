﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public abstract class RockQualityFinder
    {
        public static RockQuality findRockQualityOfMapping(ICollection<RockQuality> rockQualities, Mapping mapping) {
            QValue qValue = mapping.QValues.FirstOrDefault();
            foreach (RockQuality rockQuality in rockQualities)
            {
                if (qValueBelongsToRockQuality(qValue, rockQuality))
                {
                    return rockQuality;
                }
            }
            throw new IndexOutOfRangeException("No existe calidad de roca para el valor Q " + qValue.QAvg);
        }

        private static Boolean qValueBelongsToRockQuality(QValue qValue, RockQuality rockQuality)
        {
            return rockQuality.UpperBound > qValue.QAvg && qValue.QAvg >= rockQuality.LowerBound;
        }
    }
}
