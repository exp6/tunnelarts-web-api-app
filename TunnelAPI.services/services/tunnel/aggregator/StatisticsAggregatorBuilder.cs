﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunnelAPI.services.services.prospection.aggregator;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class StatisticsAggregatorBuilder
    {
        public IRockTypeAggregator OfferRockType { get; set; }
        public IRockTypeAggregator RealRockType { get; set; }
        public IPerformanceAggregator OfferOfferPerformance { get; set; }
        public IPerformanceAggregator RealOfferPerformance { get; set; }
        public IPerformanceAggregator RealRealPerformance { get; set; }
        public ProspectionAggregator Prospection { get; set; }

        public StatisticsAggregator build() {
            return CreateStatisticsAggregator();
        }

        public void checkIfAllArgumentsAreSet() {
            if (NotAllParametersAreSet())
            {
                throw new ArgumentException("Falta al menos uno de los argumentos para construir el agregador.");
            }
        }

        private bool NotAllParametersAreSet() {
            return OfferRockType == null
                || RealRockType == null
                || OfferOfferPerformance == null
                || RealOfferPerformance == null
                || RealRealPerformance == null
                || Prospection == null;
        }

        private StatisticsAggregator CreateStatisticsAggregator() {
            return new StatisticsAggregator(this) {
                OfferRockType = OfferRockType,
                RealRockType = RealRockType,
                OfferOfferPerformance = OfferOfferPerformance,
                RealOfferPerformance = RealOfferPerformance,
                RealRealPerformance = RealRealPerformance,
                Prospection = Prospection
            };
        }
    }
}
