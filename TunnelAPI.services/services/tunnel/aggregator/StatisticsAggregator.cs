﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunnelAPI.services.services.prospection.aggregator;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class StatisticsAggregator
    {
        public IRockTypeAggregator OfferRockType { get; set; }
        public IRockTypeAggregator RealRockType { get; set; }
        public IPerformanceAggregator OfferOfferPerformance { get; set; }
        public IPerformanceAggregator RealOfferPerformance { get; set; }
        public IPerformanceAggregator RealRealPerformance { get; set; }
        public ProspectionAggregator Prospection { get; set; }

        public StatisticsAggregator(StatisticsAggregatorBuilder builder) {
            builder.checkIfAllArgumentsAreSet();
        }
    }
}
