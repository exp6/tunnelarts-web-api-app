﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class RealPerformance
    {
        private RealPerformanceDataHolder performances;

        public RealPerformance(Project project, ICollection<Mapping> mappings) {
            performances = new RealPerformanceDataHolder(project, mappings);
            aggregateMappings(performances, project, mappings);
        }

        private void aggregateMappings(RealPerformanceDataHolder performances, Project project, ICollection<Mapping> mappings) {
            ICollection<RockQuality> rockQualities = project.RockQualities;

            if (rockQualities.Count <= 0) {
                return;
            }

            ICollection<Mapping> orderedMappings = MappingSorter.sortMappings(mappings);
            DateTime startDateOfMapping = project.StartDate;
            foreach (Mapping mapping in orderedMappings)
            {
                if (mapping.QValues == null || mapping.QValues.Count == 0) {
                    continue;
                }
                RockQuality rockQuality = RockQualityFinder.findRockQualityOfMapping(rockQualities, mapping);
                addPerformanceToRockQuality(mapping, rockQuality, startDateOfMapping);
                startDateOfMapping = (DateTime)mapping.MappingTime;
            }
            performances.calculateAverages();            
        }

        private void addPerformanceToRockQuality(Mapping mapping, RockQuality rockQuality, DateTime startingTime) {
            decimal performanceForMapping = deltaChainage(mapping) / deltaTime(mapping, startingTime);
            performances.addPerformanceToRockQuality(performanceForMapping, rockQuality);
        }

        private decimal deltaChainage(Mapping mapping) {
            return (decimal)mapping.ChainageEnd - (decimal)mapping.ChainageStart;
        }

        private decimal deltaTime(Mapping mapping, DateTime startingTime) {
            return (decimal)(mapping.MappingTime - startingTime).Value.TotalDays;
        }

        public decimal getPerformanceOfCode(int code) {
            return performances.getPerformance(code);
        }
    }

    class RealPerformanceDataHolder
    {
        private Dictionary<int, decimal> performances;
        Dictionary<int, Tuple<decimal, int>> sumAndTotalPerCodes;

        public RealPerformanceDataHolder(Project project, ICollection<Mapping> mappings) {
            performances = initializePerformances(project);
            sumAndTotalPerCodes = initializeSumAndTotalPerCodes(project);
        }

        private Dictionary<int, decimal> initializePerformances(Project project)
        {
            Dictionary<int, decimal> performances = new Dictionary<int, decimal>();
            ICollection<RockQuality> rockQualities = project.RockQualities;
            foreach (RockQuality rockQuality in rockQualities)
            {
                performances.Add((int)rockQuality.Code, 0);
            }
            return performances;
        }

        private Dictionary<int, Tuple<decimal, int>> initializeSumAndTotalPerCodes(Project project)
        {
            ICollection<RockQuality> rockQualities = project.RockQualities;
            Dictionary<int, Tuple<decimal, int>> sumAndTotalPerCodes = new Dictionary<int, Tuple<decimal, int>>();

            foreach (RockQuality rockQuality in rockQualities)
            {
                sumAndTotalPerCodes.Add((int)rockQuality.Code, new Tuple<decimal, int>(0m, 0));
            }

            return sumAndTotalPerCodes;
        }

        public void addPerformanceToRockQuality(decimal performance, RockQuality rockQuality) {
            Tuple<decimal, int> sumAndTotalOfCode = sumAndTotalPerCodes[(int)rockQuality.Code];
            sumAndTotalPerCodes[(int)rockQuality.Code] = new Tuple<decimal, int>(sumAndTotalOfCode.Item1 + performance, sumAndTotalOfCode.Item2 + 1);
        }

        public void calculateAverages()
        {
            foreach (KeyValuePair<int, Tuple<decimal, int>> sumAndTotalOfCode in sumAndTotalPerCodes)
            {
                    performances[sumAndTotalOfCode.Key] = calculateAverageOf(sumAndTotalOfCode.Key);
            }
        }

        private decimal calculateAverageOf(int code) {
            if (amountOfItemsIsNotZero(code))
            {
                Tuple<decimal, int> sumAndTotal = sumAndTotalPerCodes[code];
                return sumAndTotal.Item1 / sumAndTotal.Item2;
            }
            return 0;
        }

        private bool amountOfItemsIsNotZero(int code)
        {
            return sumAndTotalPerCodes[code].Item2 != 0;
        }

        public decimal getPerformance(int code) {
            return performances[code];
        }
    }
}
