﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class PerformanceRealOfferAggregator : IPerformanceAggregator
    {
        private Dictionary<decimal, DateTime> projectedDates;

        public PerformanceRealOfferAggregator(Project project, ICollection<Mapping> mappings)
        {
            projectedDates = new Dictionary<decimal, DateTime>();
            calculateEstimatedTimeOfMappings(mappings, project);
        }

        private void calculateEstimatedTimeOfMappings(ICollection<Mapping> mappings, Project project) {
            ICollection<RockQuality> rockQualities = project.RockQualities;

            if (rockQualities.Count <= 0) {
                return;
            }

            ICollection<Mapping> orderedMappings = MappingSorter.sortMappings(mappings);
            DateTime startDate = project.StartDate;
            foreach (Mapping mapping in orderedMappings)
            {
                if (mapping.QValues == null || mapping.QValues.Count == 0) {
                    continue;
                }
                startDate = tryAddingProjectedDate(rockQualities, mapping, startDate);
            }
        }

        private DateTime tryAddingProjectedDate(ICollection<RockQuality> rockQualities, Mapping mapping, DateTime startDate) {
            try
            {
                return addProjectedDate(rockQualities, mapping, startDate);
            }
            catch (IndexOutOfRangeException e) {
                return startDate;
            }
        }

        private DateTime addProjectedDate(ICollection<RockQuality> rockQualities, Mapping mapping, DateTime startDate) {
            RockQuality rockQuality = RockQualityFinder.findRockQualityOfMapping(rockQualities, mapping);
            double daysToAdd = calculateDaysToAdd(mapping, rockQuality);
            startDate = startDate.AddDays(daysToAdd);
            addProjectedDate(mapping, startDate);
            return startDate;
        }

        private double calculateDaysToAdd(Mapping mapping, RockQuality rockQuality) {
            return (double)(mapping.ChainageEnd - mapping.ChainageStart) / rockQuality.OfferedPerformance;
        }

        private void addProjectedDate(Mapping mapping, DateTime date) {
            projectedDates.Add((decimal)mapping.ChainageStart, date);
        }

        public DateTime getProjectedTimeOfProgress(decimal progress)
        {
            return projectedDates[progress];
        }

        public Dictionary<decimal, DateTime> getProgresses()
        {
            return projectedDates;
        }
    }
}
