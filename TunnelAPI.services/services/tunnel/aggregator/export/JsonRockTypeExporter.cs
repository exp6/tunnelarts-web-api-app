﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator.export
{
    public abstract class JsonRockTypeExporter
    {
        public static string serialize(IRockTypeAggregator rockTypeAggregator) {
            Dictionary<RockQuality, double> rockQualities = rockTypeAggregator.percentagesOfRockTypes();
            List<RockTypePercentage> rockTypePercentagesToSerialize = generateRockTypePercentagesList(rockQualities);
            List<RockTypePercentage> orderedListToSerialize = sortRockTypePercentages(rockTypePercentagesToSerialize);
            return JsonConvert.SerializeObject(orderedListToSerialize);
        }

        private static List<RockTypePercentage> generateRockTypePercentagesList(Dictionary<RockQuality, double> rockQualities) {
            List<RockTypePercentage> rockTypePercentages = new List<RockTypePercentage>();
            foreach (KeyValuePair<RockQuality, double> rockQuality in rockQualities)
            {
                addRockTypePercentage(rockQuality, rockTypePercentages);
            }
            return rockTypePercentages;
        }

        private static void addRockTypePercentage(KeyValuePair<RockQuality, double> rockTypeToAdd, List<RockTypePercentage> listToAddInto) {
            RockTypePercentage rockTypeObjectToAdd = new RockTypePercentage(rockTypeToAdd.Key, rockTypeToAdd.Value);
            listToAddInto.Add(rockTypeObjectToAdd);
        }

        private static List<RockTypePercentage> sortRockTypePercentages(List<RockTypePercentage> listToSort) {
            IOrderedEnumerable<RockTypePercentage> orderedEnumerable = listToSort.OrderBy(rockType => rockType.RockQualityLowerBound);
            return orderedEnumerable.ToList();
        }
    }

    class RockTypePercentage
    {
        public string RockQualityName { get; }
        public int RockQualityCode { get; }
        public double RockQualityLowerBound { get; }
        public double Percentage { get; }
        public RockTypePercentage(RockQuality rockQuality, double percentage) {
            RockQualityName = rockQuality.Name.Trim();
            RockQualityCode = rockQuality.Code.Value;
            Percentage = percentage;
            RockQualityLowerBound = (double)rockQuality.LowerBound;
        }
    }
}
