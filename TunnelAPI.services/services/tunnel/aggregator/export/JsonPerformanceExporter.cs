﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.services.tunnel.aggregator.export
{
    public abstract class JsonPerformanceExporter
    {
        public static string serialize(IPerformanceAggregator performanceAggregator) {
            Dictionary<decimal, DateTime> progresses = performanceAggregator.getProgresses();
            List<ProgressTime> progressTimes = loadProgressesIntoList(progresses);
            List<ProgressTime> sortedProgresses = sortProgresses(progressTimes);
            return JsonConvert.SerializeObject(sortedProgresses);
        }

        private static List<ProgressTime> loadProgressesIntoList(Dictionary<decimal, DateTime> progresses) {
            List<ProgressTime> progressTimes = new List<ProgressTime>();
            foreach (KeyValuePair<decimal, DateTime> progress in progresses) {
                addProgressTimeIntoList(progress, progressTimes);
            }
            return progressTimes;
        }

        private static void addProgressTimeIntoList(KeyValuePair<decimal, DateTime> progress, List<ProgressTime> listToAddInto) {
            ProgressTime progressTime = new ProgressTime(progress.Key, progress.Value);
            listToAddInto.Add(progressTime);
        }

        private static List<ProgressTime> sortProgresses(List<ProgressTime> progressTimes) {
            IOrderedEnumerable<ProgressTime> orderedEnumerable = progressTimes.OrderBy(progress => progress.PK);
            return orderedEnumerable.ToList();
        }
    }

    class ProgressTime
    {
        public decimal PK { get; }
        public DateTime TimeOfProgress { get; }

        public ProgressTime(decimal pk, DateTime timeOfProgress) {
            PK = pk;
            TimeOfProgress = timeOfProgress;
        }
    }
}
