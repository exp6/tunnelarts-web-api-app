﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunnelAPI.services.services.prospection.aggregator.exporter;

namespace TunnelAPI.services.services.tunnel.aggregator.export
{
    public class JsonStatisticsExporter
    {
        private string rockOfferJson;
        private string rockRealJson;
        private string offerOfferPerformanceJson;
        private string realOfferPerformanceJson;
        private string realRealPerformanceJson;
        private string prospectionJson;

        public JsonStatisticsExporter(StatisticsAggregator statisticsAggregator) {
            SerializeAllAggregators(statisticsAggregator);
        }

        private void SerializeAllAggregators(StatisticsAggregator statisticsAggregator) {
            rockOfferJson = JsonRockTypeExporter.serialize(statisticsAggregator.OfferRockType);
            rockRealJson = JsonRockTypeExporter.serialize(statisticsAggregator.RealRockType);
            offerOfferPerformanceJson = JsonPerformanceExporter.serialize(statisticsAggregator.OfferOfferPerformance);
            realOfferPerformanceJson = JsonPerformanceExporter.serialize(statisticsAggregator.RealOfferPerformance);
            realRealPerformanceJson = JsonPerformanceExporter.serialize(statisticsAggregator.RealRealPerformance);
            prospectionJson = JsonProspectionExporter.serialize(statisticsAggregator.Prospection);
        }

        public string serialize() {
            return JsonConvert.SerializeObject(GetDeserializedJson());
        }

        private dynamic GetDeserializedJson() {
            return new {
                OfferRock = JsonConvert.DeserializeObject(rockOfferJson),
                RealRock = JsonConvert.DeserializeObject(rockRealJson),
                OfferOfferPerformance = JsonConvert.DeserializeObject(offerOfferPerformanceJson),
                RealOfferPerformance = JsonConvert.DeserializeObject(realOfferPerformanceJson),
                RealRealPerformance = JsonConvert.DeserializeObject(realRealPerformanceJson),
                Prospection = JsonConvert.DeserializeObject(prospectionJson)
            };
        } 
    }
}
