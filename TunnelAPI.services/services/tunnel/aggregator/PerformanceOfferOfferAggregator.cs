﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class PerformanceOfferOfferAggregator : IPerformanceAggregator
    {
        private Dictionary<decimal, DateTime> projectedDates;

        public PerformanceOfferOfferAggregator(Project project, Tunnel tunnel) {
            projectedDates = new Dictionary<decimal, DateTime>();
            ICollection<OfferRockType> offers = tunnel.OfferRockTypes;
            fillProjectedDatesFromOffers(offers, project);
        }

        private void fillProjectedDatesFromOffers(ICollection<OfferRockType> offers, Project project) {
            DateTime initialDate = project.StartDate;
            ICollection<RockQuality> rockQualities = project.RockQualities;
            ICollection<OfferRockType> orderedOffers = sortOffers(offers);
            decimal accumulatedDays = 0.0m;

            foreach (OfferRockType offer in orderedOffers)
            {
                RockQuality currentQuality = findRockQualityCode(rockQualities, (int)offer.RockType);
                accumulatedDays += calculateDaysTakenForQuality(offer, currentQuality);
                DateTime accumulatedDate = addDaysTo(initialDate, accumulatedDays);
                decimal initialProgress = roundDouble((double)offer.InitialProgress);
                addNewProjectedDate(projectedDates, initialProgress, accumulatedDate);
            }
        }

        private ICollection<OfferRockType> sortOffers(ICollection<OfferRockType> offers)
        {
            IOrderedEnumerable<OfferRockType> orderedOffersEnumerable = offers.OrderBy(offer => offer.InitialProgress);
            return orderedOffersEnumerable.ToList();
        }

        private RockQuality findRockQualityCode(ICollection<RockQuality> rockQualities, int code) {
            foreach (RockQuality rockQuality in rockQualities)
            {
                if (rockQuality.Code == code)
                {
                    return rockQuality;
                }
            }
            throw new IndexOutOfRangeException("No existe la calidad de roca con código " + code);
        }

        private decimal calculateDaysTakenForQuality(OfferRockType offer, RockQuality quality) {
            return (decimal)(offer.FinalProgress - offer.InitialProgress) / (decimal)quality.OfferedPerformance;
        }

        private DateTime addDaysTo(DateTime date, decimal days) {
            return date.AddDays((double)days);
        }

        // Arregla imprecisiones de representación de float. ¿Quizás debería ir en otra clase?
        private decimal roundDouble(double toRound) {
            return (decimal)Math.Round((double)toRound, 3);
        }

        private void addNewProjectedDate(Dictionary<decimal, DateTime> container, decimal progress, DateTime projectedDate) {
            container.Add(progress, projectedDate);
        }

        public DateTime getProjectedTimeOfProgress(decimal progress) {
            return projectedDates[progress];
        }

        public Dictionary<decimal, DateTime> getProgresses() {
            return projectedDates;
        }
    }
}
