﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public interface IPerformanceAggregator
    {
        DateTime getProjectedTimeOfProgress(decimal progress);

        Dictionary<decimal, DateTime> getProgresses();
    }
}
