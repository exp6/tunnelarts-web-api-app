﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.tunnel.aggregator
{
    public class RealRockTypeAggregator : IRockTypeAggregator
    {
        private Dictionary<RockQuality, int> realRockTypes;
        private int totalOfMappings = 0;

        public RealRockTypeAggregator(ICollection<RockQuality> rockQualities, ICollection<QValue> qValues) {
            realRockTypes = initializeRealRockTypes();
            translateQValuesIntoRockQualities(qValues, rockQualities, realRockTypes);
        }

        private Dictionary<RockQuality, int> initializeRealRockTypes() {
            return new Dictionary<RockQuality, int>();
        }

        private void translateQValuesIntoRockQualities(ICollection<QValue> qValues, 
            ICollection<RockQuality> rockQualities, Dictionary<RockQuality, int> realRockTypes) {

            IDictionary<RockQuality, int> readyListOfRealRockTypes = createListOfRealRockTypes(rockQualities, realRockTypes);
            tallyQValuesIntoRealRockTypes(qValues, rockQualities, realRockTypes);
        }

        private IDictionary<RockQuality, int> createListOfRealRockTypes(ICollection<RockQuality> rockQualities, IDictionary<RockQuality, int> realRockTypes) {
            foreach (RockQuality rockQuality in rockQualities)
            {
                createRockQualityIntoRealRockTypes(rockQuality, realRockTypes);
            }
            return realRockTypes;
        }

        private void createRockQualityIntoRealRockTypes(RockQuality rockQuality, IDictionary<RockQuality, int> realRockTypes)
        {
            realRockTypes.Add(rockQuality, 0);
        }

        private void tallyQValuesIntoRealRockTypes(ICollection<QValue> qValues,
            ICollection<RockQuality> rockQualities, Dictionary<RockQuality, int> realRockTypes)
        {
            foreach (QValue qValue in qValues)
            {
                translateAndIncreaseRockQuality(qValue, rockQualities, realRockTypes);
            }

        }

        private void translateAndIncreaseRockQuality(QValue qValue,
            ICollection<RockQuality> rockQualities, Dictionary<RockQuality, int> realRockTypes)
        {
            try
            {
                RockQuality rockQuality = translateQValueIntoRockQuality(qValue, rockQualities);
                increaseRockQualityInRealRockTypes(rockQuality, realRockTypes);
            }
            catch (IndexOutOfRangeException e) {
            }
        }

        private RockQuality translateQValueIntoRockQuality(QValue qValue, ICollection<RockQuality> rockQualities)
        {
            foreach (RockQuality rockQuality in rockQualities)
            {
                if (qValueIsInRockQualityRange(qValue, rockQuality))
                {
                    return rockQuality;
                }
            }
            throw new IndexOutOfRangeException("No hay calidad de roca correspondiente al valor Q " + qValue.QAvg.ToString());
        }

        private Boolean qValueIsInRockQualityRange(QValue qValue, RockQuality rockQuality)
        {
            return rockQuality.UpperBound > qValue.QAvg && qValue.QAvg >= rockQuality.LowerBound;
        }

        private void increaseRockQualityInRealRockTypes(RockQuality rockQuality, IDictionary<RockQuality, int> realRockTypes) {
            realRockTypes[rockQuality] = realRockTypes[rockQuality] + 1;
            totalOfMappings++;
        }

        public int amountOfRockType(RockQuality rockType)
        {
            return realRockTypes[rockType];
        }

        public double percentageOfRockType(RockQuality rockQuality) {
            return (totalOfMappings == 0)? 0 : realRockTypes[rockQuality] / (double)totalOfMappings;
        }

        public Dictionary<RockQuality, double> percentagesOfRockTypes()
        {
            Dictionary<RockQuality, double> percentagesOfRockTypes = new Dictionary<RockQuality, double>();
            foreach (KeyValuePair<RockQuality, int> count in realRockTypes)
            {
                percentagesOfRockTypes.Add(count.Key, percentageOfRockType(count.Key));
            }
            return percentagesOfRockTypes;
        }
    }
}
