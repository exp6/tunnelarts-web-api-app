﻿using System.Collections.Generic;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.role
{
    public interface IRoleService
    {
        ICollection<RoleDTO> find(); 
    }
}
