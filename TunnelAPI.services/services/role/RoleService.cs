﻿using AutoMapper;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.role;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.role
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository roleRepository;
        private readonly IMapper mapper;

        public RoleService(IMapper mapper, IRoleRepository roleRepository)
        {
            this.mapper = mapper;
            this.roleRepository = roleRepository;
        }

        public ICollection<RoleDTO> find()
        {
            var roles = roleRepository.find();
            return mapper.Map<ICollection<Role>, ICollection<RoleDTO>>(roles);
        }
    }
}
