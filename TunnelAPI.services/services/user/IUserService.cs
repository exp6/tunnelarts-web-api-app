﻿using System;
using System.Collections.Generic;
using TunelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.user
{
    public interface IUserService
    {
        UserDTO findOne(string username);
        UserDTO findOne(Guid userId);
        ICollection<UserDTO> find(UserFilter filter);
        int findAmount(UserFilter filter);
        void create(UserDTO user);
        void updatePersonalInformation(UserDTO user);
        void update(UserDTO user);
        void remove(Guid userId);
        bool validateCredential(string username, string password);
    }
}
