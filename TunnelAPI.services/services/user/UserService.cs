﻿using System;
using System.Collections.Generic;
using AutoMapper;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.repository.user;
using TunnelAPI.services.model;
using TunnelAPI.services.services.security;
using System.Web.Security;
using System.Linq;
using TunnelAPI.repository.role;
using TunnelAPI.repository.project;

namespace TunnelAPI.services.services.user
{
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly IProjectRepository projectRepository;
        private readonly IMembershipService membershipService;
        private readonly IMapper mapper;

        public UserService( IMapper mapper,
                            IUserRepository userRepository,
                            IRoleRepository roleRepository,
                            IMembershipService membershipService,
                            IProjectRepository projectRepository)
        {
            this.mapper = mapper;
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.projectRepository = projectRepository;
            this.membershipService = membershipService;            
        }

        public UserDTO findOne(string username)
        {
            var user = userRepository.findOne(username);            

            if (user != null)
                return mapper.Map<User, UserDTO>(user);
            return null;
        }

        public UserDTO findOne(Guid userId)
        {
            var user = userRepository.findOne(userId);
            return mapper.Map<User, UserDTO>(user);
        }

        public bool validateCredential(string username, string password)
        {
            return membershipService.ValidateUser(username, password);            
        }

        public ICollection<UserDTO> find(UserFilter filter)
        {
            var users = userRepository.find(filter);
            return mapper.Map<ICollection<User>, ICollection<UserDTO>>(users);
        }

        public int findAmount(UserFilter filter)
        {
            return userRepository.findAmount(filter);
        }

        public void create(UserDTO user)
        {
            createMembership(user);

            var userEntity = mapper.Map<UserDTO, User>(user);
            userEntity.Enabled = true;
            userEntity.CreatedAt = DateTime.Now;

            userRepository.create(userEntity);
            userRepository.saveChanges();
        }

        private void createMembership(UserDTO user)
        {
            var roles = convertToStringArray(user.Roles);

            var createStatus = membershipService.CreateUser(user.Username, user.Password, user.Email);
            if (createStatus == MembershipCreateStatus.DuplicateUserName)
                throw new InvalidOperationException("The username already exists, choose a different one");
            if (createStatus == MembershipCreateStatus.DuplicateEmail)
                throw new InvalidOperationException("The email already exists, choose a different one");
            if (createStatus == MembershipCreateStatus.InvalidPassword)
                throw new InvalidOperationException("Invalid password. It must have at least 6 characters");

            Roles.AddUserToRoles(user.Username, roles);
        }

        public void updatePersonalInformation(UserDTO user)
        {
            var userEntity = userRepository.findOne(user.IdUser);
            userEntity.Contact = user.Contact;
            userEntity.Email = user.Email;
            userEntity.FirstName = user.FirstName;
            userEntity.LastName = user.LastName;

            userRepository.saveChanges();
        }

        public void update(UserDTO user)
        {
            var userEntity = userRepository.findOne(user.IdUser);
            userEntity.Contact = user.Contact;
            userEntity.Email = user.Email;
            userEntity.Enabled = user.Enabled;
            userEntity.FirstName = user.FirstName;
            userEntity.LastName = user.LastName;                                    

            updateMembership(userEntity, user.Password, user.Roles);

            unassingProjects(userEntity, user.UserProjects);
            unassignRoles(userEntity, user.Roles);
            assingProjects(userEntity, user.UserProjects);
            assignRoles(userEntity, user.Roles);
            
            userRepository.saveChanges();
        }        

        private void unassingProjects(User user, ICollection<UserProjectDTO> selectedProjects)
        {
            var currentUserProjects = user.UserProjects.ToList();

            foreach (UserProject userProject in currentUserProjects)
                if (selectedProjects.FirstOrDefault(pr => pr.IdProject == userProject.IdProject) == null)
                    user.UserProjects.Remove(userProject);
        }

        private void unassignRoles(User user, ICollection<RoleDTO> selectedRoles)
        {
            var currentRoles = user.Roles.ToList();

            foreach (Role role in currentRoles)
                if (selectedRoles.FirstOrDefault(rl => rl.IdRole == role.IdRole) == null)
                    user.Roles.Remove(role);
        }

        private void assingProjects(User user, ICollection<UserProjectDTO> selectedProjects)
        {
            foreach (UserProjectDTO projectSelected in selectedProjects)
            {
                if (user.UserProjects.FirstOrDefault(pr => pr.IdProject == projectSelected.IdProject) == null)
                {
                    var project = projectRepository.findOne(projectSelected.IdProject);
                    var userProject = new UserProject {
                        IdUser = user.IdUser,
                        IdProject = project.id,
                        deleted = false
                    };

                    user.UserProjects.Add(userProject);
                }
            }
        }

        private void assignRoles(User user, ICollection<RoleDTO> selectedRoles)
        {
            foreach (RoleDTO roleSelected in selectedRoles)
            {
                if (user.Roles.FirstOrDefault(rl => rl.IdRole == roleSelected.IdRole) == null)
                {
                    var role = roleRepository.findOne(roleSelected.IdRole);
                    user.Roles.Add(role);
                }
            }
        }

        public void updateMembership(User user, string password, ICollection<RoleDTO> selectedRoles)
        {
            string[] assignedRoles = convertToStringArray(selectedRoles);
            addTheNewAssignedRoles(user, assignedRoles);
            removeTheNonAssignedRoles(user, assignedRoles);

            var membershipUser = Membership.GetUser(user.UserName);
            membershipUser.IsApproved = true;
            membershipUser.UnlockUser();
            membershipUser.Email = user.Email;          

            if (!String.IsNullOrEmpty(password))
            {
                var oldPassWord = membershipUser.ResetPassword();
                membershipUser.ChangePassword(oldPassWord, password);
            }
                        
            Membership.UpdateUser(membershipUser);
        }

        private void addTheNewAssignedRoles(User user, string[] assignedRoles)
        {
            foreach (Role role in user.Roles)
            {
                if (!assignedRoles.Contains(role.Name))                 
                    Roles.RemoveUserFromRole(user.UserName, role.Name);                    
            }
        }

        private void removeTheNonAssignedRoles(User user, string[] assignedRoles)
        {
            foreach (string roleName in assignedRoles)
            {
                var role = user.Roles.FirstOrDefault(rl => rl.Name == roleName);

                if (role == null)
                    Roles.AddUserToRole(user.UserName, roleName);
            }
        }

        private string[] convertToStringArray(ICollection<RoleDTO> roles)
        {
            var list = new List<string>();
            foreach (RoleDTO role in roles)
            {
                list.Add(role.Name);
            }

            return list.ToArray();
        }

        private string[] convertToStringArray(ICollection<Role> roles)
        {
            var list = new List<string>();
            foreach (Role role in roles)
                list.Add(role.Name);

            return list.ToArray();
        }

        public void remove(Guid userId)
        {
            var user = userRepository.findOne(userId);

            //removeRoles(user);
            //Membership.DeleteUser(user.UserName.ToLower(), true);

            //userRepository.remove(userId);

            userRepository.deactivate(userId);
            userRepository.saveChanges();
        }

        private void removeRoles(User user)
        {
            try
            {
                var roles = convertToStringArray(user.Roles);
                Roles.RemoveUserFromRoles(user.UserName, roles);
            }
            catch (Exception ex) {
            }
        }
    }
}
