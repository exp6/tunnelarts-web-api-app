﻿using AutoMapper;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.esr;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.esr
{
    public class EsrService : IEsrService
    {
        private readonly IEsrRepository esrRepository;
        private readonly IMapper mapper;

        public EsrService(IMapper mapper, IEsrRepository esrRepository)
        {
            this.mapper = mapper;
            this.esrRepository = esrRepository;
        }

        public ICollection<EsrDTO> find()
        {
            var esrs = esrRepository.find();
            return mapper.Map<ICollection<Esr>, ICollection<EsrDTO>>(esrs);
        }
    }
}
