﻿using System.Collections.Generic;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.esr
{
    public interface IEsrService
    {
        ICollection<EsrDTO> find(); 
    }
}
