﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.repository.project;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.project
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository projectRepository;
        private readonly IMapper mapper;

        public ProjectService(IMapper mapper, IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
            this.mapper = mapper;            
        }

        public IEnumerable<ProjectDTO> find(Guid clientId, ProjectFilter filter)
        {            
            var projects = projectRepository.find(clientId, filter);

            return mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDTO>>(projects);
        }

        public IEnumerable<FormationUnitDTO> findFormationUnits(Guid mappingId)
        {
            var formationUnits = projectRepository.findFormationUnits(mappingId);

            return mapper.Map<IEnumerable<FormationUnit>, IEnumerable<FormationUnitDTO>>(formationUnits);
        }

        public int findAmount(Guid clientId, ProjectFilter filter)
        {                        
            var total = projectRepository.findAmount(clientId, filter);
            return total;
        }

        public void create(ProjectDTO project)
        {
            var projectEntity = mapper.Map<ProjectDTO, Project>(project);
            projectEntity.deleted = false;
            projectEntity.updatedAt = DateTime.Now;
            projectEntity.createdAt = DateTime.Now;
            projectEntity.Progress = 0;        
                    
            projectRepository.create(projectEntity);
            projectRepository.saveChanges();
        }

        public void update(ProjectDTO project)
        {
            var projectEntity = projectRepository.findOne(project.id);
            projectEntity.Name = project.Name;
            projectEntity.Code = project.Code;
            projectEntity.Description = project.Description;
            projectEntity.FinishDate = project.FinishDate;
            projectEntity.StartDate = project.StartDate;
            projectEntity.updatedAt = DateTime.Now;

            projectRepository.saveChanges();            
        }

        public ProjectDTO findOne(Guid projectId)
        {
            var project = projectRepository.findOne(projectId);
            return mapper.Map<Project, ProjectDTO>(project);
        }

        public void remove(Guid projectId)
        {
            projectRepository.remove(projectId);
            projectRepository.saveChanges();
        }
        
        public void updateRockQualities(ProjectDTO project)
        {
            IEnumerable<RockQuality> rockqualities = mapper.Map<IEnumerable<RockQualityDTO>, IEnumerable<RockQuality>>(project.RockQualities);
            projectRepository.updateRockQualities(project.id, rockqualities);
            projectRepository.saveChanges();
        }

        public void updateFormationUnit(ProjectDTO project)
        {
            IEnumerable<FormationUnit> formationUnits = mapper.Map<IEnumerable<FormationUnitDTO>, IEnumerable<FormationUnit>>(project.FormationUnits);
            projectRepository.updateFormationUnits(project.id, formationUnits);
            projectRepository.saveChanges();
        }

        public ClientSubscriptionDTO findSubscription(Guid clientId)
        {
            return mapper.Map<ClientSubscription, ClientSubscriptionDTO>(projectRepository.findSubscription(clientId));
        }

        public IEnumerable<MappingInputDTO> findMappingOptions()
        {
            IEnumerable<MappingInput> mappingInputs = projectRepository.findMappingOptions();
            return mapper.Map<IEnumerable<MappingInput>, IEnumerable<MappingInputDTO>>(mappingInputs);
        }

        public void updateMappingOptions(ProjectDTO project)
        {
            try
            {
                projectRepository.updateMappingOptions(project.id, mapper.Map<ICollection<MappingInputDTO>, ICollection<MappingInput>>(project.MappingInputs));
                projectRepository.saveChanges();
            }
            catch(Exception e)
            {

            }
        }
    }
}
