﻿
using System;
using System.Collections.Generic;
using TunelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.project
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> find(Guid clientId, ProjectFilter filter);
        int findAmount(Guid clientId, ProjectFilter filter);
        ProjectDTO findOne(Guid projectId);
        IEnumerable<FormationUnitDTO> findFormationUnits(Guid projectId);
        void create(ProjectDTO project);
        void update(ProjectDTO project);
        void remove(Guid projectId);
        void updateRockQualities(ProjectDTO project);
        void updateFormationUnit(ProjectDTO project);
        ClientSubscriptionDTO findSubscription(Guid clientId);
        IEnumerable<MappingInputDTO> findMappingOptions();
        void updateMappingOptions(ProjectDTO project);
    }
}
