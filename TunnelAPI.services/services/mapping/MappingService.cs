﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.mapping;
using TunnelAPI.repository.model;
using TunnelAPI.services.model;
using System.Linq;

namespace TunnelAPI.services.services.mapping
{
    public class MappingService : IMappingService
    {
        private IMappingRepository mappingRepository;
        private IMapper mapper;

        public MappingService(IMapper mapper, IMappingRepository mappingRepository)
        {
            this.mappingRepository = mappingRepository;
            this.mapper = mapper;
        }

        public MappingDTO findOne(Guid mappingId)
        {
            var mapping = mappingRepository.findOne(mappingId);             
            MappingDTO result = mapper.Map<Mapping, MappingDTO>(mapping);
            return result;
        }

        public IEnumerable<MappingDTO> find(Guid faceId, MappingFilter filter)
        {
            var mappings = mappingRepository.find(faceId, filter)
                .Select(mp => new MappingDTO {
                    id = mp.id,
                    User = new UserDTO { FirstName = mp.User.FirstName, LastName = mp.User.LastName },                    
                    ChainageStart = mp.ChainageStart,
                    ChainageEnd = mp.ChainageEnd,
                    createdAt = mp.createdAt,
                    updatedAt = mp.updatedAt,
                    Finished = mp.Finished,
                    deleted = mp.deleted,
                    QValues = mapper.Map<ICollection<QValue>, ICollection<QValueDTO>>(mp.QValues),
                    MappingTime = (DateTime)mp.MappingTime,
                    IdFace = mp.IdFace.Value
                });

            return mappings;
        }

        public int findAmount(Guid tunnelId, MappingFilter filter)
        {
            var total = mappingRepository.findAmount(tunnelId, filter);
            return total;
        }         

        public void update(MappingDTO mapping)
        {
            var mappingEntity = mappingRepository.findOne(mapping.id);
           
            updateRockMasses(mapping.RockMasses, mappingEntity.RockMasses);
            updateLithologies(mapping.Lithologies, mappingEntity.Lithologies);
            updateDiscontinuities(mapping.Discontinuities, mappingEntity.Discontinuities);
            updateAdditionaInformation(mapping.AdditionalInformations, mappingEntity.AdditionalInformations);
            updateFailureZones(mapping.FailureZones, mappingEntity.FailureZones);
            updateParticularities(mapping.Particularities, mappingEntity.Particularities);
            //updateAdditionalDescriptions(mapping.AdditionalDescriptions, mappingEntity.AdditionalDescriptions);
            updateQValues(mapping.QValues, mappingEntity.QValues);
            updateQValueInputSelections(mapping.QValueInputSelections, mappingEntity.QValueInputSelections);
            updateRMRs(mapping.Rmrs, mappingEntity.Rmrs);
            updateRMRInputSelections(mapping.RmrInputSelections, mappingEntity.RmrInputSelections);


            mappingEntity.updatedAt = DateTime.Now;            

            mappingRepository.saveChanges();
        }

        private void updateRockMasses(ICollection<RockMassDTO> newRockMasses, ICollection<RockMass> oldRockMasses)
        {
            foreach(var newRockMass in newRockMasses)
            {
                var oldRockMass = oldRockMasses.Where(rm => rm.id == newRockMass.id).FirstOrDefault();
                oldRockMass.GeneticGroup = newRockMass.GeneticGroup;
                oldRockMass.Jointing = newRockMass.Jointing;
                oldRockMass.RockName = newRockMass.RockName;
                oldRockMass.Strength = newRockMass.Strength;
                oldRockMass.Structure = newRockMass.Structure;
                oldRockMass.Water = newRockMass.Water;
                oldRockMass.Weathering = newRockMass.Weathering;
                oldRockMass.WaterT = newRockMass.WaterT;
                oldRockMass.ColorValue = newRockMass.ColorValue;
                oldRockMass.WaterQuality = newRockMass.WaterQuality;
                oldRockMass.ColorChroma = newRockMass.ColorChroma;
                oldRockMass.Inflow = newRockMass.Inflow;
                oldRockMass.ColorHue = newRockMass.ColorHue;
            }
        }

        private void updateLithologies(ICollection<LithologyDTO> newLithologies, ICollection<Lithology> oldLithologies)
        {
            foreach (var newLithology in newLithologies)
            {
                var oldLithology = oldLithologies.Where(lt => lt.id == newLithology.id).FirstOrDefault();

                oldLithology.Presence = newLithology.Presence;
                oldLithology.ColourChroma = newLithology.ColourChroma;
                oldLithology.GeneticGroup = newLithology.GeneticGroup;
                oldLithology.ColorHue = newLithology.ColorHue;
                oldLithology.SubGroup = newLithology.SubGroup;
                oldLithology.Strength = newLithology.Strength;
                oldLithology.FormationUnit = newLithology.FormationUnit;
                oldLithology.Texture = newLithology.Texture;
                oldLithology.AlterationWeathering = newLithology.AlterationWeathering;
                oldLithology.GrainSize = newLithology.GrainSize;
                oldLithology.ColourValue = newLithology.ColourValue;
                oldLithology.JvMin = newLithology.JvMin;
                oldLithology.JvMax = newLithology.JvMax;
                oldLithology.BlockShape = newLithology.BlockShape;
                oldLithology.BlockSizeOne = newLithology.BlockSizeOne;
                oldLithology.BlockSizeTwo = newLithology.BlockSizeTwo;
                oldLithology.BlockSizeThree = newLithology.BlockSizeThree;
            }
        }

        private void updateDiscontinuities(ICollection<DiscontinuityDTO> newDiscontinuities, ICollection<Discontinuity> oldDiscontinuities)
        {
            foreach (var newDiscontinuity in newDiscontinuities)
            {
                var oldDiscontinuity = oldDiscontinuities.Where(dis => dis.id == newDiscontinuity.id).FirstOrDefault();

                oldDiscontinuity.Type = newDiscontinuity.Type;
                oldDiscontinuity.Opening = newDiscontinuity.Opening;
                oldDiscontinuity.Relevance = newDiscontinuity.Relevance;
                oldDiscontinuity.Roughness = newDiscontinuity.Roughness;
                oldDiscontinuity.OrientationDd = newDiscontinuity.OrientationDd;
                oldDiscontinuity.Slickensided = newDiscontinuity.Slickensided;
                oldDiscontinuity.OrientationD = newDiscontinuity.OrientationD;
                oldDiscontinuity.Infilling = newDiscontinuity.Infilling;
                oldDiscontinuity.Spacing = newDiscontinuity.Spacing;
                oldDiscontinuity.InfillingType = newDiscontinuity.InfillingType;
                oldDiscontinuity.Persistence = newDiscontinuity.Persistence;
                oldDiscontinuity.Weathering = newDiscontinuity.Weathering;
            }
        }

        private void updateAdditionaInformation(ICollection<AdditionalInformationDTO> newAdditionals, ICollection<AdditionalInformation> oldAdditionals)
        {
            foreach (var newAdditional in newAdditionals)
            {
                var oldAdditional = oldAdditionals.Where(ad => ad.id == newAdditional.id).FirstOrDefault();
                
                foreach (var newRockMass in newAdditional.RockMassHazards)
                {
                    var oldRockMass= oldAdditional.RockMassHazards.Where(rm => rm.id == newRockMass.id).FirstOrDefault();
                    oldRockMass.None = newRockMass.None;
                    oldRockMass.RockBurst = newRockMass.RockBurst;
                    oldRockMass.Swelling = newRockMass.Swelling;
                    oldRockMass.Squeezing = newRockMass.Squeezing;
                    oldRockMass.Slaking = newRockMass.Slaking;                    
                }

                foreach (var newSpecialFeature in newAdditional.SpecialFeatures)
                {
                    var oldSpecialFeature = oldAdditional.SpecialFeatures.Where(sp => sp.id == newSpecialFeature.id).FirstOrDefault();
                    oldSpecialFeature.None = newSpecialFeature.None;
                    oldSpecialFeature.Zeolites = newSpecialFeature.Zeolites;
                    oldSpecialFeature.Clay = newSpecialFeature.Clay;
                    oldSpecialFeature.Chlorite = newSpecialFeature.Chlorite;
                    oldSpecialFeature.RedTuff = newSpecialFeature.RedTuff;
                    oldSpecialFeature.Sulfates = newSpecialFeature.Sulfates;
                    oldSpecialFeature.Sulfides = newSpecialFeature.Sulfides;
                }
            }
        }

        private void updateFailureZones(ICollection<FailureZoneDTO> newFailureZones, ICollection<FailureZone> oldFailureZones)
        {
            foreach (var newFailureZone in newFailureZones)
            {
                var oldFailureZone = oldFailureZones.Where(fz => fz.id == newFailureZone.id).FirstOrDefault();
                oldFailureZone.SenseOfMovement = newFailureZone.SenseOfMovement;
                oldFailureZone.MatrixColourChroma = newFailureZone.MatrixColourChroma;
                oldFailureZone.RakeOfStriae = newFailureZone.RakeOfStriae;
                oldFailureZone.NoneRake = newFailureZone.NoneRake;
                oldFailureZone.MatrixColourHue = newFailureZone.MatrixColourHue;
                oldFailureZone.OrientationDd = newFailureZone.OrientationDd;
                oldFailureZone.MatrixGainSize = newFailureZone.MatrixGainSize;
                oldFailureZone.OrientationD = newFailureZone.OrientationD;
                oldFailureZone.BlockSize = newFailureZone.BlockSize;
                oldFailureZone.Thickness = newFailureZone.Thickness;
                oldFailureZone.BlockShape = newFailureZone.BlockShape;
                oldFailureZone.MatrixBlock = newFailureZone.MatrixBlock;
                oldFailureZone.BlockGeneticGroup = newFailureZone.BlockGeneticGroup;
                oldFailureZone.MatrixColourValue = newFailureZone.MatrixColourValue;
                oldFailureZone.BlockSubGroup = newFailureZone.BlockSubGroup;
            }
        }

        private void updateParticularities(ICollection<ParticularityDTO> newParticularities, ICollection<Particularity> oldParticularities)
        {
            foreach (var newParticularity in newParticularities)
            {
                var oldParticularity = oldParticularities.Where(pa => pa.id == newParticularity.id).FirstOrDefault();

                oldParticularity.InstabilityCausedBy = newParticularity.InstabilityCausedBy;
                oldParticularity.InstabilityLocation = newParticularity.InstabilityLocation;
                oldParticularity.InstabilityNa = newParticularity.InstabilityNa;
                oldParticularity.OverbreakNa = newParticularity.OverbreakNa;
                oldParticularity.OverbreakCausedBy = newParticularity.OverbreakCausedBy;
                oldParticularity.OverbreakLocationStart = newParticularity.OverbreakLocationStart;
                oldParticularity.OverbreakLocationEnd = newParticularity.OverbreakLocationEnd;
                oldParticularity.OverbreakVolume = newParticularity.OverbreakVolume;
            }
        }

        private void updateAdditionalDescriptions(ICollection<AdditionalDescriptionDTO> newDescriptions, ICollection<AdditionalDescription> oldDescriptions)
        {
            foreach (var newDescription in newDescriptions)
            {
                var oldDescription = oldDescriptions.Where(ds => ds.id == newDescription.id).FirstOrDefault();

                oldDescription.DistanceToFace = newDescription.DistanceToFace;
                oldDescription.ResponsibleForRestrictedArea = newDescription.ResponsibleForRestrictedArea;
                oldDescription.ReasonForRestrictedArea = newDescription.ReasonForRestrictedArea;
                oldDescription.LightQuality = newDescription.LightQuality;
                oldDescription.AirQuality = newDescription.AirQuality;
                oldDescription.AvailableTime = newDescription.AvailableTime;
                oldDescription.NewDamageBehind = newDescription.NewDamageBehind;
                oldDescription.Description = newDescription.Description;
            }
        }

        private void updateQValues(ICollection<QValueDTO> newQValues, ICollection<QValue> oldValues)
        {
            foreach (var newQValue in newQValues)
            {
                var oldQValue = oldValues.Where(qv => qv.id == newQValue.id).FirstOrDefault();

                oldQValue.Rqd = newQValue.Rqd;
                oldQValue.Jn = newQValue.Jn;
                oldQValue.Jr = newQValue.Jr;
                oldQValue.Ja = newQValue.Ja;
                oldQValue.Jw = newQValue.Jw;
                oldQValue.Srf = newQValue.Srf;
                oldQValue.RockQuality = newQValue.RockQuality;
                oldQValue.rockQualityCode = newQValue.rockQualityCode;
                oldQValue.QAvg = newQValue.QAvg;
                oldQValue.QMax = newQValue.QMax;
                oldQValue.QMin = newQValue.QMin;
                oldQValue.JnType = newQValue.JnType;
                oldQValue.Discontinuities = newQValue.Discontinuities.Value;
                foreach (var a in oldQValue.QValueDiscontinuities)
                {
                    var oldQvalueDiscontinuity = newQValue.QValueDiscontinuities.Where(y => y.id == a.id).FirstOrDefault();
                    a.JaSelection = oldQvalueDiscontinuity.JaSelection;
                    a.JrSelection = oldQvalueDiscontinuity.JrSelection;
                }
            }
        }

        private void updateQValueInputSelections(ICollection<QValueInputSelectionDTO> newQValues, ICollection<QValueInputSelection> oldValues)
        {
            foreach (var newQValue in newQValues)
            {
                var oldQValue = oldValues.Where(qv => qv.id == newQValue.id).FirstOrDefault();
                
                oldQValue.Jn = newQValue.Jn;
                oldQValue.Jr = newQValue.Jr;
                oldQValue.Ja = newQValue.Ja;
                oldQValue.Jw = newQValue.Jw;
                oldQValue.Srf = newQValue.Srf;
            }
        }

        private void updateRMRs(ICollection<RmrDTO> newRMRs, ICollection<Rmr> oldRMRs)
        {
            foreach (var newRMR in newRMRs)
            {
                var oldRMR = oldRMRs.Where(rmr => rmr.id == newRMR.id).FirstOrDefault();

                oldRMR.Rqd = newRMR.Rqd;
                oldRMR.Strength = newRMR.Strength;
                oldRMR.Spacing = newRMR.Spacing;
                oldRMR.Persistence = newRMR.Persistence;
                oldRMR.Opening = newRMR.Opening;
                oldRMR.Roughness = newRMR.Roughness;
                oldRMR.Infilling = newRMR.Infilling;
                oldRMR.Weathering = newRMR.Weathering;
                oldRMR.Groundwater = newRMR.Groundwater;
                oldRMR.Orientation = newRMR.Orientation;
                oldRMR.RockQualityCode = newRMR.RockQualityCode;
            }
        }

        private void updateRMRInputSelections(ICollection<RmrInputSelectionDTO> newRMRs, ICollection<RmrInputSelection> oldRMRs)
        {
            foreach (var newRMR in newRMRs)
            {
                var oldRMR = oldRMRs.Where(rmr => rmr.id == newRMR.id).FirstOrDefault();

                oldRMR.Rqd = newRMR.Rqd;
                oldRMR.Strength = newRMR.Strength;
                oldRMR.Spacing = newRMR.Spacing;
                oldRMR.Persistence = newRMR.Persistence;
                oldRMR.Opening = newRMR.Opening;
                oldRMR.Roughness = newRMR.Roughness;
                oldRMR.Infilling = newRMR.Infilling;
                oldRMR.Weathering = newRMR.Weathering;
                oldRMR.Groundwater = newRMR.Groundwater;
                oldRMR.OrientationDD = newRMR.OrientationDD;                
            }
        }

        public void changeStatus(MappingDTO mapping)
        {
            var mappingEntity = mappingRepository.findOne(mapping.id);            

            mappingEntity.Finished = mapping.Finished;
            mappingEntity.updatedAt = DateTime.Now;

            mappingRepository.saveChanges();
        }

        public void updatePicturesRemoteUri(Guid mappingId, int sideCode, string remoteUri, string remoteUriOriginal)
        {
            mappingRepository.updatePicturesRemoteUri(mappingId, sideCode, remoteUri, remoteUriOriginal);
            mappingRepository.saveChanges();
        }

        public IEnumerable<PictureDTO> findPictures(Guid mappingId)
        {
            List<PictureDTO> pictures = new List<PictureDTO>();
            IEnumerable<Picture> pictures1 = mappingRepository.findPictures(mappingId);

            PictureDTO picture;
            foreach (var pic in pictures1) {
                picture = mapper.Map<Picture, PictureDTO>(pic);
                pictures.Add(picture);
            }
            
            return pictures;
        }
    }
}
