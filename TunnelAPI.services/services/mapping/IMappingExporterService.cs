﻿using System;
using System.IO;

namespace TunnelAPI.services.services.mapping
{
    public interface IMappingExporterService
    {
        MemoryStream toExcel(Guid mappingId);
    }
}
