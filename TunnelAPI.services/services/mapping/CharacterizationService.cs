﻿using AutoMapper;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.mapping;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.mapping
{
    public class CharacterizationService : ICharacterizationService
    {
        private readonly ICharacterizationRepository repository;
        private readonly IMapper mapper;

        public CharacterizationService(IMapper mapper, ICharacterizationRepository repository)
        {
            this.mapper = mapper;
            this.repository = repository;
        }

        public CharacterizationDTO find()
        {
            CharacterizationDTO characterization = new CharacterizationDTO();
            characterization.Airs = repository.findAirs();
            characterization.AlterationsWeathering = repository.findAlterationsWeathering();
            characterization.BlockShapes = repository.findBlockShapes();
            characterization.BlockShapeTypes = repository.findBlockShapeTypes();
            characterization.BlockSizes = repository.findBlockSizes();
            characterization.ColourTypes = repository.findColourTypes();
            characterization.ColourTypeValues = repository.findColourTypeValues();
            characterization.Damages = repository.findDamages();
            characterization.DiscontinuityTypes = repository.findDiscontinuityTypes();
            characterization.FaceTypes = repository.findFaceTypes();
            characterization.FaultTypes = repository.findFaultTypes();
            characterization.GeneticGroups = repository.findGeneticGroups();
            characterization.GrainSizes = repository.findGrainSizes();
            characterization.Infillings = repository.findInfillings();
            characterization.InfillingTypes = repository.findInfillingTypes();
            characterization.Jointings = repository.findJointings();
            characterization.Lights = repository.findLights();
            characterization.Openings = repository.findOpenings();
            characterization.OverbreakCauses = repository.findOverbreakCauses();
            characterization.Persistences = repository.findPersistences();
            characterization.QRockQualities = repository.findQRockQualities();
            characterization.QJnTypes = repository.findQJnTypes();
            characterization.Reasons = repository.findReasons();
            characterization.Relevances = repository.findRelevances();
            characterization.Responsibles = repository.findResponsibles();
            characterization.RmrRockQualities = repository.findRmrRockQualities();
            characterization.Roughnesses = repository.findRoughnesses();
            characterization.SenseOfMovement = repository.findSenseMovement();
            characterization.Slickensideds = repository.findSlickensideds();
            characterization.Spacings = repository.findSpacings();
            characterization.Strengths = repository.findStrengths();
            characterization.Structures = repository.findStructures();
            characterization.SubGroups = repository.findSubGroups();
            characterization.Textures = repository.findTextures();
            characterization.WaterQualities = repository.findWaterQualities();
            characterization.Waters = repository.findWaters();
            characterization.Weatherings = repository.findWeatherings();

            var QValueInputGroupTypes = repository.findQValueInputGroupTypes();
            var RmrInputGroupTypes = repository.findRmrInputGroupTypes();

            characterization.QValueInputGroupTypes = mapper.Map<IEnumerable<QValueInputGroupTypeDTO>>(QValueInputGroupTypes);
            characterization.RmrInputGroupTypes = mapper.Map<IEnumerable<RmrInputGroupTypeDTO>>(RmrInputGroupTypes);


            return characterization;            
        }
    }
}

