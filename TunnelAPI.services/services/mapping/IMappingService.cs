﻿using System;
using System.Collections.Generic;
using TunnelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.mapping
{
    public interface IMappingService
    {
        MappingDTO findOne(Guid mappingId);
        IEnumerable<MappingDTO> find(Guid faceId, MappingFilter mappingFilter);
        int findAmount(Guid faceId, MappingFilter mappingFilter);
        void update(MappingDTO mapping);
        void changeStatus(MappingDTO mapping);
        void updatePicturesRemoteUri(Guid mappingId, int sideCode, string remoteUri, string remoteUriOriginal);
        IEnumerable<PictureDTO> findPictures(Guid mappingId);
    }
}
