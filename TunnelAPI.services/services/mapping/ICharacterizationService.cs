﻿using TunnelAPI.services.model;

namespace TunnelAPI.services.services.mapping
{
    public interface ICharacterizationService
    {
        CharacterizationDTO find();
    }
}
