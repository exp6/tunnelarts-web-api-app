﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.prospection;

namespace TunnelAPI.services.services.mapping
{
    public class MappingLoader
    {
        private IProspectionHoleGroupService prospectionHoleGroupService;
        private IMapper mapper;
        public MappingLoader(IProspectionHoleGroupService prospectionHoleGroupService, IMapper mapper) {
            this.prospectionHoleGroupService = prospectionHoleGroupService;
            this.mapper = mapper;
        }

        public Mapping buildProspectionsForMapping(Mapping mapping) {
            ProspectionHoleGroupFilter filter = new ProspectionHoleGroupFilter() {
                range = 0
            };

            IEnumerable<ProspectionHoleGroupDTO> prospectionHoleGroupEnumerable = prospectionHoleGroupService.find(mapping.IdFace.Value, filter);
            ProspectionHoleGroupDTO prospectionHoleGroupDTO = getLatestProspectionHoleGroup(prospectionHoleGroupEnumerable);
            ProspectionHoleGroup prospectionHoleGroup = convertToProspectionHoleGroup(prospectionHoleGroupDTO);

            prospectionHoleGroup.ProspectionHoleGroupCalculations = getProspectionHoleGroupCalculations(prospectionHoleGroupDTO);

            mapping.ProspectionHoleGroups = new List<ProspectionHoleGroup>() { prospectionHoleGroup };
            return mapping;
        }

        private ProspectionHoleGroupDTO getLatestProspectionHoleGroup(IEnumerable<ProspectionHoleGroupDTO> prospectionHoleGroupEnumerable) {
            if (ThereAreNoProspectionHoleGroups(prospectionHoleGroupEnumerable)) {
                return new ProspectionHoleGroupDTO();
            }
            if (ThereIsOnlyOneProspectionHoleGroup(prospectionHoleGroupEnumerable)) {
                return prospectionHoleGroupEnumerable.FirstOrDefault();
            }
            return sortAndReturnMostRecentDate(prospectionHoleGroupEnumerable);
        }

        private bool ThereAreNoProspectionHoleGroups(IEnumerable<ProspectionHoleGroupDTO> prospectionHoleGroupEnumerable)
        {
            return prospectionHoleGroupEnumerable.Count() == 0;
        }

        private bool ThereIsOnlyOneProspectionHoleGroup(IEnumerable<ProspectionHoleGroupDTO> prospectionHoleGroupEnumerable) {
            return prospectionHoleGroupEnumerable.Count() == 1;
        }

        private ProspectionHoleGroupDTO sortAndReturnMostRecentDate(IEnumerable<ProspectionHoleGroupDTO> prospectionHoleGroupEnumerable) {
            IOrderedEnumerable<ProspectionHoleGroupDTO> holeGroupsOrderedByUpdateDateDescending = prospectionHoleGroupEnumerable.OrderByDescending(m => m.updatedAt);
            ProspectionHoleGroupDTO mostRecentHoleGroup = holeGroupsOrderedByUpdateDateDescending.FirstOrDefault();
            return mostRecentHoleGroup;
        }

        private ProspectionHoleGroup convertToProspectionHoleGroup(ProspectionHoleGroupDTO prospectionHoleGroupDTO)
        {
            return mapper.Map<ProspectionHoleGroupDTO, ProspectionHoleGroup>(prospectionHoleGroupDTO);
        }

        private List<ProspectionHoleGroupCalculation> getProspectionHoleGroupCalculations(ProspectionHoleGroupDTO prospectionHoleGroupDTO) {
            try
            {
                return getProspectionHoleGroupCalculationObjects(prospectionHoleGroupDTO);
                
            }
            catch (NullReferenceException e)
            {
                return getEmptyProspectionHoleGroupCalculations();
            }
        }

        private List<ProspectionHoleGroupCalculation> getProspectionHoleGroupCalculationObjects(ProspectionHoleGroupDTO prospectionHoleGroupDTO)
        {
            List<ProspectionHoleGroupCalculationDTO> prospectionHoleGroupCalculationsDTO = prospectionHoleGroupService.getProspectionHoleGroupCalculations(prospectionHoleGroupDTO);
            return convertToProspectionHoleGroupCalculations(prospectionHoleGroupCalculationsDTO);
        }

        private List<ProspectionHoleGroupCalculation> getEmptyProspectionHoleGroupCalculations()
        {
            return new List<ProspectionHoleGroupCalculation>();
        }

        private List<ProspectionHoleGroupCalculation> convertToProspectionHoleGroupCalculations(List<ProspectionHoleGroupCalculationDTO> prospectionHoleGroupCalculationsDTO)
        {
            return mapper.Map<
                List<ProspectionHoleGroupCalculationDTO>, 
                List<ProspectionHoleGroupCalculation>
            >(prospectionHoleGroupCalculationsDTO);
        }
    }
}
