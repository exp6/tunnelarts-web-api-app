﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TunelAPI.repository;
using TunnelAPI.repository.mapping;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.mapping
{
    public class MappingExporterService : IMappingExporterService
    {
        private readonly IMappingRepository mappingRepository;
        private readonly ICharacterizationService characterizationSevice;

        public MappingExporterService(IMappingRepository mappingRepository, ICharacterizationService characterizationService) {
            this.mappingRepository = mappingRepository;
            this.characterizationSevice = characterizationService;
        }
        
        public MemoryStream toExcel(Guid mappingId)
        {
            IWorkbook workbook = new XSSFWorkbook();            
            ISheet rockMassDescriptionSheet = workbook.CreateSheet("Rock Mass Description");
            ISheet rockMassClassificationSheet = workbook.CreateSheet("Rock Mass Classification");
            //ISheet graphicalRegisterSheet = workbook.CreateSheet("Graphical Register");
            ISheet generalCommentsSheet = workbook.CreateSheet("General Comments");
            var characterization = characterizationSevice.find();
            var mapping = mappingRepository.findOne(mappingId);

            fillOutRockMassDescription(rockMassDescriptionSheet, mapping, characterization);
            fillOutRockMassClassification(rockMassClassificationSheet, mapping, characterization);
            fillOutGeneralComments(generalCommentsSheet, mapping, characterization);

            for (int i = 0; i <= 13; i++)
            {
                rockMassDescriptionSheet.SetColumnWidth(i, (15 * 500));
            }

            for (int i = 0; i <= 6; i++)
            {
                generalCommentsSheet.SetColumnWidth(i, (15 * 500));
            }

            for (int i = 0; i <= 8; i++)
            {
                rockMassClassificationSheet.SetColumnWidth(i, (15 * 500));
            }

            MemoryStream memoryStream = new MemoryStream();
            workbook.Write(memoryStream);
            return new MemoryStream(memoryStream.ToArray());
        }

        
        private void fillOutRockMassDescription(ISheet sheet, Mapping mapping, CharacterizationDTO characterization)
        {
            var rockMass = mapping.RockMasses.FirstOrDefault();

            // Rock Mass Overview
            var geneticGroup = rockMass.GeneticGroup.Value == 0 ? "N/A" : characterization.GeneticGroups.FirstOrDefault(gg => gg.code == rockMass.GeneticGroup.Value).description;
            var rockName = rockMass.RockName.Value == 0 ? "N/A" : characterization.SubGroups.FirstOrDefault(gg => gg.groupGeneticCode == rockMass.GeneticGroup.Value && gg.code == rockMass.RockName.Value).description;
            var structure = rockMass.Structure.Value == 0 ? "N/A" : characterization.Structures.FirstOrDefault(est => est.code == rockMass.Structure.Value).description;
            var weathering = rockMass.Weathering.Value == 0 ? "N/A" : characterization.Weatherings.FirstOrDefault(gg => gg.code == rockMass.Weathering.Value).description;
            var colourValue = rockMass.ColorValue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 0 && col.code == rockMass.ColorValue.Value).description;
            var colourChroma = rockMass.ColorChroma.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 1 && col.code == rockMass.ColorChroma.Value).description;
            var colourHue = rockMass.ColorHue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 2 && col.code == rockMass.ColorHue.Value).description;
            var jointing = rockMass.Jointing.Value == 0 ? "N/A" : characterization.Jointings.FirstOrDefault(jtg => jtg.code == rockMass.Jointing.Value).description;
            var strength = rockMass.Strength.Value == 0 ? "N/A" : characterization.Jointings.FirstOrDefault(st => st.code == rockMass.Strength.Value).description;
            var water = rockMass.Water.Value == 0 ? "N/A" : characterization.Waters.FirstOrDefault(wt => wt.code == rockMass.Water.Value).description;
            var waterQuality = characterization.WaterQualities.FirstOrDefault(wq => wq.code == rockMass.WaterQuality.Value);
            

            int columnIndex = 0;
            var rockMassTitles = new string[] { "", "Genetic Group", "Rock Name", "Structure", "Weathering", "Colour Value", "Colour Chroma", "Colour Hue", "Jointing", "Strength", "Water", "Water T° [C°]", "Water Quality",
                "Inflow" };
            IRow titleRow = sheet.CreateRow(0);
            IRow valueRow = sheet.CreateRow(1);

            addTitles(titleRow, 0, rockMassTitles);
            addData(valueRow, columnIndex++, "Rock Mass Overview");
            addData(valueRow, columnIndex++, geneticGroup);
            addData(valueRow, columnIndex++, rockName);
            addData(valueRow, columnIndex++, structure);
            addData(valueRow, columnIndex++, weathering);
            addData(valueRow, columnIndex++, colourValue);
            addData(valueRow, columnIndex++, colourChroma);
            addData(valueRow, columnIndex++, colourHue);
            addData(valueRow, columnIndex++, jointing);
            addData(valueRow, columnIndex++, strength);
            addData(valueRow, columnIndex++, water);
            addData(valueRow, columnIndex++, rockMass.WaterT.GetValueOrDefault(0));
            addData(valueRow, columnIndex++, waterQuality.description);
            addData(valueRow, columnIndex++, rockMass.Inflow.Value);

            // Lithologies
            var rowIndex = 3;
            var lithologies = mapping.Lithologies.OrderBy(lth => lth.Position);
            
            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Lithologies");

            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue("Lithology " + (columnIndex-1));
            }

            //rowIndex++;
            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Presence:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.Presence.Value.ToString() + "%");
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Genetic group:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.GeneticGroup.Value == 0 ? "N/A" : characterization.GeneticGroups.FirstOrDefault(gg => gg.code == lithology.GeneticGroup.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Rock Name:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.SubGroup.Value == 0 ? "N/A" : characterization.SubGroups.FirstOrDefault(sg => sg.groupGeneticCode == lithology.GeneticGroup.Value && sg.code == lithology.SubGroup.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Formation/Unit:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue("N/A");
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Weathering:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.AlterationWeathering.Value == 0 ? "N/A" : characterization.AlterationsWeathering.FirstOrDefault(aw => aw.code == lithology.AlterationWeathering.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Colour value:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.ColourValue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 0 && col.code == lithology.ColourValue.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Colour chroma:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.ColourChroma.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 1 && col.code == lithology.ColourChroma.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Colour hue:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.ColorHue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(col => col.idColourType.GetValueOrDefault(-1) == 2 && col.code == lithology.ColorHue.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Strength [Mpa]:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.Strength.Value == 0 ? "N/A" : characterization.Jointings.FirstOrDefault(st => st.code == lithology.Strength.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Texture:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.Texture.Value == 0 ? "N/A" : characterization.Textures.FirstOrDefault(ttr => ttr.code == lithology.Texture.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Grain size [mm]:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.GrainSize.Value == 0 ? "N/A" : characterization.GrainSizes.FirstOrDefault(gs => gs.code == lithology.GrainSize.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("J(v) (joints/m3) min-max:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.JvMin + " - " + lithology.JvMax);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Block shape:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.BlockShape.Value == 0 ? "N/A" : characterization.BlockShapes.FirstOrDefault(gs => gs.code == lithology.BlockShape.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Size (LxWxH) [m]:");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.BlockSizeOne.Value + " x " + lithology.BlockSizeTwo.Value + " x " + lithology.BlockSizeThree.Value);
            }

            rowIndex++;
            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Brief description");
            foreach (var lithology in lithologies)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(lithology.briefDesc == null ? "N/A" : lithology.briefDesc.Trim());
            }

            rowIndex++;
            rowIndex++;

            //Discontinuities
            var discontinuities = mapping.Discontinuities.OrderBy(dst => dst.Position);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Discontinuities");

            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue("Discontinuity " + (columnIndex - 1));
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Type:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Type.Value == 0 ? "N/A" : characterization.DiscontinuityTypes.FirstOrDefault(e => e.code == discontinuity.Type.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Relevance:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Relevance.Value == 0 ? "N/A" : characterization.Relevances.FirstOrDefault(e => e.code == discontinuity.Relevance.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Orientation DD:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.OrientationDd.Value);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Orientation D:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.OrientationD.Value);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Spacing:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Spacing.Value == 0 ? "N/A" : characterization.Spacings.FirstOrDefault(e => e.code == discontinuity.Spacing.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Persistence:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Persistence.Value == 0 ? "N/A" : characterization.Persistences.FirstOrDefault(e => e.code == discontinuity.Persistence.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Opening:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Opening.Value == 0 ? "N/A" : characterization.Openings.FirstOrDefault(e => e.code == discontinuity.Opening.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Roughness:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Roughness.Value == 0 ? "N/A" : characterization.Roughnesses.FirstOrDefault(e => e.code == discontinuity.Roughness.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Slickensided:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Slickensided.Value == 0 ? "N/A" : characterization.Slickensideds.FirstOrDefault(e => e.code == discontinuity.Slickensided.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Infilling:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Infilling.Value == 0 ? "N/A" : characterization.Infillings.FirstOrDefault(e => e.code == discontinuity.Infilling.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Infilling Type:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.InfillingType.Value == 0 ? "N/A" : characterization.InfillingTypes.FirstOrDefault(e => e.code == discontinuity.InfillingType.Value).description);
            }

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Weathering:");
            foreach (var discontinuity in discontinuities)
            {
                valueRow.CreateCell(columnIndex++).SetCellValue(discontinuity.Weathering.Value == 0 ? "N/A" : characterization.Weatherings.FirstOrDefault(e => e.code == discontinuity.Weathering.Value).description);
            }

            rowIndex++;
            rowIndex++;

            // Additional Information
            ICellStyle boldStyle = sheet.Workbook.CreateCellStyle();
            boldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            boldStyle.FillPattern = FillPattern.SolidForeground;

            var additionalInformation = mapping.AdditionalInformations.FirstOrDefault();
            var specialFeature = additionalInformation.SpecialFeatures.FirstOrDefault();
            var rockMassHazard = additionalInformation.RockMassHazards.FirstOrDefault();

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Additional Information");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Special Features");
            valueRow.CreateCell(columnIndex++).SetCellValue("Rock Mass Hazard");

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            var cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Zeolites");
            if (specialFeature.Zeolites.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Rock Burst");
            if (rockMassHazard.RockBurst.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Clay");
            if (specialFeature.Clay.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Swelling");
            if (rockMassHazard.Swelling.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Chlorite");
            if (specialFeature.Chlorite.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Squeezing");
            if (rockMassHazard.Squeezing.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Red Tuff");
            if (specialFeature.RedTuff.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Slaking");
            if (rockMassHazard.Slaking.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            cell = valueRow.CreateCell(columnIndex++);
            cell.SetCellValue("Sulfates");
            if (specialFeature.Sulfates.GetValueOrDefault(false))
                cell.CellStyle = boldStyle;

            rowIndex++;
            rowIndex++;

            // Failure Zone
            var failureZone = mapping.FailureZones.FirstOrDefault();

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Fault Zone Description");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Sense of movement:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.SenseOfMovement.Value == 0 ? "N/A" : characterization.SenseOfMovement.FirstOrDefault(e => e.code == failureZone.SenseOfMovement.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Rake of striae:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.RakeOfStriae.Value);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Orientation DD:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.OrientationDd.Value);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Orientation D:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.OrientationD.Value);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Thickness:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.Thickness.Value);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Matrix/Block:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.MatrixBlock.Value);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Matrix colour value:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.MatrixColourValue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(e => e.idColourType == 0 && e.code == failureZone.MatrixColourValue.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Matrix colour chroma:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.MatrixColourChroma.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(e => e.idColourType == 1 && e.code == failureZone.MatrixColourChroma.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Matrix colour hue:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.MatrixColourHue.Value == 0 ? "N/A" : characterization.ColourTypeValues.FirstOrDefault(e => e.idColourType == 2 && e.code == failureZone.MatrixColourHue.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Matrix grain size [mm]:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.MatrixGainSize.Value == 0 ? "N/A" : characterization.GrainSizes.FirstOrDefault(e => e.code == failureZone.MatrixGainSize.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Block size:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.BlockSize.Value == 0 ? "N/A" : characterization.BlockSizes.FirstOrDefault(e => e.code == failureZone.BlockSize.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Block shape:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.BlockShape.Value == 0 ? "N/A" : characterization.BlockShapeTypes.FirstOrDefault(e => e.code == failureZone.BlockShape.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Block genetic group:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.BlockGeneticGroup.Value == 0 ? "N/A" : characterization.GeneticGroups.FirstOrDefault(e => e.code == failureZone.BlockGeneticGroup.Value).description);

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Block sub-group:");
            valueRow.CreateCell(columnIndex).SetCellValue(failureZone.BlockSubGroup.Value == 0 ? "N/A" : characterization.SubGroups.FirstOrDefault(e => e.groupGeneticCode == failureZone.BlockGeneticGroup.Value && e.code == failureZone.BlockSubGroup.Value).description);

            rowIndex++;

            var particularity = mapping.Particularities.FirstOrDefault();

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Particularities");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Instability");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Caused by:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.InstabilityCausedBy);

            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Location:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.InstabilityLocation);
            
            rowIndex++;

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Overbreak");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Caused by:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.OverbreakCausedBy);

            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Location Start:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.OverbreakLocationStart);

            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Location End:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.OverbreakLocationEnd);

            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Volume [m3]:");
            valueRow.CreateCell(columnIndex).SetCellValue(particularity.OverbreakVolume);
            
        }

        private void fillOutRockMassClassification(ISheet sheet, Mapping mapping, CharacterizationDTO characterization)
        {
            int rowIndex = 0;
            int columnIndex = 0;

            var qv = mapping.QValues.FirstOrDefault();

            IRow valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Q-Method (Barton, 2013)");

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Q Min");
            valueRow.CreateCell(columnIndex++).SetCellValue("Q Avg");
            valueRow.CreateCell(columnIndex++).SetCellValue("Q Max");

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.QMin.ToString());
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.QAvg.ToString());
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.QMax.ToString());

            rowIndex++;

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("RQD");
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.Rqd.ToString());

            valueRow = sheet.CreateRow(rowIndex++);
            columnIndex = 0;
            valueRow.CreateCell(columnIndex++).SetCellValue("Jn");
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.Jn.ToString());
            columnIndex++;
            valueRow.CreateCell(columnIndex++).SetCellValue("Type");
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.JnType == 2 ? "Portal" : (qv.JnType == 1 ? "Intersection" : "None"));

            List<QValueDiscontinuity> QValueDiscontinuities = new List<QValueDiscontinuity>();
            if (!String.IsNullOrEmpty(qv.id.ToString())) QValueDiscontinuities = qv.QValueDiscontinuities.ToList();

            rowIndex++;
            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            
            for (int i = 0; i < QValueDiscontinuities.Count; i++)
            {
                valueRow.CreateCell(i + 1).SetCellValue("Discontinuity " + (i + 1));
            }
            
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Jr");
            for (int i = 0; i < QValueDiscontinuities.Count; i++)
            {
                valueRow.CreateCell(i + 1).SetCellValue(QValueDiscontinuities.ElementAt(i).Jr.ToString());
            }

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Ja");
            for (int i = 0; i < QValueDiscontinuities.Count; i++)
            {
                valueRow.CreateCell(i + 1).SetCellValue(QValueDiscontinuities.ElementAt(i).Ja.ToString());
            }

            rowIndex++;
            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Jw");
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.Jw.ToString());

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("SRF");
            valueRow.CreateCell(columnIndex++).SetCellValue(qv.Srf.ToString());

            rowIndex++;
            columnIndex = 0;

            var rmr = mapping.Rmrs.FirstOrDefault();

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("RMR (Bieniawski, 1989)");

            rowIndex++;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Strength of intact rock material");
            valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Strength.ToString());

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Drill core quality RQD");
            valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Rqd.ToString());

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Spacing of discontinuities");
            valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Spacing.ToString());

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Groundwater");
            valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Groundwater.ToString());

            columnIndex = 0;
            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex++).SetCellValue("Orientation of discontinuities");
            valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Orientation.ToString());

            rowIndex++;
            columnIndex = 0;

            valueRow = sheet.CreateRow(rowIndex++);
            valueRow.CreateCell(columnIndex).SetCellValue("Condition of discontinuities");

            if (!(rmr.conditionEnabled ?? true))
            {
                valueRow.CreateCell(++columnIndex).SetCellValue(rmr.Orientation.ToString());
            }
            else
            {
                valueRow = sheet.CreateRow(rowIndex++);
                valueRow.CreateCell(columnIndex++).SetCellValue("Discontinuity length (persistence)");
                valueRow.CreateCell(columnIndex++).SetCellValue("Separation (aperture)");
                valueRow.CreateCell(columnIndex++).SetCellValue("Roughness");
                valueRow.CreateCell(columnIndex++).SetCellValue("Infilling (gouge)");
                valueRow.CreateCell(columnIndex++).SetCellValue("Weathering");

                columnIndex = 0;
                valueRow = sheet.CreateRow(rowIndex++);
                valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Persistence.ToString());
                valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Opening.ToString());
                valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Roughness.ToString());
                valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Infilling.ToString());
                valueRow.CreateCell(columnIndex++).SetCellValue(rmr.Weathering.ToString());
            }


        }

        private void fillOutGeneralComments(ISheet sheet, Mapping mapping, CharacterizationDTO characterization)
        {
            var additionalDescription = mapping.AdditionalDescriptions.FirstOrDefault();

            // Mapping Conditions
            var distance = additionalDescription.DistanceToFace.Value == 0 ? "N/A" : characterization.FaceTypes.FirstOrDefault(e => e.code == additionalDescription.DistanceToFace.Value).description;
            var responsible = additionalDescription.ResponsibleForRestrictedArea.Value == 0 ? "N/A" : characterization.Responsibles.FirstOrDefault(e => e.code == additionalDescription.ResponsibleForRestrictedArea.Value).description;
            var reason = additionalDescription.ReasonForRestrictedArea.Value == 0 ? "N/A" : characterization.Reasons.FirstOrDefault(e => e.code == additionalDescription.ReasonForRestrictedArea.Value).description;
            var lightQuality = additionalDescription.LightQuality.Value == 0 ? "N/A" : characterization.Lights.FirstOrDefault(e => e.code == additionalDescription.LightQuality.Value).description;
            var airQuality = additionalDescription.AirQuality.Value == 0 ? "N/A" : characterization.Airs.FirstOrDefault(e => e.code == additionalDescription.AirQuality.Value).description;

            var mappingConditionTitles = new string[] { "", "Distance", "Responsible for Restricted Area", "Reason for Restricted Area",
                "Light Quality", "Air Quality", "Available Time" };

            int rowIndex = 0;            
            IRow mappingConditionsRow = sheet.CreateRow(rowIndex++);

            int columnIndex = 0;
            addTitles(mappingConditionsRow, columnIndex, mappingConditionTitles);

            mappingConditionsRow = sheet.CreateRow(rowIndex++);
            addData(mappingConditionsRow, columnIndex++, "Mapping Conditions");            
            addData(mappingConditionsRow, columnIndex++, distance);
            addData(mappingConditionsRow, columnIndex++, responsible);
            addData(mappingConditionsRow, columnIndex++, reason);
            addData(mappingConditionsRow, columnIndex++, lightQuality);
            addData(mappingConditionsRow, columnIndex++, airQuality);
            addData(mappingConditionsRow, columnIndex++, additionalDescription.AvailableTime);            

            // Other Information
            var damage = additionalDescription.NewDamageBehind.Value == 0 ? "N/A" : characterization.Damages.FirstOrDefault(e => e.code == additionalDescription.NewDamageBehind.Value).description;
            
            var otherInformationTitles = new string[] { "", "New Damage 40[m] behind", "Description" };

            rowIndex++;
            IRow otherInformationRow = sheet.CreateRow(rowIndex++);

            columnIndex = 0;
            addTitles(otherInformationRow, columnIndex, otherInformationTitles);

            otherInformationRow = sheet.CreateRow(rowIndex++);
            addData(otherInformationRow, columnIndex++, "Other Information");
            addData(otherInformationRow, columnIndex++, damage);
            addData(otherInformationRow, columnIndex++, additionalDescription.Description);            
        }

        private void addTitles(IRow titleRow, int startIndex, string[] titles)
        {
            var index = startIndex;

            foreach (string title in titles)
                titleRow.CreateCell(index++).SetCellValue(title);
        }

        private void addData(IRow valueRow, int columnIndex, string description)
        {            
            valueRow.CreateCell(columnIndex).SetCellValue(description.Trim());
        }

        private void addData(IRow valueRow, int columnIndex, double value)
        {
            valueRow.CreateCell(columnIndex).SetCellValue(value);
        }

        //public static byte[] createPDF(string html)
        //{
        //    MemoryStream msOutput = new MemoryStream();
        //    TextReader reader = new StringReader(html);

        //    // step 1: creation of a document-object
        //    Document document = new Document(PageSize.A4, 30, 30, 30, 30);

        //    // step 2:
        //    // we create a writer that listens to the document
        //    // and directs a XML-stream to a file
        //    PdfWriter writer = PdfWriter.GetInstance(document, msOutput);

        //    // step 3: we create a worker parse the document
        //    HTMLWorker worker = new HTMLWorker(document);

        //    // step 4: we open document and start the worker on the document
        //    document.Open();
        //    worker.StartDocument();

        //    // step 5: parse the html into the document
        //    worker.Parse(reader);

        //    // step 6: close the document and the worker
        //    worker.EndDocument();
        //    worker.Close();
        //    document.Close();
        //    msOutput.Flush();
        //    return msOutput.ToArray();
        //}
    }
}
