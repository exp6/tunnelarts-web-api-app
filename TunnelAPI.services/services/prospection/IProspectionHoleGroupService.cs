﻿using System;
using System.Collections.Generic;
using TunnelAPI.services.model;
using TunelAPI.repository.model;

namespace TunnelAPI.services.services.prospection
{
    public interface IProspectionHoleGroupService
    {
        ProspectionHoleGroupDTO findOne(Guid prospectionHoleGroupId);
        IEnumerable<ProspectionHoleGroupDTO> find(Guid faceId, ProspectionHoleGroupFilter filter);
        int findAmount(Guid faceId, ProspectionHoleGroupFilter filter);
        List<ForecastCalculationDTO> getCalculations(Guid faceId);
        List<ProspectionHoleGroupCalculationDTO> getProspectionHoleGroupCalculations(ProspectionHoleGroupDTO prospectionHoleGroupDTO);
    }
}
