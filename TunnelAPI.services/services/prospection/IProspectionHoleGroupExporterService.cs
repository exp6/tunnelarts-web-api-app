﻿using System;
using System.IO;

namespace TunnelAPI.services.services.prospection
{
    public interface IProspectionHoleGroupExporterService
    {
        MemoryStream toExcel(Guid faceId);
    }
}
