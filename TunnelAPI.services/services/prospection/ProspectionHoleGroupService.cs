﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository.repository.prospectionholegroup;
using TunnelAPI.services.model;
using TunelAPI.repository;
using TunelAPI.repository.model;
using TunnelAPI.repository.model;

namespace TunnelAPI.services.services.prospection
{
    public class ProspectionHoleGroupService : IProspectionHoleGroupService
    {
        private IProspectionHoleGroupRepository prospectionHoleGroupRepository;
        private IMapper mapper;

        public ProspectionHoleGroupService(IMapper mapper, IProspectionHoleGroupRepository prospectionHoleGroupRepository)
        {
            this.prospectionHoleGroupRepository = prospectionHoleGroupRepository;
            this.mapper = mapper;
        }

        public ProspectionHoleGroupDTO findOne(Guid prospectionHoleGroupId)
        {
            var prospectionHoleGroup = prospectionHoleGroupRepository.findOne(prospectionHoleGroupId);
            return mapper.Map<ProspectionHoleGroup, ProspectionHoleGroupDTO>(prospectionHoleGroup);
        }

        public IEnumerable<ProspectionHoleGroupDTO> find(Guid faceId, ProspectionHoleGroupFilter filter)
        {
            var prospectionHoleGroups = prospectionHoleGroupRepository.find(faceId, filter)
                .Select(x => new ProspectionHoleGroupDTO {
                    id = x.id,
                    Name = x.Name,
                    QValueSelected = x.QValueSelected ?? 0.0,
                    User = new UserDTO { FirstName = x.User.FirstName, LastName = x.User.LastName },
                    Mapping = new MappingDTO { id = x.IdMapping ?? Guid.Empty, ChainageStart = x.Mapping.ChainageStart, ChainageEnd = x.Mapping.ChainageEnd },
                    createdAt = x.createdAt,
                    updatedAt = x.updatedAt,
                    deleted = x.deleted,
                    probeHolesCount = prospectionHoleGroupRepository.findAmountProbeHoles(x.id),
                    rodsCount = prospectionHoleGroupRepository.findAmountRods(x.id)
                });

            return prospectionHoleGroups;
        }

        public int findAmount(Guid faceId, ProspectionHoleGroupFilter filter)
        {
            var total = prospectionHoleGroupRepository.findAmount(faceId, filter);
            return total;
        }

        public List<ForecastCalculationDTO> getCalculations(Guid faceId)
        {
            IEnumerable<MappingDTO> mappings = prospectionHoleGroupRepository.findMappings(faceId)
                .Select(x => new MappingDTO {
                    id = x.id,
                    ChainageStart = x.ChainageStart,
                    ChainageEnd = x.ChainageEnd
                });

            IEnumerable<ProspectionHoleGroup> phgs = prospectionHoleGroupRepository.find(faceId);

            List<QValue> QObservedList = new List<QValue>();

            Dictionary<MappingDTO, List<ProspectionHoleGroupDTO>> phgsPerMapping =  new Dictionary<MappingDTO, List<ProspectionHoleGroupDTO>>();
            List<ProspectionHoleGroupDTO> phgsAux;
            foreach (MappingDTO mapping in mappings)
            {
                phgsAux = getPhgsPerMapping(mapping, phgs);
                phgsPerMapping.Add(mapping, phgsAux);
                IEnumerable<QValue> qvalues = prospectionHoleGroupRepository.findQObserved(mapping.id);
                QObservedList.Add(qvalues.FirstOrDefault());
            }

            List<ForecastCalculationDTO> forecastCalculations = new List<ForecastCalculationDTO>();

            int index = 0;

            foreach (KeyValuePair<MappingDTO, List<ProspectionHoleGroupDTO>> entry in phgsPerMapping)
            {
                List<ProspectionHoleGroupDTO> phgList = entry.Value;

                IEnumerable<ProspectionHoleGroupCalculation> calculations;
                List<ProspectionHoleGroupCalculation> avgs = new List<ProspectionHoleGroupCalculation>();
                foreach (ProspectionHoleGroupDTO phg in phgList)
                {
                    calculations = prospectionHoleGroupRepository.findCalculations(phg.id);
                    avgs.Add(getAvgCalculations(calculations));
                }

                ProspectionHoleGroupCalculation predicted = getAvgCalculations(avgs);

                ProspectionHoleGroupCalculationDTO predictedDTO = new ProspectionHoleGroupCalculationDTO()
                {
                    QMax = predicted.QMax ?? 0.0,
                    QMin = predicted.QMin ?? 0.0,
                    QAvg = predicted.QAvg ?? 0.0
                };

                QValue qvalue = QObservedList.ElementAt(index);

                ProspectionHoleGroupCalculationDTO observedDTO = new ProspectionHoleGroupCalculationDTO()
                {
                    QMax = qvalue.QMax ?? 0.0,
                    QMin = qvalue.QMin ?? 0.0,
                    QAvg = qvalue.QAvg ?? 0.0
                };

                ForecastCalculationDTO NewForecastCalc = new ForecastCalculationDTO
                {
                    mapping = entry.Key,
                    predicted = predictedDTO,
                    observed = observedDTO
                };

                forecastCalculations.Add(NewForecastCalc);
                ++index;
            }

            return forecastCalculations;
        }

        private ProspectionHoleGroupCalculation getAvgCalculations(IEnumerable<ProspectionHoleGroupCalculation> calculations)
        {
            ProspectionHoleGroupCalculation result;

            double QMaxAvg = 0.0;
            double QMinAvg = 0.0;
            double QAvgAvg = 0.0;

            double total = calculations.Count() == 0 ? 1.0 : calculations.Count();

            foreach (ProspectionHoleGroupCalculation phgc in calculations)
            {
                QMaxAvg += phgc.QMax ?? 0.0;
                QMinAvg += phgc.QMax ?? 0.0;
                QAvgAvg += phgc.QMax ?? 0.0;
            }

            result = new ProspectionHoleGroupCalculation()
            {
                QMax = QMaxAvg / total,
                QMin = QMinAvg / total,
                QAvg = QAvgAvg / total
            };

            return result;
        }

        private List<ProspectionHoleGroupDTO> getPhgsPerMapping(MappingDTO mapping, IEnumerable<ProspectionHoleGroup> phgs)
        {
            List<ProspectionHoleGroupDTO> phgsAux = new List<ProspectionHoleGroupDTO>();

            foreach (ProspectionHoleGroup phg in phgs)
            {
                if (phg.IdMapping == mapping.id)
                {
                    phgsAux.Add(new ProspectionHoleGroupDTO {
                        id = phg.id
                    });
                }
            }

            return phgsAux;
        }

        public List<ProspectionHoleGroupCalculationDTO> getProspectionHoleGroupCalculations(ProspectionHoleGroupDTO prospectionHoleGroup) {
            IEnumerable<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculationEnumerable = prospectionHoleGroupRepository.findCalculations(prospectionHoleGroup.id);
            List<ProspectionHoleGroupCalculation> returnList = prospectionHoleGroupCalculationEnumerable.ToList();
            return mapper.Map<List<ProspectionHoleGroupCalculation>, List<ProspectionHoleGroupCalculationDTO>>(returnList);
        }
    }
}
