﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using TunelAPI.repository.repository.prospectionholegroup;
using TunelAPI.repository;
using System.Globalization;
using NPOI.SS.UserModel.Charts;
using NPOI.SS.Util;
using NPOI.OpenXmlFormats.Dml.Chart;
using NPOI.OpenXmlFormats.Shared;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.prospection
{
    public class ProspectionHoleGroupExporterService : IProspectionHoleGroupExporterService
    {
        private readonly IProspectionHoleGroupRepository prospectionHoleGroupRepository;
        private readonly IProspectionHoleGroupService prospectionHoleGroupSevice;

        public ProspectionHoleGroupExporterService(IProspectionHoleGroupRepository prospectionHoleGroupRepository, IProspectionHoleGroupService prospectionHoleGroupSevice)
        {
            this.prospectionHoleGroupRepository = prospectionHoleGroupRepository;
            this.prospectionHoleGroupSevice = prospectionHoleGroupSevice;
        }

        public MemoryStream toExcel(Guid faceId)
        {
            
            IWorkbook workbook = new XSSFWorkbook();
            ISheet forecastSummarySheet = workbook.CreateSheet("Forecast Summary");
            fillForecastSummarySheet(forecastSummarySheet, faceId);

            ICellStyle cs = workbook.CreateCellStyle();

            IEnumerable<ProspectionHoleGroup> phgs = prospectionHoleGroupRepository.find(faceId);
            ISheet phgSheet;
            foreach (ProspectionHoleGroup phg in phgs)
            {
                phgSheet = workbook.CreateSheet(phg.Name);
                fillPhgSheet(phgSheet, phg);
            }

            MemoryStream memoryStream = new MemoryStream();
            workbook.Write(memoryStream);
            return new MemoryStream(memoryStream.ToArray());
        }

        private int generateNewSummary(ISheet sheet, ICell c, IRow row, int index, int NUM_OF_COLUMNS, List<ForecastCalculationDTO> forecastData, int valueType)
        {
            string valueTypeStr = "(Average Values)";

            switch (valueType)
            {
                case 1:
                    valueTypeStr = "(Minimun Values)";
                    break;
                case 2:
                    valueTypeStr = "(Maximum Values)";
                    break;
            }

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberGroupSeparator = "+";
            nfi.NumberDecimalSeparator = ",";
            nfi.NumberDecimalDigits = 2;

            row = sheet.CreateRow(index);
            c = row.CreateCell(0);
            c.SetCellValue("Chainages");
            MappingDTO mapping;
            for (int colIndex = 1; colIndex <= NUM_OF_COLUMNS; colIndex++)
            {
                mapping = forecastData.ElementAt(colIndex - 1).mapping;
                c = row.CreateCell((short)colIndex);
                c.SetCellValue(mapping.ChainageStart.Value.ToString(nfi) + " - " + mapping.ChainageEnd.Value.ToString(nfi));
            }

            ++index;

            ProspectionHoleGroupCalculationDTO predicted;

            row = sheet.CreateRow(index);
            c = row.CreateCell(0);
            c.SetCellValue("Observed Q " + valueTypeStr);
            ProspectionHoleGroupCalculationDTO observed;
            for (int colIndex = 1; colIndex <= NUM_OF_COLUMNS; colIndex++)
            {
                observed = forecastData.ElementAt(colIndex - 1).observed;
                c = row.CreateCell((short)colIndex);

                switch (valueType)
                {
                    case 1:
                        c.SetCellValue(observed.QMin.ToString());
                        break;
                    case 2:
                        c.SetCellValue(observed.QMax.ToString());
                        break;
                    default:
                        c.SetCellValue(observed.QAvg.ToString());
                        break;
                }
            }

            ++index;

            row = sheet.CreateRow(index);
            c = row.CreateCell(0);
            c.SetCellValue("Predicted Q " + valueTypeStr);

            for (int colIndex = 1; colIndex <= NUM_OF_COLUMNS; colIndex++)
            {
                predicted = forecastData.ElementAt(colIndex - 1).predicted;
                c = row.CreateCell((short)colIndex);

                switch (valueType)
                {
                    case 1:
                        c.SetCellValue(predicted.QMin.ToString());
                        break;
                    case 2:
                        c.SetCellValue(predicted.QMax.ToString());
                        break;
                    default:
                        c.SetCellValue(predicted.QAvg.ToString());
                        break;
                }
            }

            IDrawing drawing = sheet.CreateDrawingPatriarch();
            IClientAnchor anchor = drawing.CreateAnchor(0, 0, 0, 0, 1, index + 2, 5, index + 12);

            IChart chart = drawing.CreateChart(anchor);

            IChartLegend legend = chart.GetOrCreateLegend();
            legend.Position = LegendPosition.TopRight;

            ILineChartData<string, double> data = chart.ChartDataFactory.CreateLineChartData<string, double>();

            IChartAxis bottomAxis = chart.ChartAxisFactory.CreateCategoryAxis(AxisPosition.Bottom);
            IValueAxis leftAxis = chart.ChartAxisFactory.CreateValueAxis(AxisPosition.Left);
            leftAxis.Crosses = AxisCrosses.AutoZero;
            leftAxis.Minimum = 0.0;

            IChartDataSource<string> xs = DataSources.FromStringCellRange(sheet, new CellRangeAddress(index - 2, index - 2, 1, NUM_OF_COLUMNS));
            IChartDataSource<double> ys1 = DataSources.FromNumericCellRange(sheet, new CellRangeAddress(index - 1, index - 1, 1, NUM_OF_COLUMNS));
            IChartDataSource<double> ys2 = DataSources.FromNumericCellRange(sheet, new CellRangeAddress(index, index, 1, NUM_OF_COLUMNS));

            var s1 = data.AddSeries(xs, ys1);
            s1.SetTitle("Observed Q");
            var s2 = data.AddSeries(xs, ys2);
            s2.SetTitle("Predicted Q");

            chart.Plot(data, bottomAxis, leftAxis);

            return index + 13;
        }

        private void fillForecastSummarySheet(ISheet sheet, Guid faceId)
        {
            IRow row = sheet.CreateRow(0);
            ICell c = row.CreateCell(0);
            c.SetCellValue("Forecast Summary");

            List<ForecastCalculationDTO> forecastData = prospectionHoleGroupSevice.getCalculations(faceId);

            int NUM_OF_COLUMNS = forecastData.Count;
            
            for (int i = 0; i <= NUM_OF_COLUMNS; i++)
            {
                sheet.SetColumnWidth(i, (15 * 500));
            }
            
            int index = 2;

            for (int i = 0; i < 3; i++)
            {
                index = generateNewSummary(sheet, c, row, index, NUM_OF_COLUMNS, forecastData, i);
            }
        }

        private void CreateChartData(ISheet sheet, int index)
        {
            
        }

        private void fillPhgSheet(ISheet sheet, ProspectionHoleGroup phg)
        {
            IRow row = sheet.CreateRow(0);
            ICell c = row.CreateCell(0);
            c.SetCellValue("Prospection Hole Group");

            sheet.SetColumnWidth(0, (15 * 500));
            sheet.SetColumnWidth(1, (15 * 500));
            sheet.SetColumnWidth(2, (15 * 500));
            sheet.SetColumnWidth(3, (15 * 500));
            sheet.SetColumnWidth(4, (15 * 500));
            sheet.SetColumnWidth(5, (15 * 500));
            sheet.SetColumnWidth(6, (15 * 500));
            sheet.SetColumnWidth(7, (15 * 500));
            sheet.SetColumnWidth(8, (15 * 500));
            sheet.SetColumnWidth(9, (15 * 500));
            sheet.SetColumnWidth(10, (15 * 500));
            sheet.SetColumnWidth(11, (15 * 500));

            Mapping mapping = prospectionHoleGroupRepository.findMapping(phg.IdMapping ?? Guid.Empty);

            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberGroupSeparator = "+";
            nfi.NumberDecimalSeparator = ",";
            nfi.NumberDecimalDigits = 2;

            row = sheet.CreateRow(2);
            c = row.CreateCell(0);
            c.SetCellValue("Chainages");
            c = row.CreateCell(1);
            c.SetCellValue(mapping.ChainageStart.Value.ToString(nfi) + " - " + mapping.ChainageEnd.Value.ToString(nfi));

            row = sheet.CreateRow(3);
            c = row.CreateCell(0);
            c.SetCellValue("Q Value (observed)");
            c = row.CreateCell(1);
            c.SetCellValue(phg.QValueSelected.ToString());

            row = sheet.CreateRow(5);
            c = row.CreateCell(0);
            c.SetCellValue("Prospection Holes");

            IEnumerable<ProspectionHole> phs = phg.ProspectionHoles.Where(x => x.deleted == false);
            int index = 7;

            for (int i = 0; i < phs.Count(); i++)
            {
                row = sheet.CreateRow(index);
                c = row.CreateCell(0);
                c.SetCellValue(phs.ElementAt(i).Name);

                row = sheet.CreateRow(++index);
                c = row.CreateCell(0);
                c.SetCellValue("Angle (°):");
                c = row.CreateCell(1);
                c.SetCellValue(phs.ElementAt(i).Angle.ToString());

                ++index;

                row = sheet.CreateRow(++index);
                c = row.CreateCell(0);
                c.SetCellValue("Rods");
                c = row.CreateCell(1);
                c.SetCellValue("Time [min]");
                c = row.CreateCell(2);
                c.SetCellValue("Average Pressure [Bar]");
                c = row.CreateCell(3);
                c.SetCellValue("Drilled Length [m]");
                c = row.CreateCell(4);
                c.SetCellValue("Speed [m/min]");
                c = row.CreateCell(5);
                c.SetCellValue("Water color");
                c = row.CreateCell(6);
                c.SetCellValue("Q Predicted");

                IEnumerable<Rod> rods = phs.ElementAt(i).Rods.Where(x => x.deleted == false);

                for (int j = 0; j < rods.Count(); j++)
                {
                    row = sheet.CreateRow(++index);
                    c = row.CreateCell(0);
                    c.SetCellValue(j + 1);
                    c = row.CreateCell(1);
                    c.SetCellValue(rods.ElementAt(j).time.ToString());
                    c = row.CreateCell(2);
                    c.SetCellValue(rods.ElementAt(j).AveragePressure.ToString());
                    c = row.CreateCell(3);
                    c.SetCellValue(rods.ElementAt(j).DrilledLength.ToString());
                    c = row.CreateCell(4);
                    c.SetCellValue(rods.ElementAt(j).Speed.ToString());
                    c = row.CreateCell(5);
                    c.SetCellValue(rods.ElementAt(j).WaterColor);
                    c = row.CreateCell(6);
                    c.SetCellValue(rods.ElementAt(j).QValue.ToString());
                }

                index += 2;
            }
        }
    }
}
