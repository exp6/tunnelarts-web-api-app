﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.services.prospection.aggregator.exporter
{
    public static class JsonProspectionExporter
    {
        public static string serialize(ProspectionAggregator prospectionAggregator) {
            return JsonConvert.SerializeObject(prospectionAggregator);
        }
    }
}
