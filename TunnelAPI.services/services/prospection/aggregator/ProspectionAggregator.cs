﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;

namespace TunnelAPI.services.services.prospection.aggregator
{
    public class ProspectionAggregator
    {
        public List<ProspectionFaceAggregator> ProspectionFaceAggregators { get; }

        public ProspectionAggregator(Tunnel tunnel) {
            ProspectionFaceAggregators = new List<ProspectionFaceAggregator>();
            Project project = tunnel.Project;

            foreach (Face face in tunnel.Faces) {
                Mapping mapping = findLatestMapping(face);
                if (mapping == null || mapping.ChainageStart == null) {
                    continue;
                }
                ProspectionFaceAggregator prospectionFaceAggregator = new ProspectionFaceAggregator(face, mapping, project);
                ProspectionFaceAggregators.Add(prospectionFaceAggregator);
            }
        }

        private Mapping findLatestMapping(Face face) {
            int direction = face.Direction.Value;
            ICollection<Mapping> mappings = face.Mappings;
            IOrderedEnumerable<Mapping> orderedMappings =
                (direction == 0) ? mappings.OrderByDescending(m => m.ChainageStart)
                : orderedMappings = mappings.OrderBy(m => m.ChainageStart);
            orderedMappings.DefaultIfEmpty(new Mapping());
            return orderedMappings.FirstOrDefault();
        }

        public ProspectionFaceAggregator findFaceAggregator(string id) {
            foreach (ProspectionFaceAggregator prospectionFaceAggregator in ProspectionFaceAggregators) {
                if (idsAreEqual(id, prospectionFaceAggregator)) {
                    return prospectionFaceAggregator;
                }
            }
            throw new IndexOutOfRangeException("No existe el face con id " + id);
        }

        private bool idsAreEqual(string id, ProspectionFaceAggregator prospectionFaceAggregator) {
            return prospectionFaceAggregator.id.ToLower() == id.ToLower();
        }
    }

    public class ProspectionFaceAggregator
    {
        public decimal PK { get; }
        public string FaceName { get; }
        public string id { get; }
        public List<ProspectionRodAggregator> ProspectionRodAggregators { get; }

        public ProspectionFaceAggregator(Face face, Mapping mapping, Project project)
        {
            this.PK = mapping.ChainageStart.Value;
            this.FaceName = face.Name;
            this.id = face.id.ToString();
            ProspectionRodAggregators = new List<ProspectionRodAggregator>();

            List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = getOrderedListOfProspectionHoleGroupCalculations(mapping);

            performAggregationOfProspections(mapping, project);
        }

        private void performAggregationOfProspections(Mapping mapping, Project project)
        {
            List<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = getOrderedListOfProspectionHoleGroupCalculations(mapping);
            decimal currentDistance = (decimal)mapping.ChainageStart;
            ICollection<RockQuality> rockQualities = project.RockQualities;
            foreach (ProspectionHoleGroupCalculation prospectionHoleGroupCalculation in prospectionHoleGroupCalculations)
            {
                ProspectionRodAggregator prospectionRodAggregator = createRodAggregator(prospectionHoleGroupCalculation, rockQualities, currentDistance);
                ProspectionRodAggregators.Add(prospectionRodAggregator);
                currentDistance += (decimal)prospectionHoleGroupCalculation.ProjectedDistance;
            }
        }

        private List<ProspectionHoleGroupCalculation> getOrderedListOfProspectionHoleGroupCalculations(Mapping mapping)
        {
            ProspectionHoleGroup prospectionHoleGroup = mapping.ProspectionHoleGroups.First();
            ICollection<ProspectionHoleGroupCalculation> prospectionHoleGroupCalculations = prospectionHoleGroup.ProspectionHoleGroupCalculations;
            IOrderedEnumerable<ProspectionHoleGroupCalculation> orderedProspectionHoleGroupCalculations = prospectionHoleGroupCalculations.OrderBy(phgc => phgc.RodPosition);
            return orderedProspectionHoleGroupCalculations.ToList();
        }

        private ProspectionRodAggregator createRodAggregator(
            ProspectionHoleGroupCalculation prospectionHoleGroupCalculation,
            ICollection<RockQuality> rockQualities,
            decimal currentDistance)
        {
            RockQuality rockQuality = findRockQualityName(prospectionHoleGroupCalculation, rockQualities);
            decimal projectedDistance = currentDistance + (decimal)prospectionHoleGroupCalculation.ProjectedDistance;
            ProspectionRodAggregator prospectionRodAggregator = new ProspectionRodAggregatorBuilder() {
                RodPosition = (int)prospectionHoleGroupCalculation.RodPosition,
                RockQualityName = rockQuality.Name.Trim(),
                Distance = projectedDistance,
                LowerBound = (double)rockQuality.LowerBound }
            .build();
            return prospectionRodAggregator;

        }

        private RockQuality findRockQualityName(ProspectionHoleGroupCalculation prospectionHoleGroupCalculation, ICollection<RockQuality> rockQualities)
        {
            double qAvg = prospectionHoleGroupCalculation.QAvg.Value;
            foreach (RockQuality rockQuality in rockQualities)
            {
                if (rockQuality.UpperBound > qAvg && qAvg >= rockQuality.LowerBound)
                {
                    return rockQuality;
                }
            }
            throw new IndexOutOfRangeException("No existe una calidad de roca para el valor Q " + qAvg);
        }

        public ProspectionRodAggregator findRodAggregator(int rodPosition)
        {
            foreach (ProspectionRodAggregator rodAggregator in ProspectionRodAggregators)
            {
                if (rodAggregator.RodPosition == rodPosition)
                {
                    return rodAggregator;
                }
            }
            throw new IndexOutOfRangeException("No existe la posición de rod " + rodPosition);
        }
    }

    public class ProspectionRodAggregatorBuilder
    {
        public int? RodPosition { get; set; }
        public string RockQualityName { get; set; }
        public decimal? Distance { get; set; }
        public double? LowerBound { get; set; }

        public ProspectionRodAggregator build() {
            if (notAllFieldsAreSet()) {
                throw new ArgumentNullException("Uno o más argumentos no han sido inicializados.");
            }

            return new ProspectionRodAggregator(this);
        }

        private bool notAllFieldsAreSet() {
            return RodPosition == null
                || RockQualityName == null
                || Distance == null
                || LowerBound == null;
        }
    }

    public class ProspectionRodAggregator
    {
        public int RodPosition { get; }
        public string RockQualityName { get; }
        public decimal Distance { get; }
        public double LowerBound { get; }

        public ProspectionRodAggregator(ProspectionRodAggregatorBuilder builder) {
            RodPosition = builder.RodPosition.Value;
            RockQualityName = builder.RockQualityName;
            Distance = builder.Distance.Value;
            LowerBound = builder.LowerBound.Value;
        }
    }
}
