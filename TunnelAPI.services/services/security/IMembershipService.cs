﻿using System.Web.Security;

namespace TunnelAPI.services.services.security
{
    public interface IMembershipService
    {
        bool ValidateUser(string username, string password);
        int MinPasswordLength();        
        MembershipCreateStatus CreateUser(string userName, string password, string email);
        bool ChangePassword(string userName, string oldPassword, string newPassword);
        MembershipUser GetUser(string username, bool userIsOnline);
        string ResetPassword(string username, string passwordAnswer);
    }
}
