﻿using System;
using System.Web.Security;

namespace TunnelAPI.services.services.security
{
    public class MembershipService : IMembershipService
    {
        private readonly MembershipProvider membershipProvider;

        public MembershipService()
        {
            this.membershipProvider = Membership.Provider;
        }        

        public int MinPasswordLength()
        {
            return membershipProvider.MinRequiredPasswordLength;
        }

        public bool ValidateUser(string username, string password)
        {
            return true;
            //return membershipProvider.ValidateUser(username, password);
        }

        public MembershipCreateStatus CreateUser(string userName, string password, string email)
        {
            MembershipCreateStatus status;
            membershipProvider.CreateUser(userName, password, email, null, null, true, null, out status);
            return status;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {           
            try
            {
                MembershipUser currentUser = membershipProvider.GetUser(userName, true);
                return currentUser.ChangePassword(oldPassword, newPassword);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

        public MembershipUser GetUser(string username, bool userIsOnline)
        {
            return membershipProvider.GetUser(username.Trim().ToLower(), userIsOnline);
        }

        public string ResetPassword(string username, string passwordAnswer)
        {
            return membershipProvider.ResetPassword(username.Trim().ToLower(), passwordAnswer);
        }        
    }
}
