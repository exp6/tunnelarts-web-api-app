﻿using AutoMapper;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.repository.tunnelprofile;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.tunnelprofile
{
    public class TunnelProfileService : ITunnelProfileService
    {
        private readonly ITunnelProfileRepository tunnelProfileRepository;
        private readonly IMapper mapper;

        public TunnelProfileService(IMapper mapper, ITunnelProfileRepository tunnelProfileRepository)
        {
            this.mapper = mapper;
            this.tunnelProfileRepository = tunnelProfileRepository;
        }

        public ICollection<TunnelProfileDTO> find()
        {
            var tunnelProfiles = tunnelProfileRepository.find();
            return mapper.Map<ICollection<TunnelProfile>, ICollection<TunnelProfileDTO>>(tunnelProfiles);
        }
    }
}
