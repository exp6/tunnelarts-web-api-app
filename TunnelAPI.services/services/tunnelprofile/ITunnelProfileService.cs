﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.tunnelprofile
{
    public interface ITunnelProfileService
    {
        ICollection<TunnelProfileDTO> find();
    }
}
