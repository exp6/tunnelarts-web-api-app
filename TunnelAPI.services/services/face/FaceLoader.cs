﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository;
using TunnelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.mapping;

namespace TunnelAPI.services.services.face
{
    public class FaceLoader
    {
        private IMappingService mappingService;
        private IMapper mapper;
        private MappingLoader mappingLoader;

        public FaceLoader(MappingLoader mappingLoader, IMappingService mappingService, IMapper mapper) {
            this.mappingLoader = mappingLoader;
            this.mappingService = mappingService;
            this.mapper = mapper;
        }

        public Face buildFace(Face face) {
            ICollection<Mapping> mappings = findAllMappings(face);
            List<Mapping> mappingsToAdd = new List<Mapping>();
            foreach (Mapping mapping in mappings) {
                Mapping mappingToAdd = mappingLoader.buildProspectionsForMapping(mapping);
                mappingsToAdd.Add(mappingToAdd);
            }
            face.Mappings = mappingsToAdd;
            return face;
        }

        private ICollection<Mapping> findAllMappings(Face face) {
            MappingFilter mappingFilter = new MappingFilter()
            {
                range = 0
            };
            IEnumerable<MappingDTO> mappingsDTOEnumerable = mappingService.find(face.id, mappingFilter);
            ICollection<MappingDTO> mappingsDTO = mappingsDTOEnumerable.ToList();
            return mapper.Map<ICollection<MappingDTO>, ICollection<Mapping>>(mappingsDTO);
        }
    }
}
