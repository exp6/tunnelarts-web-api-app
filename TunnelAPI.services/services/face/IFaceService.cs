﻿using System;
using System.Collections.Generic;
using TunelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.face
{
    public interface IFaceService
    {
        ICollection<FaceDTO> find(FaceFilter filter);
        int findAmount(FaceFilter filter);
        FaceDTO findOne(Guid faceId);
        void remove(Guid faceId);
        void update(FaceDTO face);
        void create(FaceDTO face);
    }
}
