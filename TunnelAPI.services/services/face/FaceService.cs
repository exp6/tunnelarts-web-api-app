﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.face;
using TunelAPI.repository.model;
using TunnelAPI.services.model;

namespace TunnelAPI.services.services.face
{
    public class FaceService : IFaceService
    {
        private readonly IFaceRepository faceRepository;
        private readonly IMapper mapper;

        public FaceService(IMapper mapper, IFaceRepository faceRepository)
        {
            this.mapper = mapper;
            this.faceRepository = faceRepository;           
        }

        public void create(FaceDTO face)
        {
            var faceEntity = mapper.Map<FaceDTO, Face>(face);
            faceEntity.Tunnel = null;
            faceEntity.IdTunnel = face.Tunnel.id;
            faceEntity.createdAt = DateTime.Now;
            faceEntity.updatedAt = DateTime.Now;
            faceEntity.deleted = false;

            faceRepository.create(faceEntity);
            faceRepository.saveChanges();
        }        

        public ICollection<FaceDTO> find(FaceFilter filter)
        {            
            var faces = faceRepository.find(filter);
            return mapper.Map<ICollection<Face>, ICollection<FaceDTO>>(faces);
        }        

        public int findAmount(FaceFilter filter)
        {
            return faceRepository.findAmount(filter);            
        }

        public FaceDTO findOne(Guid faceId)
        {
            var face = faceRepository.findOne(faceId);
            return mapper.Map<Face, FaceDTO>(face);
        }

        public void remove(Guid faceId)
        {
            faceRepository.remove(faceId);
            faceRepository.saveChanges();
        }

        public void update(FaceDTO face)
        {
            var faceEntity = faceRepository.findOne(face.id);
            faceEntity.updatedAt = DateTime.Now;
            faceEntity.Name = face.Name;
            faceEntity.Inclination = face.Inclination;
            faceEntity.RefChainage = face.RefChainage;
            faceEntity.Orientation = face.Orientation;
            faceEntity.Direction = face.Direction;

            faceRepository.saveChanges();
        }
    }
}
