﻿using AutoMapper;
using System;
using TunelAPI.repository;
using TunnelAPI.services.model;

namespace TunnelAPI.services.mapper
{
    public class AutoMapperConfiguration : Profile
    {
        protected override void Configure()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();
            CreateMap<Tunnel, TunnelDTO>().ReverseMap();
            CreateMap<Face, FaceDTO>().ReverseMap();
            CreateMap<Esr, EsrDTO>().ReverseMap();
            CreateMap<Mapping, MappingDTO>()
                .ForMember(dst => dst.MappingTime, opt => opt.MapFrom(src => src.MappingTime.Value))
                .ReverseMap()
                .ForMember(dst => dst.MappingTime, opt => opt.MapFrom(src => src.MappingTime.UtcDateTime));
            CreateMap<RockMass, RockMassDTO>().ReverseMap();
            CreateMap<Lithology, LithologyDTO>().ReverseMap();
            CreateMap<Discontinuity, DiscontinuityDTO>().ReverseMap();
            CreateMap<RockMassHazard, RockMassHazardDTO>().ReverseMap();
            CreateMap<SpecialFeature, SpecialFeatureDTO>().ReverseMap();
            CreateMap<AdditionalInformation, AdditionalInformationDTO>().ReverseMap();
            CreateMap<FailureZone, FailureZoneDTO>().ReverseMap();
            CreateMap<Particularity, ParticularityDTO>().ReverseMap();
            CreateMap<AdditionalDescription, AdditionalDescriptionDTO>().ReverseMap();
            CreateMap<QValueInputSelection, QValueInputSelectionDTO>().ReverseMap();
            CreateMap<QValueDiscontinuity, QValueDiscontinuityDTO>().ReverseMap();
            CreateMap<QValue, QValueDTO>().ReverseMap();            
            CreateMap<Rmr, RmrDTO>().ReverseMap();
            CreateMap<RmrInputSelection, RmrInputSelectionDTO>().ReverseMap();
            CreateMap<Picture, PictureDTO>().ReverseMap();
            CreateMap<Gsi, GsiDTO>().ReverseMap();
            CreateMap<ExpandedTunnel, ExpandedTunnelDTO>().ReverseMap();
            CreateMap<QValueInput, QValueInputDTO>().ReverseMap();
            CreateMap<QValueInputGroup, QValueInputGroupDTO>().ReverseMap();
            CreateMap<QValueInputGroupType, QValueInputGroupTypeDTO>().ReverseMap();
            CreateMap<RmrInput, RmrInputDTO>().ReverseMap();
            CreateMap<RmrInputGroup, RmrInputGroupDTO>().ReverseMap();
            CreateMap<RmrInputGroupType, RmrInputGroupTypeDTO>().ReverseMap();
            CreateMap<Role, RoleDTO>().ReverseMap();
            CreateMap<UserProject, UserProjectDTO>().ReverseMap();
            CreateMap<ProspectionHoleGroup, ProspectionHoleGroupDTO>().ReverseMap();
            CreateMap<ProspectionHoleGroupCalculation, ProspectionHoleGroupCalculationDTO>().ReverseMap();
            CreateMap<ExternalPicture, ExternalPictureDTO>().ReverseMap();
            CreateMap<TunnelProfile, TunnelProfileDTO>().ReverseMap();
            CreateMap<RockQuality, RockQualityDTO>().ReverseMap();
            CreateMap<FormationUnit, FormationUnitDTO>().ReverseMap();
            CreateMap<ClientSubscription, ClientSubscriptionDTO>().ReverseMap();
            CreateMap<Subscription, SubscriptionDTO>().ReverseMap();
            CreateMap<Client, ClientDTO>().ReverseMap();
            CreateMap<GsiValue, GsiValueDTO>().ReverseMap();
            CreateMap<RecommendationRockbolt, RecommendationRockboltDTO>().ReverseMap();
            CreateMap<RecommendationShotcrete, RecommendationShotcreteDTO>().ReverseMap();
            CreateMap<OfferRockType, OfferRockTypeDTO>().ReverseMap();
            CreateMap<MappingInput, MappingInputDTO>().ReverseMap();
            CreateMap<ProjectMappingInput, ProjectMappingInputDTO>().ReverseMap();
            CreateMap<StereonetPicture, StereonetPictureDTO>().ReverseMap();
            CreateMap<User, UserDTO>()
                .ForMember(dst => dst.Username, opt => opt.MapFrom(src => src.UserName));
            CreateMap<UserDTO, User>()
                .ForMember(dst => dst.UserName, opt => opt.MapFrom(src => src.Username));
        }
    }    
}
