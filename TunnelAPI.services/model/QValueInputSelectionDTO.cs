﻿using System;

namespace TunnelAPI.services.model
{
    public class QValueInputSelectionDTO
    {
        public Guid id { get; set; }        
        public Nullable<Guid> Ja { get; set; }
        public Nullable<Guid> Jn { get; set; }
        public Nullable<Guid> Jr { get; set; }
        public Nullable<Guid> Jw { get; set; }
        public Nullable<Guid> Srf { get; set; }
        public Nullable<Guid> JnType { get; set; }        
    }
}
