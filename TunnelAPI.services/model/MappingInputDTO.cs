﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class MappingInputDTO
    {
        public Guid id { get; set; }
        public Guid? Parent { get; set; }
        public string Name { get; set; }
        public string NameEs { get; set; }
        public int? Code { get; set; }
        public string Label { get; set; }
        public bool enabled { get; set; }
    }
}
