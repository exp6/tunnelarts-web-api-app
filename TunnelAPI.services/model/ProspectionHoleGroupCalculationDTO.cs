﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ProspectionHoleGroupCalculationDTO
    {
        public Guid id { get; set; }
        public ProspectionHoleGroupDTO ProspectionHoleGroup { get; set; }
        public int RodPosition { get; set; }
        public float ProjectedDistance { get; set; }
        public String EstimatedRock { get; set; }
        public double QMax { get; set; }
        public double QMin { get; set; }
        public double QAvg { get; set; }
        public Nullable<bool> deleted { get; set; }
        public DateTimeOffset createdAt { get; set; }
        public DateTimeOffset updatedAt { get; set; }
        public ICollection<ProspectionHoleGroupDTO> ProspectionHoleGroupsRelated { get; set; }
    }
}
