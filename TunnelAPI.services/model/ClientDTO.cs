﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ClientDTO
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
    }
}
