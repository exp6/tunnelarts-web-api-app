﻿using System;

namespace TunnelAPI.services.model
{
    public class RoleDTO
    {
        public Guid IdRole { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
