﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class QValueInputGroupTypeDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Code { get; set; }
        public string Description { get; set; }        
        public ICollection<QValueInputGroupDTO> QValueInputGroups { get; set; }
    }
}
