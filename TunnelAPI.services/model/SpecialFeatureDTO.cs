﻿using System;

namespace TunnelAPI.services.model
{
    public class SpecialFeatureDTO
    {
        public Guid id { get; set; }        
        public Nullable<bool> Zeolites { get; set; }
        public Nullable<bool> Clay { get; set; }
        public Nullable<bool> Chlorite { get; set; }
        public Nullable<bool> RedTuff { get; set; }
        public Nullable<bool> Sulfides { get; set; }
        public Nullable<bool> Sulfates { get; set; }        
        public Nullable<bool> None { get; set; }
    }
}
