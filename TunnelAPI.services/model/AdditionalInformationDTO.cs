﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class AdditionalInformationDTO
    {
        public Guid id { get; set; }

        public ICollection<RockMassHazardDTO> RockMassHazards { get; set; }
        public ICollection<SpecialFeatureDTO> SpecialFeatures { get; set; }
    }
}
