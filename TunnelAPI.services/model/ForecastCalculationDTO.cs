﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ForecastCalculationDTO
    {
        public MappingDTO mapping { get; set; }
        public ProspectionHoleGroupCalculationDTO predicted { get; set; }
        public ProspectionHoleGroupCalculationDTO observed { get; set; }
    }
}
