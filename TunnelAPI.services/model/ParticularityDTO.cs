﻿using System;

namespace TunnelAPI.services.model
{
    public class ParticularityDTO
    {
        public Guid id { get; set; }        
        public string InstabilityCausedBy { get; set; }
        public string InstabilityLocation { get; set; }
        public bool InstabilityNa { get; set; }
        public string OverbreakCausedBy { get; set; }
        public int OverbreakCausedByValue { get; set; }
        public string OverbreakLocation { get; set; }
        public string OverbreakVolume { get; set; }
        public string OverbreakLocationStart { get; set; }
        public string OverbreakLocationEnd { get; set; }
        public bool OverbreakNa { get; set; }
    }
}
