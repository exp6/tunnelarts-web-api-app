﻿using System;

namespace TunnelAPI.services.model
{
    public class DiscontinuityDTO
    {
        public Guid id { get; set; }        
        public Nullable<int> Type { get; set; }
        public Nullable<int> Relevance { get; set; }
        public Nullable<double> OrientationDd { get; set; }
        public Nullable<double> OrientationD { get; set; }
        public Nullable<int> Spacing { get; set; }
        public Nullable<int> Persistence { get; set; }
        public Nullable<int> Opening { get; set; }
        public Nullable<int> Roughness { get; set; }
        public Nullable<int> Infilling { get; set; }
        public Nullable<int> InfillingType { get; set; }
        public Nullable<int> Weathering { get; set; }                
        public Nullable<int> Position { get; set; }        
        public Nullable<int> Slickensided { get; set; }
    }
}
