﻿using System;

namespace TunnelAPI.services.model
{
    public class RmrInputSelectionDTO
    {
        public Guid id { get; set; }        
        public Nullable<Guid> Rqd { get; set; }
        public Nullable<Guid> Spacing { get; set; }
        public Nullable<Guid> Persistence { get; set; }
        public Nullable<Guid> Opening { get; set; }
        public Nullable<Guid> Roughness { get; set; }
        public Nullable<Guid> Infilling { get; set; }
        public Nullable<Guid> Weathering { get; set; }
        public Nullable<Guid> Groundwater { get; set; }
        public Nullable<Guid> OrientationDD { get; set; }
        public Nullable<Guid> Strength { get; set; }
        public Nullable<Guid> ConditionDD { get; set; }
    }
}
