﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class OfferRockTypeDTO
    {
        public Guid id { get; set; }
        public double? InitialProgress { get; set; }
        public double? FinalProgress { get; set; }
        public int? RockType { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdateAt { get; set; }
    }
}
