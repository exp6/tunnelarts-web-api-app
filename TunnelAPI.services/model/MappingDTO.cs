﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class MappingDTO
    {
        public Guid id { get; set; }
        public Guid IdFace { get; set; }
        public Guid IdUser { get; set; }
        public Nullable<decimal> ChainageStart { get; set; }
        public Nullable<decimal> ChainageEnd { get; set; }
        public bool Completed { get; set; }
        public Nullable<bool> Finished { get; set; }
        public Nullable<bool> deleted { get; set; }        
        public DateTimeOffset createdAt { get; set; }
        public DateTimeOffset updatedAt { get; set; }
        public UserDTO User { get; set; }
        public ICollection<LithologyDTO> Lithologies { get; set; }
        public ICollection<RockMassDTO> RockMasses { get; set; }
        public ICollection<DiscontinuityDTO> Discontinuities { get; set; }
        public ICollection<AdditionalInformationDTO> AdditionalInformations { get; set; }
        public ICollection<FailureZoneDTO> FailureZones { get; set; }
        public ICollection<ParticularityDTO> Particularities { get; set; }
        public ICollection<AdditionalDescriptionDTO> AdditionalDescriptions { get; set; }
        public ICollection<QValueInputSelectionDTO> QValueInputSelections { get; set; }
        public ICollection<FormationUnitDTO> FormationUnits { get; set; }
        public ICollection<QValueDTO> QValues { get; set; }        
        public ICollection<RmrDTO> Rmrs { get; set; }
        public ICollection<RmrInputSelectionDTO> RmrInputSelections { get; set; }
        public ICollection<ExpandedTunnelDTO> ExpandedTunnels { get; set; }
        public ICollection<PictureDTO> Pictures { get; set; }
        public ICollection<GsiDTO> Gsis { get; set; }
        public ICollection<ExternalPictureDTO> ExternalPictures { get; set; }
        public DateTimeOffset MappingTime { get; set; }
        public ICollection<GsiValueDTO> GsiValues { get; set; }
        public ICollection<RecommendationRockboltDTO> RecommendationRockbolts { get; set; }
        public ICollection<RecommendationShotcreteDTO> RecommendationShotcretes { get; set; }
        public ICollection<StereonetPictureDTO> StereonetPictures { get; set; }
    }
}
