﻿using System;

namespace TunnelAPI.services.model
{
    public class QValueInputDTO
    {
        public Guid id { get; set; }
        public string CodeIndex { get; set; }
        public string Description { get; set; }
        public Nullable<double> StartValue { get; set; }
        public Nullable<double> EndValue { get; set; }        
    }
}
