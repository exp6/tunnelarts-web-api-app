﻿using System;

namespace TunnelAPI.services.model
{
    public class AdditionalDescriptionDTO
    {
        public Guid id { get; set; }        
        public Nullable<int> DistanceToFace { get; set; }
        public Nullable<int> ResponsibleForRestrictedArea { get; set; }
        public Nullable<int> ReasonForRestrictedArea { get; set; }
        public Nullable<int> LightQuality { get; set; }
        public Nullable<int> AirQuality { get; set; }
        public string AvailableTime { get; set; }
        public Nullable<int> NewDamageBehind { get; set; }
        public string Description { get; set; }        
    }
}
