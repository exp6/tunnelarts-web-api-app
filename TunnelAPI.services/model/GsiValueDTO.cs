﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class GsiValueDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Structure { get; set; }
        public Nullable<int> Surface { get; set; }
        public Nullable<float> Max { get; set; }
        public Nullable<float> Min { get; set; }
        public Nullable<float> Avg { get; set; }
    }
}
