﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class TunnelDTO
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public Guid IdProject { get; set; }
        public Guid IdEsr { get; set; }
        public float Span { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> deleted { get; set; }
        public byte[] version { get; set; }
        public DateTimeOffset updatedAt { get; set; }
        public Nullable<DateTimeOffset> createdAt { get; set; }
        public ProjectDTO Project { get; set; }
        public EsrDTO Esr { get; set; }
        public float Perimeter { get; set; }
        public Guid IdTunnelProfile { get; set; }
        public TunnelProfileDTO TunnelProfile { get; set; }
        public float Balance { get; set; }
        public float Progress { get; set; }
        public ICollection<OfferRockTypeDTO> OfferRockTypes { get; set; }
    }
}
