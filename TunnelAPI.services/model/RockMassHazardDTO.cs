﻿using System;

namespace TunnelAPI.services.model
{
    public class RockMassHazardDTO
    {
        public Guid id { get; set; }        
        public Nullable<bool> RockBurst { get; set; }
        public Nullable<bool> Swelling { get; set; }
        public Nullable<bool> Squeezing { get; set; }
        public Nullable<bool> Slaking { get; set; }  
        public Nullable<bool> None { get; set; }      
    }
}
