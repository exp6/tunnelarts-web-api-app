﻿using System;

namespace TunnelAPI.services.model
{
    public class RmrDTO
    {
        public Guid id { get; set; }        
        public Nullable<double> Strength { get; set; }
        public Nullable<double> Rqd { get; set; }
        public Nullable<double> Spacing { get; set; }
        public Nullable<double> Persistence { get; set; }
        public Nullable<double> Opening { get; set; }
        public Nullable<double> Roughness { get; set; }
        public Nullable<double> Infilling { get; set; }
        public Nullable<double> Weathering { get; set; }
        public Nullable<double> Groundwater { get; set; }
        public Nullable<double> Orientation { get; set; }
        public Nullable<int> RockQualityCode { get; set; }        
        public Nullable<double> condition { get; set; }
        public Nullable<bool> conditionEnabled { get; set; }
    }
}
