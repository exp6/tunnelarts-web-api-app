﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class StereonetPictureDTO
    {
        public Guid id { get; set; }
        public string SideName { get; set; }
        public string RemoteUri { get; set; }
        public string LocalUri { get; set; }
    }
}
