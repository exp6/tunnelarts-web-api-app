﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class ProspectionHoleGroupDTO
    {
        public Guid id { get; set; }
        public String Name { get; set; }
        public Guid IdFace { get; set; }
        public Guid IdUser { get; set; }
        public double QValueSelected { get; set; }
        public Guid IdMapping { get; set; }
        public MappingDTO Mapping { get; set; }
        public UserDTO User { get; set; }
        public Nullable<bool> deleted { get; set; }
        public DateTimeOffset createdAt { get; set; }
        public DateTimeOffset updatedAt { get; set; }
        public int probeHolesCount { get; set; }
        public int rodsCount { get; set; }
    }
}
