﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class SubscriptionDTO
    {
        public Guid IdSubscription { get; set; }
        public string TierName { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
