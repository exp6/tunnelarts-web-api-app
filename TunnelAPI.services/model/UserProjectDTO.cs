﻿using System;

namespace TunnelAPI.services.model
{
    public class UserProjectDTO
    {
        public System.Guid IdUser { get; set; }
        public System.Guid IdProject { get; set; }
        public virtual ProjectDTO Project { get; set; }
    }
}
