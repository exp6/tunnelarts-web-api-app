﻿using System;

namespace TunnelAPI.services.model
{
    public class FaceDTO
    {
        public Guid id { get; set; }
        public string Name { get; set; }
        public Guid IdTunnel { get; set; }
        public string RefChainage { get; set; }
        public decimal Orientation { get; set; }
        public decimal Inclination { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<bool> deleted { get; set; }
        public Nullable<DateTimeOffset> createdAt { get; set; }
        public byte[] version { get; set; }
        public DateTimeOffset updatedAt { get; set; }     
        public TunnelDTO Tunnel { get; set; }
        public int Direction { get; set; }
    }
}
