﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class EsrDTO
    {
        public Guid IdEsr { get; set; }
        public String Description { get; set; }
    }
}
