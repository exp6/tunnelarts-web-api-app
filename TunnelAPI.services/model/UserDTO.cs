﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class UserDTO
    {
        public Guid IdUser { get; set; }
        public Guid IdClient { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string CreatedBy { get; set; }
        public bool Enabled { get; set; }
        public Nullable<DateTime> CreatedAt { get; set; }
        public ICollection<RoleDTO> Roles { get; set; }
        public ICollection<UserProjectDTO> UserProjects { get; set; }
    }
}
