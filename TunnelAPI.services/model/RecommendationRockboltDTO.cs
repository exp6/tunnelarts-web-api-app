﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class RecommendationRockboltDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Position { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<float> Pk { get; set; }
        public Nullable<float> Length { get; set; }
        public Nullable<int> Diameter { get; set; }
        public Nullable<float> Spacing { get; set; }
        public Nullable<float> Angle { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public Nullable<float> Horizontal { get; set; }
        public Nullable<float> Vertical { get; set; }
    }
}
