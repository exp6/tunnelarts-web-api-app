﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class FormationUnitDTO
    {
        public Guid id { get; set; }
        public Guid IdProject { get; set; }
        public int? Code { get; set; }
        public string Label { get; set; }
    }
}
