﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ExternalPictureDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Code { get; set; }
        public string Name { get; set; }
        public string RemoteUri { get; set; }
        public DateTimeOffset createdAt { get; set; }
        public DateTimeOffset updatedAt { get; set; }
    }
}
