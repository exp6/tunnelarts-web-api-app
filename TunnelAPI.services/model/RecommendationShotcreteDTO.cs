﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class RecommendationShotcreteDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Position { get; set; }
        public Nullable<float> Thickness { get; set; }
        public Nullable<int> FiberReinforced { get; set; }
        public Nullable<float> Pk { get; set; }
        public Nullable<int> Location { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
    }
}
