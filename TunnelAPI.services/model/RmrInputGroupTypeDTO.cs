﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class RmrInputGroupTypeDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Code { get; set; }
        public string Description { get; set; }        
        public ICollection<RmrInputGroupDTO> RmrInputGroups { get; set; }
    }
}
