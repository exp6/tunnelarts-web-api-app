﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class QValueDiscontinuityDTO
    {
        public Guid id { get; set; }
        public Nullable<int> Discontinuity { get; set; }
        public Nullable<double> Jr { get; set; }
        public Nullable<double> Ja { get; set; }
        public Nullable<bool> Completed { get; set; }
        public Nullable<Guid> JrSelection { get; set; }
        public Nullable<Guid> JaSelection { get; set; }
    }
}
