﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class RockQualityDTO
    {
        public Guid id { get; set; }
        public Guid IdProject { get; set; }
        public int? Code { get; set; }
        public string Name { get; set; }
        public float UpperBound { get; set; }
        public float LowerBound { get; set; }
        public float OfferedPerformance { get; set; }
    }
}
