﻿using System;

namespace TunnelAPI.services.model
{
    public class LithologyDTO
    {
        public Guid id { get; set; }
        public int Position { get; set; }
        public double Presence { get; set; }
        public int Strength { get; set; }
        public int ColourValue { get; set; }
        public int ColourChroma { get; set; }
        public int ColorHue { get; set; }
        public int Texture { get; set; }
        public int AlterationWeathering { get; set; }
        public int GrainSize { get; set; }
        public int GeneticGroup { get; set; }
        public int SubGroup { get; set; }
        public int JvMin { get; set; }
        public int JvMax { get; set; }
        public int BlockShape { get; set; }
        public double BlockSizeOne { get; set; }
        public double BlockSizeTwo { get; set; }
        public double BlockSizeThree { get; set; }
        public string briefDesc { get; set; }
        public int FormationUnit { get; set; }
    }
}
