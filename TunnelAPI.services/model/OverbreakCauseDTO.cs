﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class OverbreakCauseDTO
    {
        public Guid id { get; set; }
        public int code { get; set; }
        public string description { get; set; }
    }
}
