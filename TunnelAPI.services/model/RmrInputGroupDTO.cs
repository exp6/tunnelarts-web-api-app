﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class RmrInputGroupDTO
    {
        public Guid id { get; set; }        
        public string CodeIndex { get; set; }
        public string Description { get; set; }        
        public ICollection<RmrInputDTO> RmrInputs { get; set; }        
    }
}
