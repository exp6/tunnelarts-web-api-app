﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class QValueInputGroupDTO
    {
        public Guid id { get; set; }        
        public string CodeIndex { get; set; }
        public string Description { get; set; }        
        public ICollection<QValueInputDTO> QValueInputs { get; set; }        
    }
}
