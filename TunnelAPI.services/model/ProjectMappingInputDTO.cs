﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ProjectMappingInputDTO
    {
        public Guid id { get; set; }
        public Guid IdProject { get; set; }
        public Guid IdMappingInput { get; set; }
    }
}
