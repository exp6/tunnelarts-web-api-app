﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class QValueDTO
    {        
        public Guid id { get; set; }
        public Nullable<double> Rqd { get; set; }
        public Nullable<double> Jn { get; set; }
        public Nullable<double> Jr { get; set; }
        public Nullable<double> Ja { get; set; }
        public Nullable<double> Jw { get; set; }
        public Nullable<double> Srf { get; set; }
        public Nullable<int> RockQuality { get; set; }        
        public Nullable<double> rockQualityCode { get; set; }     
        public Nullable<double> QMax { get; set; }
        public Nullable<double> QMin { get; set; }
        public Nullable<double> QAvg { get; set; }
        public Nullable<int> Discontinuities { get; set; }
        public Nullable<int> JnType { get; set; }
        public ICollection<QValueDiscontinuityDTO> QValueDiscontinuities { get; set; }

    }
}
