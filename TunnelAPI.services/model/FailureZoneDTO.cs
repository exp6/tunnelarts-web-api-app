﻿using System;

namespace TunnelAPI.services.model
{
    public class FailureZoneDTO
    {
        public Guid id { get; set; }        
        public Nullable<double> OrientationDd { get; set; }
        public Nullable<double> OrientationD { get; set; }
        public Nullable<double> Thickness { get; set; }
        public Nullable<double> MatrixBlock { get; set; }
        public Nullable<int> MatrixColourValue { get; set; }
        public Nullable<int> MatrixColourChroma { get; set; }
        public Nullable<int> MatrixColourHue { get; set; }
        public Nullable<int> MatrixGainSize { get; set; }
        public Nullable<int> BlockSize { get; set; }
        public Nullable<int> BlockShape { get; set; }
        public Nullable<int> BlockGeneticGroup { get; set; }
        public Nullable<int> BlockSubGroup { get; set; }        
        public Nullable<double> RakeOfStriae { get; set; }
        public Nullable<int> SenseOfMovement { get; set; }
        public Nullable<bool> NoneRake { get; set; }
    }
}
