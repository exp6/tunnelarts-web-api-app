﻿using System;
using System.Collections.Generic;

namespace TunnelAPI.services.model
{
    public class ProjectDTO
    {
        public Guid id { get; set; }
        public Nullable<Guid> IdClient { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string CreatedBy { get; set; }                
        public DateTimeOffset updatedAt { get; set; }
        public Nullable<int> Progress { get; set; }
        public Nullable<DateTimeOffset> createdAt { get; set; }
        public ICollection<RockQualityDTO> RockQualities { get; set; }
        public ICollection<FormationUnitDTO> FormationUnits { get; set; }
        public float Balance { get; set; }
        public ICollection<ProjectMappingInputDTO> ProjectMappingInputs { get; set; }
        public ICollection<MappingInputDTO> MappingInputs { get; set; }
    }
}
