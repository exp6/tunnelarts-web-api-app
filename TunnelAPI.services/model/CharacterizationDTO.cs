﻿using System.Collections.Generic;
using TunelAPI.repository;

namespace TunnelAPI.services.model
{
    public class CharacterizationDTO
    {
        public IEnumerable<Air> Airs { get; set; }
        public IEnumerable<AlterationWeathering> AlterationsWeathering { get; set; }
        public IEnumerable<BlockShape> BlockShapes { get; set; }
        public IEnumerable<BlockShapeType> BlockShapeTypes { get; set; }
        public IEnumerable<BlockSize> BlockSizes { get; set; }
        public IEnumerable<ColourType> ColourTypes { get; set; }
        public IEnumerable<ColourTypeValue> ColourTypeValues { get; set; }
        public IEnumerable<Damage> Damages { get; set; }
        public IEnumerable<DiscontinuityType> DiscontinuityTypes { get; set; }
        public IEnumerable<FaceType> FaceTypes { get; set; }
        public IEnumerable<FaultType> FaultTypes { get; set; }
        public IEnumerable<GeneticGroup> GeneticGroups { get; set; }
        public IEnumerable<GrainSize> GrainSizes { get; set; }
        public IEnumerable<Infilling> Infillings { get; set; }
        public IEnumerable<InfillingType> InfillingTypes { get; set; }
        public IEnumerable<JointingType> Jointings { get; set; }
        public IEnumerable<Light> Lights { get; set; }
        public IEnumerable<Opening> Openings { get; set; }
        public IEnumerable<OverbreakCaus> OverbreakCauses { get; set; }
        public IEnumerable<Persistence> Persistences { get; set; }
        public IEnumerable<QRockQuality> QRockQualities { get; set; }
        public IEnumerable<Reason> Reasons { get; set; }
        public IEnumerable<Relevance> Relevances { get; set; }
        public IEnumerable<Responsible> Responsibles { get; set; }
        public IEnumerable<RmrRockQuality> RmrRockQualities { get; set; }
        public IEnumerable<Roughness> Roughnesses { get; set; }
        public IEnumerable<SenseMovement> SenseOfMovement { get; set; }
        public IEnumerable<Slickensided> Slickensideds { get; set; }
        public IEnumerable<Spacing> Spacings { get; set; }
        public IEnumerable<Strength> Strengths { get; set; }
        public IEnumerable<StructureType> Structures { get; set; }
        public IEnumerable<SubGroup> SubGroups { get; set; }
        public IEnumerable<Texture> Textures { get; set; }
        public IEnumerable<Water> Waters { get; set; }
        public IEnumerable<WaterQuality> WaterQualities { get; set; }
        public IEnumerable<Weathering> Weatherings { get; set; }        
        public IEnumerable<QValueInputGroupTypeDTO> QValueInputGroupTypes { get; set; }
        public IEnumerable<QJnType> QJnTypes { get; set; }
        public IEnumerable<RmrInputGroupTypeDTO> RmrInputGroupTypes { get; set; }



       
    }
}
