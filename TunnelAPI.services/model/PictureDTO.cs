﻿using System;

namespace TunnelAPI.services.model
{
    public class PictureDTO
    {
        public Guid id { get; set; }        
        public Nullable<int> Code { get; set; }
        public string SideName { get; set; }
        public string RemoteUri { get; set; }
        public string RemoteUriOriginal { get; set; }        
    }
}
