﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class TunnelProfileDTO
    {
        public Guid id { get; set; }
        public int? Code { get; set; }
        public string Description { get; set; }
        public string RemoteUri { get; set; }
    }
}
