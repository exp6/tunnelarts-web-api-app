﻿using System;

namespace TunnelAPI.services.model
{
    public class RockMassDTO
    {
        public Guid id { get; set; }        
        public Nullable<int> Structure { get; set; }
        public Nullable<int> Water { get; set; }
        public Nullable<int> WaterQuality { get; set; }
        public Nullable<int> GeneticGroup { get; set; }
        public Nullable<int> Jointing { get; set; }
        public Nullable<double> WaterT { get; set; }
        public Nullable<double> Inflow { get; set; }        
        public Nullable<int> RockName { get; set; }
        public Nullable<int> Weathering { get; set; }
        public Nullable<int> ColorValue { get; set; }
        public Nullable<int> ColorChroma { get; set; }
        public Nullable<int> ColorHue { get; set; }
        public Nullable<int> Strength { get; set; }
        public string briefDesc { get; set; }
    }
}
