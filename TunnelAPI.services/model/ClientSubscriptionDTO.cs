﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunnelAPI.services.model
{
    public class ClientSubscriptionDTO
    {
        public Guid IdClientSubscription { get; set; }
        public Guid IdClient { get; set; }
        public Guid IdSubscription { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public SubscriptionDTO subscription { get; set; }
        public ClientDTO client { get; set; }
        public int ReportType { get; set; }
    }
}
