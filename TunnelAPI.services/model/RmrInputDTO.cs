﻿using System;

namespace TunnelAPI.services.model
{
    public class RmrInputDTO
    {
        public Guid id { get; set; }        
        public string CodeIndex { get; set; }
        public string Description { get; set; }
        public Nullable<double> StartValue { get; set; }
        public Nullable<double> EndValue { get; set; }        
    }
}
