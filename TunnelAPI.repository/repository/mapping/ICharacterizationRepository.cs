﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;

namespace TunnelAPI.repository.mapping
{
    public interface ICharacterizationRepository
    {
        IEnumerable<Air> findAirs();
        IEnumerable<AlterationWeathering> findAlterationsWeathering();
        IEnumerable<BlockShape> findBlockShapes();
        IEnumerable<BlockShapeType> findBlockShapeTypes();
        IEnumerable<BlockSize> findBlockSizes();
        IEnumerable<ColourType> findColourTypes();
        IEnumerable<ColourTypeValue> findColourTypeValues();
        IEnumerable<Damage> findDamages();
        IEnumerable<DiscontinuityType> findDiscontinuityTypes();
        IEnumerable<FaceType> findFaceTypes();
        IEnumerable<FaultType> findFaultTypes();
        IEnumerable<GeneticGroup> findGeneticGroups();
        IEnumerable<GrainSize> findGrainSizes();
        IEnumerable<Infilling> findInfillings();
        IEnumerable<InfillingType> findInfillingTypes();
        IEnumerable<JointingType> findJointings();
        IEnumerable<Light> findLights();
        IEnumerable<Opening> findOpenings();
        IEnumerable<OverbreakCaus> findOverbreakCauses();
        IEnumerable<Persistence> findPersistences();
        IEnumerable<QRockQuality> findQRockQualities();
        IEnumerable<QJnType> findQJnTypes();
        IEnumerable<Reason> findReasons();
        IEnumerable<Relevance> findRelevances();
        IEnumerable<Responsible> findResponsibles();
        IEnumerable<RmrRockQuality> findRmrRockQualities();
        IEnumerable<Roughness> findRoughnesses();
        IEnumerable<SenseMovement> findSenseMovement();
        IEnumerable<Spacing> findSpacings();
        IEnumerable<Strength> findStrengths();
        IEnumerable<StructureType> findStructures();
        IEnumerable<SubGroup> findSubGroups();
        IEnumerable<Texture> findTextures();
        IEnumerable<Water> findWaters();
        IEnumerable<WaterQuality> findWaterQualities();
        IEnumerable<Weathering> findWeatherings();
        IEnumerable<QValueInput> findQValueInputs();
        IEnumerable<QValueInputGroup> findQValueInputGroups();
        IEnumerable<QValueInputGroupType> findQValueInputGroupTypes();
        IEnumerable<RmrInput> findRmrInputs();
        IEnumerable<RmrInputGroup> findRmrInputGroups();
        IEnumerable<RmrInputGroupType> findRmrInputGroupTypes();
        IEnumerable<Slickensided> findSlickensideds();
    }
}
