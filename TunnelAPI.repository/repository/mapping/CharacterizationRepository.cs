﻿
using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;

namespace TunnelAPI.repository.mapping
{
    public class CharacterizationRepository : ICharacterizationRepository, IDisposable
    {
        private TunnelArtsEntities context;
        private bool disposed = false;

        public CharacterizationRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public IEnumerable<Air> findAirs()
        {
            return context.Airs.OrderBy(e => e.code);
        }

        public IEnumerable<AlterationWeathering> findAlterationsWeathering()
        {
            return context.AlterationWeatherings.OrderBy(e => e.code);
        }

        public IEnumerable<BlockShape> findBlockShapes()
        {
            return context.BlockShapes.OrderBy(e => e.code);
        }

        public IEnumerable<BlockShapeType> findBlockShapeTypes()
        {
            return context.BlockShapeTypes.OrderBy(e => e.code);
        }

        public IEnumerable<BlockSize> findBlockSizes()
        {
            return context.BlockSizes.OrderBy(e => e.code);
        }

        public IEnumerable<ColourType> findColourTypes()
        {
            return context.ColourTypes.OrderBy(e => e.code);
        }

        public IEnumerable<ColourTypeValue> findColourTypeValues()
        {
            return context.ColourTypeValues.OrderBy(e => e.code);
        }

        public IEnumerable<Damage> findDamages()
        {
            return context.Damages.OrderBy(e => e.code);
        }

        public IEnumerable<DiscontinuityType> findDiscontinuityTypes()
        {
            return context.DiscontinuityTypes.OrderBy(dt => dt.code);
        }

        public IEnumerable<FaceType> findFaceTypes()
        {
            return context.FaceTypes.OrderBy(e => e.code);
        }

        public IEnumerable<FaultType> findFaultTypes()
        {
            return context.FaultTypes.OrderBy(e => e.code);
        }

        public IEnumerable<GeneticGroup> findGeneticGroups()
        {
            return context.GeneticGroups.OrderBy(gn => gn.code);
        }

        public IEnumerable<GrainSize> findGrainSizes()
        {
            return context.GrainSizes.OrderBy(e => e.code);
        }

        public IEnumerable<Infilling> findInfillings()
        {
            return context.Infillings.OrderBy(e => e.code);
        }

        public IEnumerable<InfillingType> findInfillingTypes()
        {
            return context.InfillingTypes.OrderBy(e => e.code);
        }

        public IEnumerable<JointingType> findJointings()
        {
            return context.JointingTypes.OrderBy(joi => joi.code);
        }

        public IEnumerable<Light> findLights()
        {
            return context.Lights.OrderBy(e => e.code);
        }

        public IEnumerable<Opening> findOpenings()
        {
            return context.Openings.OrderBy(e => e.code);
        }

        public IEnumerable<Persistence> findPersistences()
        {
            return context.Persistences.OrderBy(e => e.code);
        }

        public IEnumerable<QRockQuality> findQRockQualities()
        {
            return context.QRockQualities.OrderBy(e => e.Code);
        }

        public IEnumerable<Reason> findReasons()
        {
            return context.Reasons.OrderBy(e => e.code);
        }

        public IEnumerable<Relevance> findRelevances()
        {
            return context.Relevances.OrderBy(rl => rl.code);
        }

        public IEnumerable<Responsible> findResponsibles()
        {
            return context.Responsibles.OrderBy(e => e.code);
        }

        public IEnumerable<RmrRockQuality> findRmrRockQualities()
        {
            return context.RmrRockQualities.OrderBy(e => e.Code);
        }

        public IEnumerable<Roughness> findRoughnesses()
        {
            return context.Roughnesses.OrderBy(e => e.code);
        }

        public IEnumerable<SenseMovement> findSenseMovement()
        {
            return context.SenseMovements.OrderBy(e => e.code);
        }

        public IEnumerable<Spacing> findSpacings()
        {
            return context.Spacings.OrderBy(e => e.code);
        }

        public IEnumerable<Strength> findStrengths()
        {
            return context.Strengths.OrderBy(e => e.code);
        }

        public IEnumerable<StructureType> findStructures()
        {            
            return context.StructureTypes.OrderBy(str => str.code);
        }

        public IEnumerable<SubGroup> findSubGroups()
        {
            return context.SubGroups.OrderBy(e => e.code);
        }

        public IEnumerable<Texture> findTextures()
        {
            return context.Textures.OrderBy(e => e.code);
        }

        public IEnumerable<WaterQuality> findWaterQualities()
        {
            return context.WaterQualities.OrderBy(wt => wt.code);
        }

        public IEnumerable<Slickensided> findSlickensideds()
        {
            return context.Slickensideds.OrderBy(wt => wt.code);
        }

        public IEnumerable<Water> findWaters()
        {
            return context.Waters.OrderBy(wt => wt.code);
        }

        public IEnumerable<Weathering> findWeatherings()
        {
            return context.Weatherings.OrderBy(e => e.code);
        }        

        public IEnumerable<QValueInput> findQValueInputs()
        {
            return context.QValueInputs.OrderBy(e => e.CodeIndex);
        }

        public IEnumerable<QJnType> findQJnTypes()
        {
            return context.QJnTypes.OrderBy(e => e.code);
        }

        public IEnumerable<QValueInputGroup> findQValueInputGroups()
        {
            return context.QValueInputGroups.OrderBy(e => e.CodeIndex);
        }

        public IEnumerable<QValueInputGroupType> findQValueInputGroupTypes()
        {
            return context.QValueInputGroupTypes.OrderBy(e => e.Code).ToList();
        }

        public IEnumerable<RmrInput> findRmrInputs()
        {
            return context.RmrInputs.OrderBy(e => e.CodeIndex);
        }

        public IEnumerable<RmrInputGroup> findRmrInputGroups()
        {
            return context.RmrInputGroups.OrderBy(e => e.CodeIndex);
        }

        public IEnumerable<RmrInputGroupType> findRmrInputGroupTypes()
        {
            return context.RmrInputGroupTypes.OrderBy(e => e.Code).ToList();
        }

        public IEnumerable<OverbreakCaus> findOverbreakCauses()
        {
            return context.OverbreakCauses.OrderBy(e => e.code).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }
    }
}
