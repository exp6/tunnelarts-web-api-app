﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using TunnelAPI.repository.model;
using TunelAPI.repository;

namespace TunnelAPI.repository.mapping
{
    public class MappingRepository : IMappingRepository, IDisposable
    {
        private TunnelArtsEntities context;
        private bool disposed = false;

        public MappingRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public Mapping findOne(Guid mappingId)
        {
            var mapping = context.Mappings.FirstOrDefault(mp => mp.id == mappingId);
            mapping.Lithologies = findLithologies(mappingId);
            mapping.RockMasses = findRockMasses(mappingId);
            mapping.Discontinuities = findDiscontinuity(mappingId);
            mapping.AdditionalInformations = findAdditionalInformation(mappingId);
            mapping.FailureZones = findFailureZone(mappingId);
            mapping.Particularities = findParticularities(mappingId);
            mapping.AdditionalDescriptions = findAdditionalDescription(mappingId);
            mapping.QValueInputSelections = findQValueInputSelection(mappingId);
            mapping.RmrInputSelections = findRMRInputSelection(mappingId);
            mapping.QValues = findQValue(mappingId);
            mapping.GsiValues = findGsiValues(mappingId);
            mapping.RecommendationRockbolts = findRecommendationRockbolts(mappingId);
            mapping.RecommendationShotcretes = findRecommendationShotcretes(mappingId);

            foreach (var q in mapping.QValues)
            {
                q.QValueDiscontinuities = q.QValueDiscontinuities.Where(x => !x.deleted.Value).ToList();
            }
            mapping.Rmrs = findRMR(mappingId);
            return mapping;
        }

        private ICollection<GsiValue> findGsiValues(Guid mappingId)
        {
            return context.GsiValues
                .Where(ltg => ltg.IdMapping == mappingId)
                .Where(ltg => ltg.deleted == false)
                .ToList();
        }

        private ICollection<RecommendationRockbolt> findRecommendationRockbolts(Guid mappingId)
        {
            return context.RecommendationRockbolts
                .Where(ltg => ltg.IdMapping == mappingId)
                .Where(ltg => ltg.deleted == false)
                .ToList();
        }

        private ICollection<RecommendationShotcrete> findRecommendationShotcretes(Guid mappingId)
        {
            return context.RecommendationShotcretes
                .Where(ltg => ltg.IdMapping == mappingId)
                .Where(ltg => ltg.deleted == false)
                .ToList();
        }

        private ICollection<Lithology> findLithologies(Guid mappingId)
        {
            return context.Lithologies
                .Where(ltg => ltg.IdMapping == mappingId)
                .Where(ltg => ltg.deleted == false)
                .ToList();
        }

        private ICollection<RockMass> findRockMasses(Guid mappingId)
        {
            return context.RockMasses
                .Where(rm => rm.IdMapping == mappingId)
                .Where(rm => rm.deleted == false)
                .ToList();
        }

        private ICollection<Discontinuity> findDiscontinuity(Guid mappingId)
        {
            return context.Discontinuities
                .Where(dis => dis.IdMapping == mappingId)
                .Where(dis => dis.deleted == false)
                .ToList();
        }

        private ICollection<AdditionalInformation> findAdditionalInformation(Guid mappingId)
        {
            return context.AdditionalInformations
                .Where(addi => addi.IdMapping == mappingId)
                .Where(addi => addi.deleted == false)
                .ToList();
        }

        private ICollection<FailureZone> findFailureZone(Guid mappingId)
        {
            return context.FailureZones
                .Where(fz => fz.IdMapping == mappingId)
                .Where(fz => fz.deleted == false)
                .ToList();
        }

        private ICollection<Particularity> findParticularities(Guid mappingId)
        {
            return context.Particularities
                .Where(prt => prt.IdMapping == mappingId)
                .Where(prt => prt.deleted == false)
                .ToList();
        }

        private ICollection<AdditionalDescription> findAdditionalDescription(Guid mappingId)
        {
            return context.AdditionalDescriptions
                .Where(ad => ad.IdMapping == mappingId)
                .Where(ad => ad.deleted == false)
                .ToList();
        }

        private ICollection<QValue> findQValue(Guid mappingId)
        {
            return context.QValues
                .Where(qv => qv.IdMapping == mappingId)
                .Where(qv => qv.deleted == false)
                .ToList();
        }

        private ICollection<Rmr> findRMR(Guid mappingId)
        {
            return context.Rmrs
                .Where(rmr => rmr.IdMapping == mappingId)
                .Where(rmr => rmr.deleted == false)
                .ToList();
        }

        private ICollection<QValueInputSelection> findQValueInputSelection(Guid mappingId)
        {
            return context.QValueInputSelections
                .Where(qv => qv.IdMapping == mappingId)                
                .ToList();
        }

        private ICollection<RmrInputSelection> findRMRInputSelection(Guid mappingId)
        {
            return context.RmrInputSelections
                .Where(rmr => rmr.IdMapping == mappingId)
                .ToList();
        }

        public IEnumerable<Mapping> find(Guid faceId, MappingFilter filter)
        {
            IQueryable<Mapping> query = context.Mappings;

            query = filterMappings(query, faceId, filter);
            query = orderMappings(query, filter.sortBy);
            if (filter.start != 0) {
                query.Skip(filter.start);
            }
            if (filter.range != 0) {
                query.Take(filter.range);
            }
            query.Include("QValues");
            var mappings = query.ToList();

            return mappings;
        }

        public int findAmount(Guid faceId, MappingFilter filter)
        {
            IQueryable<Mapping> query = context.Mappings;
            query = filterMappings(query, faceId, filter);
            
            return query.Count();            
        }

        public void saveChanges()
        {            
            context.SaveChanges();
        }

        private IQueryable<Mapping> filterMappings(IQueryable<Mapping> query, Guid faceId, MappingFilter filter)
        {
            query = query.Where(mp => mp.IdFace == faceId)
                .Where(mp => mp.deleted == false);

            if (filter.userId != null)
            {
                var userId = Guid.Parse(filter.userId);
                query = query.Where(mp => mp.IdUser == userId);
            }

            return query;
        }

        private IQueryable<Mapping> orderMappings(IQueryable<Mapping> query, string sortBy)
        {
            switch (sortBy)
            {
                case "user-asc":
                    query = query.OrderBy(mp => mp.User.FirstName);
                    break;
                case "user-desc":
                    query = query.OrderByDescending(mp => mp.User.FirstName);
                    break;
                case "chainage-start-asc":
                    query = query.OrderBy(mp => mp.ChainageStart);
                    break;
                case "chainage-start-desc":
                    query = query.OrderByDescending(mp => mp.ChainageStart);
                    break;
                case "chainage-end-asc":
                    query = query.OrderBy(mp => mp.ChainageEnd);
                    break;
                case "chainage-end-desc":
                    query = query.OrderByDescending(mp => mp.ChainageEnd);
                    break;                
                case "created-at-asc":
                    query = query.OrderBy(mp => mp.createdAt);
                    break;
                case "created-at-desc":
                    query = query.OrderByDescending(mp => mp.createdAt);
                    break;
                case "updated-at-asc":
                    query = query.OrderBy(mp => mp.updatedAt);
                    break;
                case "updated-at-desc":
                    query = query.OrderByDescending(mp => mp.updatedAt);
                    break;
            }

            return query;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }

        public void updatePicturesRemoteUri(Guid mappingId, int sideCode, string remoteUri, string remoteUriOriginal)
        {
            IQueryable<Picture> pictures = context.Pictures.Where(x => x.IdMapping == mappingId);
            Picture picture = pictures.SingleOrDefault(x => x.Code == sideCode);
            if (string.IsNullOrEmpty(picture.RemoteUri))
            {
                picture.RemoteUri = remoteUri.Trim();
                picture.RemoteUriOriginal = remoteUriOriginal.Trim();
            }
        }

        public IEnumerable<Picture> findPictures(Guid mappingId)
        {
            return context.Pictures.Where(x => x.IdMapping == mappingId);
        }

    }
}
