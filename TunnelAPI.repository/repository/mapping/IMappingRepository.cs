﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunnelAPI.repository.model;

namespace TunnelAPI.repository.mapping
{
    public interface IMappingRepository
    {
        Mapping findOne(Guid mappingId);
        IEnumerable<Mapping> find(Guid faceId, MappingFilter filter);
        int findAmount(Guid faceId, MappingFilter filter);
        void saveChanges();
        void updatePicturesRemoteUri(Guid mappingId, int sideCode, string remoteUri, string remoteUriOriginal);
        IEnumerable<Picture> findPictures(Guid mappingId);
    }
}
