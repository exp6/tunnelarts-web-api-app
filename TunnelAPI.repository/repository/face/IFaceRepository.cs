﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.face
{
    public interface IFaceRepository
    {
        ICollection<Face> find(FaceFilter filter);
        int findAmount(FaceFilter filter);
        Face findOne(Guid faceId);
        void create(Face face);
        void remove(Guid facelId);
        void saveChanges();
    }
}
