﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.face
{
    public class FaceRepository : IFaceRepository, IDisposable
    {
        private readonly TunnelArtsEntities context;
        private bool disposed = false;

        public FaceRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public void create(Face face)
        {
            context.Faces.Add(face);
        }

        public ICollection<Face> find(FaceFilter filter)
        {
            Guid clientId = filter.clientId != null ? Guid.Parse(filter.clientId) : Guid.Empty;
            IQueryable<Face> query = filter.clientId != null ? context.Faces.Where(x => x.Tunnel.Project.IdClient == clientId) : context.Faces;

            query = filterFaces(query, filter);
            query = sortFaces(query, filter.sortBy);

            if (filter.range > 0)
                return query.Skip(filter.start)
                .Take(filter.range)
                .Include("Tunnel")
                .Include("Tunnel.Project")
                .ToList();
            else
                return query.ToList();
        }

        public int findAmount(FaceFilter filter)
        {
            Guid clientId = filter.clientId != null ? Guid.Parse(filter.clientId) : Guid.Empty;
            IQueryable<Face> query = filter.clientId != null ? context.Faces.Where(x => x.Tunnel.Project.IdClient == clientId) : context.Faces;
            query = filterFaces(query, filter);

            return query.Count();
        }

        private IQueryable<Face> filterFaces(IQueryable<Face> query, FaceFilter filter)
        {            
            if (filter.query != null)           
                query = query.Where(fc => 
                    fc.Name.Contains(filter.query) 
                    || fc.Tunnel.Name.Contains(filter.query)
                    || fc.Tunnel.Project.Name.Contains(filter.query)
                );
            if (filter.tunnelId.CompareTo(Guid.Empty) != 0)
                query = query.Where(tn => tn.IdTunnel == filter.tunnelId);            
 
            return query;
        }

        private IQueryable<Face> sortFaces(IQueryable<Face> query, string sortBy)
        {
            switch (sortBy)
            {
                case "project-asc":
                    query = query.OrderBy(fc => fc.Tunnel.Project.Name);
                    break;
                case "project-desc":
                    query = query.OrderByDescending(fc => fc.Tunnel.Project.Name);
                    break;
                case "tunnel-asc":
                    query = query.OrderBy(fc => fc.Tunnel.Name);
                    break;
                case "tunnel-desc":
                    query = query.OrderByDescending(fc => fc.Tunnel.Name);
                    break;
                case "name-asc":
                    query = query.OrderBy(tn => tn.Name);
                    break;
                case "name-desc":
                    query = query.OrderByDescending(tn => tn.Name);
                    break;
                case "chainage-asc":
                    query = query.OrderBy(fc => fc.RefChainage);
                    break;
                case "chainage-desc":
                    query = query.OrderByDescending(fc => fc.RefChainage);
                    break;
                case "orientation-asc":
                    query = query.OrderBy(fc => fc.Orientation);
                    break;
                case "orientation-desc":
                    query = query.OrderByDescending(fc => fc.Orientation);
                    break;
                case "inclination-asc":
                    query = query.OrderBy(fc => fc.Inclination);
                    break;
                case "inclination-desc":
                    query = query.OrderByDescending(fc => fc.Inclination);
                    break;
            }

            return query;
        }

        public Face findOne(Guid faceId)
        {
            return context.Faces
                .Where(fc => fc.id == faceId)
                .Include("Tunnel")
                .Include("Tunnel.Project")
                .FirstOrDefault();
        }

        public void remove(Guid faceId)
        {
            var face = findOne(faceId);
            if (face.Mappings.Count() == 0)
                context.Faces.Remove(face);
            else
                throw new InvalidOperationException("The face cannot be deleted because there are data associated to it");            
        }

        public void saveChanges()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }
    }
}
