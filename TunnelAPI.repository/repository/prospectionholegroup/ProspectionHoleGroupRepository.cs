﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TunelAPI.repository.model;

namespace TunelAPI.repository.repository.prospectionholegroup
{
    public class ProspectionHoleGroupRepository : IProspectionHoleGroupRepository, IDisposable
    {
        private TunnelArtsEntities context;
        private bool disposed = false;

        public ProspectionHoleGroupRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public ProspectionHoleGroup findOne(Guid prospectionHoleGroupId)
        {
            var prospectionHoleGroup = context.ProspectionHoleGroups.FirstOrDefault(x => x.id == prospectionHoleGroupId);
            return prospectionHoleGroup;
        }

        public IEnumerable<ProspectionHoleGroup> find(Guid faceId, ProspectionHoleGroupFilter filter)
        {
            IQueryable<ProspectionHoleGroup> query = context.ProspectionHoleGroups.Where(x => x.deleted == false);
            query = filterMappings(query, faceId, filter);
            query = orderMappings(query, filter.sortBy);

            if (filter.start > 0) {
                query.Skip(filter.start);
            }
            if (filter.range > 0) {
                query.Take(filter.range);
            }

            var prospectionHoleGroups = query.ToList();

            return prospectionHoleGroups;
        }

        public int findAmount(Guid faceId, ProspectionHoleGroupFilter filter)
        {
            IQueryable<ProspectionHoleGroup> query = context.ProspectionHoleGroups.Where(x => x.deleted == false);
            query = filterMappings(query, faceId, filter);

            return query.Count();
        }

        private IQueryable<ProspectionHoleGroup> filterMappings(IQueryable<ProspectionHoleGroup> query, Guid faceId, ProspectionHoleGroupFilter filter)
        {
            query = query.Where(x => x.IdFace == faceId).Where(x => x.deleted == false);

            if (filter.userId != null)
            {
                var userId = Guid.Parse(filter.userId);
                query = query.Where(mp => mp.IdUser == userId);
            }

            return query;
        }

        private IQueryable<ProspectionHoleGroup> orderMappings(IQueryable<ProspectionHoleGroup> query, string sortBy)
        {
            switch (sortBy)
            {
                case "user-asc":
                    query = query.OrderBy(mp => mp.User.FirstName);
                    break;
                case "user-desc":
                    query = query.OrderByDescending(mp => mp.User.FirstName);
                    break;
                case "created-at-asc":
                    query = query.OrderBy(mp => mp.createdAt);
                    break;
                case "created-at-desc":
                    query = query.OrderByDescending(mp => mp.createdAt);
                    break;
                case "updated-at-asc":
                    query = query.OrderBy(mp => mp.updatedAt);
                    break;
                case "updated-at-desc":
                    query = query.OrderByDescending(mp => mp.updatedAt);
                    break;
            }

            return query;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }

        public int findAmountProbeHoles(Guid prospectionHoleGroupId)
        {
            ProspectionHoleGroup phg = context.ProspectionHoleGroups.FirstOrDefault(x => x.id == prospectionHoleGroupId);
            return phg == null ? 0 : phg.ProspectionHoles.Where(x => x.deleted == false).ToList().Count;
        }

        public int findAmountRods(Guid prospectionHoleGroupId)
        {
            ProspectionHoleGroup phg = context.ProspectionHoleGroups.FirstOrDefault(x => x.id == prospectionHoleGroupId);
            int rods = 0;

            foreach (ProspectionHole p in phg.ProspectionHoles)
            {
                List<Rod> list = p.Rods.Where(x => x.deleted == false).ToList();
                rods += list.Count;
            }

            return rods;
        }

        public IEnumerable<Mapping> findMappings(Guid faceId)
        {
            IQueryable<Mapping> mappings = context.Mappings.Where(x => x.IdFace == faceId && x.deleted == false).OrderBy(x => x.createdAt);
            return mappings.ToList();
        }

        public IEnumerable<ProspectionHoleGroup> find(Guid faceId)
        {
            IQueryable<ProspectionHoleGroup> query = context.ProspectionHoleGroups.Where(x => x.IdFace == faceId && x.deleted == false).OrderBy(x => x.createdAt);
            return query.ToList();
        }

        public IEnumerable<ProspectionHoleGroupCalculation> findCalculations(Guid phgId)
        {
            IQueryable<ProspectionHoleGroupCalculation> query = context.ProspectionHoleGroupCalculations.Where(x => x.IdProspectionHoleGroup == phgId && x.deleted == false);
            return query.ToList();
        }

        public IEnumerable<QValue> findQObserved(Guid mappingId)
        {
            IQueryable<QValue> query = context.QValues.Where(x => x.IdMapping == mappingId && x.deleted == false);
            return query.ToList();
        }

        public Mapping findMapping(Guid mappingId)
        {
            return context.Mappings.SingleOrDefault(x => x.id == mappingId);
        }
    }
}
