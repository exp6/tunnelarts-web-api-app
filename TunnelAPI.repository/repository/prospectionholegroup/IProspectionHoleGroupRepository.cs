﻿using System;
using System.Collections.Generic;
using TunelAPI.repository.model;

namespace TunelAPI.repository.repository.prospectionholegroup
{
    public interface IProspectionHoleGroupRepository
    {
        ProspectionHoleGroup findOne(Guid prospectionHoleGroupId);
        IEnumerable<ProspectionHoleGroup> find(Guid faceId, ProspectionHoleGroupFilter filter);
        int findAmount(Guid faceId, ProspectionHoleGroupFilter filter);
        int findAmountProbeHoles(Guid prospectionHoleGroupId);
        int findAmountRods(Guid prospectionHoleGroupId);
        IEnumerable<Mapping> findMappings(Guid faceId);
        IEnumerable<ProspectionHoleGroup> find(Guid faceId);
        IEnumerable<ProspectionHoleGroupCalculation> findCalculations(Guid phgId);
        IEnumerable<QValue> findQObserved(Guid mappingId);
        Mapping findMapping(Guid mappingId);
    }
}
