﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;

namespace TunnelAPI.repository.role
{
    public interface IRoleRepository
    {
        ICollection<Role> find();
        Role findOne(Guid roleId);
    }
}
