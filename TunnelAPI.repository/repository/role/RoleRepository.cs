﻿using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;

namespace TunnelAPI.repository.role
{
    public class RoleRepository : IRoleRepository, IDisposable
    {
        private readonly TunnelArtsEntities context;
        private bool disposed = false;

        public RoleRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public ICollection<Role> find()
        {
            return context.Roles
                .OrderBy(rl => rl.Name)
                .ToList();
        }

        public Role findOne(Guid roleId)
        {
            return context.Roles.FirstOrDefault(role => role.IdRole == roleId);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }
    }
}
