﻿using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.project
{
    public class ProjectRepository : IProjectRepository, IDisposable
    {
        private TunnelArtsEntities context;
        private bool disposed = false;

        public ProjectRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public ICollection<FormationUnit> findFormationUnits(Guid mappingId)
        {
            var query = from mapping in context.Mappings
                        join face in context.Faces on mapping.IdFace equals face.id
                        join tunnel in context.Tunnels on face.IdTunnel equals tunnel.id
                        join formationUnits in context.FormationUnits on tunnel.IdProject equals formationUnits.IdProject
                        where mapping.id == mappingId
                        select formationUnits;
            return query.ToList();
        }

        public ICollection<Project> find(Guid clientId, ProjectFilter filter)
        {
            IQueryable<Project> query = context.Projects;
            query = filterProjects(query, clientId, filter);
            query = orderProjects(query, filter.sortBy);

            if (filter.range > 0)
                return query.Skip(filter.start)
                    .Take(filter.range)
                    .ToList();
            else
                return query.ToList();
        }

        private IQueryable<Project> filterProjects(IQueryable<Project> query, Guid clientId, ProjectFilter filter)
        {
            query = query.Where(pr => pr.IdClient == clientId)
                .Where(mp => mp.deleted == false);

            if (filter.query != null)
            {                
                query = query.Where(pr => 
                    pr.Code.Contains(filter.query) || pr.Name.Contains(filter.query) || pr.Description.Contains(filter.query)
                );
            }

            return query;
        }

        private IQueryable<Project> orderProjects(IQueryable<Project> query, string sortBy)
        {
            switch (sortBy)
            {
                case "code-asc":
                    query = query.OrderBy(pr => pr.Code);
                    break;
                case "code-desc":
                    query = query.OrderByDescending(pr => pr.Code);
                    break;
                case "name-asc":
                    query = query.OrderBy(pr => pr.Name);
                    break;
                case "name-desc":
                    query = query.OrderByDescending(pr => pr.Name);
                    break;
                case "description-asc":
                    query = query.OrderBy(pr => pr.Description);
                    break;
                case "description-desc":
                    query = query.OrderByDescending(pr => pr.Description);
                    break;
                case "start-date-asc":
                    query = query.OrderBy(pr => pr.StartDate);
                    break;
                case "start-date-desc":
                    query = query.OrderByDescending(pr => pr.StartDate);
                    break;
                case "finish-date-asc":
                    query = query.OrderBy(pr => pr.FinishDate);
                    break;
                case "finish-date-desc":
                    query = query.OrderByDescending(pr => pr.FinishDate);
                    break;                
            }

            return query;
        }

        public int findAmount(Guid clientId, ProjectFilter filter)
        {
            IQueryable<Project> query = context.Projects;
            query = filterProjects(query, clientId, filter);            

            return query.Count();            
        }

        public Project findOne(Guid projectId)
        {
            return context.Projects
                .Where(pr => pr.id == projectId)
                .FirstOrDefault();
        }

        public void create(Project project)
        {                                               
            context.Projects.Add(project);
            enableMapingInputs(project);
        }

        private void enableMapingInputs(Project project)
        {
            ICollection<MappingInput> inputs = findMappingOptions();
            List<ProjectMappingInput> newMappingInputs = new List<ProjectMappingInput>();
            foreach (MappingInput mi in inputs)
            {
                ProjectMappingInput pmi = new ProjectMappingInput
                {
                    id = Guid.NewGuid(),
                    IdProject = project.id,
                    IdMappingInput = mi.id,
                    deleted = false
                };
                newMappingInputs.Add(pmi);
            }
            context.ProjectMappingInputs.AddRange(newMappingInputs);
        }

        public void remove(Guid projectId)
        {
            var project = findOne(projectId);

            if (project.Tunnels.Count() != 0 || project.UserProjects.Count() != 0)
                throw new InvalidOperationException("The project cannot be deleted because there are data associated to it");
            else
                context.Projects.Remove(project); 
        }

        public void saveChanges()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }

        public void updateRockQualities(Guid projectId, IEnumerable<RockQuality> rockqualities)
        {
            Project project = findOne(projectId);

            foreach (RockQuality rq in rockqualities)
            {
                rq.id = Guid.NewGuid();
                rq.deleted = false;
                rq.IdProject = projectId;
                rq.Project = project;
            }

            context.RockQualities.RemoveRange(context.RockQualities.Where(x => x.IdProject == projectId));
            context.RockQualities.AddRange(rockqualities);
        }

        public void updateFormationUnits(Guid projectId, IEnumerable<FormationUnit> formationUnits)
        {
            Project project = findOne(projectId);

            foreach (FormationUnit fu in formationUnits)
            {
                fu.id = Guid.NewGuid();
                fu.deleted = false;
                fu.IdProject = projectId;
                fu.Project = project;
            }

            context.FormationUnits.RemoveRange(context.FormationUnits.Where(x => x.IdProject == projectId));
            context.FormationUnits.AddRange(formationUnits);
        }

        public ClientSubscription findSubscription(Guid clientId)
        {
            return context.ClientSubscriptions
                .Where(pr => pr.IdClient == clientId)
                .FirstOrDefault();
        }

        public ICollection<MappingInput> findMappingOptions()
        {
            return context.MappingInputs.ToList();
        }

        public void updateMappingOptions(Guid projectId, IEnumerable<MappingInput> mappingOptions)
        {
            Project project = findOne(projectId);
            List<ProjectMappingInput> newMappingInputs = new List<ProjectMappingInput>();

            List<Guid> finalMappingOptions = new List<Guid>();
            List<MappingInput> newElements = new List<MappingInput>();

            List<MappingInput> totalElements = context.ProjectMappingInputs.Where(x => x.IdProject == projectId).Select(x => x.MappingInput).ToList();
            bool exists;
            foreach (MappingInput mi in mappingOptions)
            {
                exists = false;
                foreach (MappingInput mi2 in totalElements)
                {
                    if (mi2.Label.Trim() == mi.Label.Trim())
                    {
                        exists = true;
                        break;
                    }
                }

                if (!exists) newElements.Add(mi);
            }

            foreach (MappingInput mi in mappingOptions)
            {
                finalMappingOptions.Add(mi.id);
            }

            List<Guid> totalMappingOptions = context.ProjectMappingInputs.Where(x => x.IdProject == projectId).Select(x => x.IdMappingInput).ToList();
            List<Guid> deletedMappingOptions = totalMappingOptions.Except(finalMappingOptions).Union(finalMappingOptions.Except(totalMappingOptions)).ToList();
            List<ProjectMappingInput> deleteItems = context.ProjectMappingInputs.Where(x => deletedMappingOptions.Contains(x.IdMappingInput) && x.IdProject == projectId).ToList();

            List<ProjectMappingInput> finalDeleteItems = new List<ProjectMappingInput>();
            MappingInput mit;
            foreach (ProjectMappingInput pmi in deleteItems)
            {
                mit = pmi.MappingInput;
                exists = false;
                foreach (MappingInput newMi in newElements)
                {
                    if (newMi.Label.Trim() == mit.Label.Trim())
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists) continue;
                finalDeleteItems.Add(pmi);
            }

            //context.ProjectMappingInputs.RemoveRange(finalDeleteItems);

            foreach (ProjectMappingInput pmi in finalDeleteItems)
            {
                pmi.deleted = true;
            }

            List<ProjectMappingInput> totalItems = context.ProjectMappingInputs.Where(x => x.IdProject == projectId).ToList();

            bool existedNew;
            foreach (MappingInput mi in mappingOptions)
            {
                existedNew = false;
                foreach (ProjectMappingInput pmi2 in totalItems)
                {
                    if (pmi2.MappingInput.Label.Trim() == mi.Label.Trim())
                    {
                        pmi2.deleted = false;
                        existedNew = true;
                        break;
                    }

                }

                if (existedNew) continue;
            }


            foreach (MappingInput mi in newElements)
            {
                ProjectMappingInput pmi = new ProjectMappingInput
                {
                    id = Guid.NewGuid(),
                    IdProject = project.id,
                    IdMappingInput = mi.id,
                    deleted = false
                };

                newMappingInputs.Add(pmi);
            }
            context.ProjectMappingInputs.AddRange(newMappingInputs);
        }
    }
}
