﻿using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.project
{
    public interface IProjectRepository
    {        
        Project findOne(Guid projectId);
        ICollection<Project> find(Guid clientId, ProjectFilter filter);
        ICollection<FormationUnit> findFormationUnits(Guid mappingId);
        int findAmount(Guid clientId, ProjectFilter filter);
        void create(Project project);
        void remove(Guid projectId);
        void saveChanges();
        void updateRockQualities(Guid projectId, IEnumerable<RockQuality> rockqualities);
        void updateFormationUnits(Guid projectId, IEnumerable<FormationUnit> formationUnits);
        ClientSubscription findSubscription(Guid clientId);
        ICollection<MappingInput> findMappingOptions();
        void updateMappingOptions(Guid projectId, IEnumerable<MappingInput> mappingOptions);
    }
}
