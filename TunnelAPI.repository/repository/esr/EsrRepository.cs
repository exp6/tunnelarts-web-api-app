﻿using System;
using System.Collections.Generic;
using System.Linq;
using TunelAPI.repository;

namespace TunnelAPI.repository.esr
{
    public class EsrRepository : IEsrRepository, IDisposable
    {
        private readonly TunnelArtsEntities context;
        private bool disposed = false;

        public EsrRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public ICollection<Esr> find()
        {
            return context.Esrs
                .OrderBy(esr => esr.Description)
                .ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }
    }
}
