﻿using System.Collections.Generic;
using TunelAPI.repository;

namespace TunnelAPI.repository.esr
{
    public interface IEsrRepository
    {
        ICollection<Esr> find();
    }
}
