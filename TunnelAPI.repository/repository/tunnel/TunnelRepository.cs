﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.tunnel
{
    public class TunnelRepository : ITunnelRepository, IDisposable
    {
        private readonly TunnelArtsEntities context;
        private bool disposed = false;

        public TunnelRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public void create(Tunnel tunnel)
        {
            context.Tunnels.Add(tunnel);
        }

        public ICollection<Tunnel> find(TunnelFilter filter)
        {
            Guid clientId = filter.clientId != null ? Guid.Parse(filter.clientId) : Guid.Empty;
            IQueryable<Tunnel> query = filter.clientId != null ? context.Tunnels.Where(x => x.Project.IdClient == clientId) : context.Tunnels;
            query = filterTunnels(query, filter);
            query = sortTunnels(query, filter.sortBy);

            if (filter.range > 0)
                return query.Skip(filter.start)
                .Take(filter.range)
                .ToList();
            else 
                return query.ToList();
        }

        public int findAmount(TunnelFilter filter)
        {
            Guid clientId = filter.clientId != null ? Guid.Parse(filter.clientId) : Guid.Empty;
            IQueryable<Tunnel> query = filter.clientId != null ?  context.Tunnels.Where(x => x.Project.IdClient == clientId) : context.Tunnels;
            query = filterTunnels(query, filter);

            return query.Count();
        }

        private IQueryable<Tunnel> filterTunnels(IQueryable<Tunnel> query, TunnelFilter filter)
        {            
            if (filter.query != null)           
                query = query.Where(tn => tn.Name.Contains(filter.query) || tn.Project.Name.Contains(filter.query));
            if (filter.projectId.CompareTo(Guid.Empty) != 0)
                query = query.Where(tn => tn.IdProject == filter.projectId);            
 
            return query;
        }

        private IQueryable<Tunnel> sortTunnels(IQueryable<Tunnel> query, string sortBy)
        {
            switch (sortBy)
            {
                case "project-asc":
                    query = query.OrderBy(tn => tn.Project.Name);
                    break;
                case "project-desc":
                    query = query.OrderByDescending(tn => tn.Project.Name);
                    break;
                case "name-asc":
                    query = query.OrderBy(tn => tn.Name);
                    break;
                case "name-desc":
                    query = query.OrderByDescending(tn => tn.Name);
                    break;
                case "esr-asc":
                    query = query.OrderBy(tn => tn.Esr.Description);
                    break;
                case "esr-desc":
                    query = query.OrderByDescending(tn => tn.Esr.Description);
                    break;
                case "span-asc":
                    query = query.OrderBy(tn => tn.Span);
                    break;
                case "span-desc":
                    query = query.OrderByDescending(tn => tn.Span);
                    break;                
            }

            return query;
        }

        public Tunnel findOne(Guid tunnelId)
        {
            return context.Tunnels
                .Where(tn => tn.id == tunnelId)
                .Include("Project")
                .Include("Esr")
                .FirstOrDefault();
        }

        public void remove(Guid tunnelId)
        {
            var tunnel = findOne(tunnelId);
            if (tunnel.Faces.Count() == 0)
                context.Tunnels.Remove(tunnel);
            else
                throw new InvalidOperationException("The tunnel cannot be deleted because there are data associated to it");            
        }

        public void saveChanges()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }

        public ICollection<Face> findFaces(Guid tunnelId)
        {
            return context.Faces.Where(x => x.IdTunnel == tunnelId).ToList();
        }

        public bool saveOfferRockTypes(Guid tunnelId, List<double> initialProgresses, List<double> finalProgresses, List<int> rocktypes)
        {
            IEnumerable<OfferRockType> offers = context.OfferRockTypes.Where(x => x.IdTunnel == tunnelId);

            if (offers.Count() > 0)
            {
                context.OfferRockTypes.RemoveRange(offers);
            }

            List<OfferRockType> offersList = new List<OfferRockType>();
            OfferRockType offer;

            for (int i = 0; i < initialProgresses.Count; i++)
            {
                offer = new OfferRockType()
                {
                    id = Guid.NewGuid(),
                    IdTunnel = tunnelId,
                    InitialProgress = initialProgresses.ElementAt(i),
                    FinalProgress = finalProgresses.ElementAt(i),
                    RockType = rocktypes.ElementAt(i),
                    CreatedAt = DateTime.Now,
                    UpdateAt = DateTime.Now
                };

                offersList.Add(offer);
            }
            context.OfferRockTypes.AddRange(offersList);

            return context.SaveChanges() > 0;
        }

        public ICollection<Mapping> findMappings(Guid tunnelId) {
            ICollection<Face> faces = findFaces(tunnelId);
            List<Mapping> mappingsForTunnel = new List<Mapping>();
            foreach (Face face in faces) {
                List<Mapping> mappingsForFace = context.Mappings.Where(x => x.Face.id == face.id && x.deleted == false).ToList();
                mappingsForTunnel.AddRange(mappingsForFace);
            }
            return mappingsForTunnel;
        }
    }
}
