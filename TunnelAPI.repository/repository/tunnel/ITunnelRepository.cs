﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.tunnel
{
    public interface ITunnelRepository
    {
        ICollection<Tunnel> find(TunnelFilter filter);
        int findAmount(TunnelFilter filter);
        Tunnel findOne(Guid tunnelId);
        void create(Tunnel tunnel);
        void remove(Guid tunnelId);
        void saveChanges();
        ICollection<Face> findFaces(Guid tunnelId);
        bool saveOfferRockTypes(Guid tunnelId, List<double> initialProgresses, List<double> finalProgresses, List<int> rocktypes);
    }
}
