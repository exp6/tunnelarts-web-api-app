﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunelAPI.repository.repository.tunnelprofile
{
    public interface ITunnelProfileRepository
    {
        ICollection<TunnelProfile> find();
    }
}
