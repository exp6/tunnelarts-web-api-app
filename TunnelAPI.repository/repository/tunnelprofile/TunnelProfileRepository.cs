﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TunelAPI.repository.repository.tunnelprofile
{
    public class TunnelProfileRepository : ITunnelProfileRepository
    {
        private readonly TunnelArtsEntities context;
        private bool disposed = false;

        public TunnelProfileRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public ICollection<TunnelProfile> find()
        {
            return context.TunnelProfiles
               .OrderBy(tp => tp.Code)
               .ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }
    }
}
