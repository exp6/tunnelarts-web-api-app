﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.user
{
    public class UserRepository : IUserRepository, IDisposable
    {
        private TunnelArtsEntities context;
        private bool disposed;

        public UserRepository(TunnelArtsEntities context)
        {
            this.context = context;
        }

        public User findOne(string username)
        {
            User u = null;

            try
            {
                IEnumerable<User> users = context.Users;
                u = users.FirstOrDefault(us => us.UserName == username);
            }
            catch (Exception e)
            {

            }

            return u;            
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing && context != null)
                context.Dispose();

            disposed = true;
        }

        public User findOne(Guid userId)
        {
            var user = context.Users
                .Where(us => us.IdUser == userId)
                .Include("UserProjects")
                .Include("UserProjects.Project")
                .FirstOrDefault();
            return user;
        }

        public ICollection<User> find(UserFilter filter)
        {
            var query = context.Users.AsQueryable();
            query = filterUsers(query, filter);
            query = sortUsers(query, filter.sortBy);

            var users = query.Skip(filter.start)
                .Take(filter.range)
                .ToList();
            
            return users;
        }

        public int findAmount(UserFilter filter)
        {
            var total = filterUsers(context.Users, filter)
                .Count();

            return total;
        }

        private IQueryable<User> filterUsers(IQueryable<User> query, UserFilter filter)
        {
            if (filter.query != null)
                query = query.Where(us =>
                    us.UserName.Contains(filter.query)
                    || us.FirstName.Contains(filter.query)
                    || us.LastName.Contains(filter.query)
                    || us.Email.Contains(filter.query)
                );
            if (filter.clientId.CompareTo(Guid.Empty) != 0)
                query = query.Where(us => us.IdClient == filter.clientId);
            return query;
        }

        private IQueryable<User> sortUsers(IQueryable<User> query, string sortBy)
        {
            switch (sortBy)
            {
                case "username-asc":
                    query = query.OrderBy(us => us.UserName);
                    break;
                case "username-desc":
                    query = query.OrderByDescending(us => us.UserName);
                    break;
                case "firstname-asc":
                    query = query.OrderBy(us => us.FirstName);
                    break;
                case "firstname-desc":
                    query = query.OrderByDescending(us => us.LastName);
                    break;
                case "lastname-asc":
                    query = query.OrderBy(us => us.LastName);
                    break;
                case "lastname-desc":
                    query = query.OrderByDescending(us => us.LastName);
                    break;
                case "contact-asc":
                    query = query.OrderBy(us => us.Contact);
                    break;
                case "contact-desc":
                    query = query.OrderByDescending(us => us.Contact);
                    break;
                case "email-asc":
                    query = query.OrderBy(us => us.Email);
                    break;
                case "email-desc":
                    query = query.OrderByDescending(us => us.Email);
                    break;
                case "created-at-asc":
                    query = query.OrderBy(us => us.CreatedAt);
                    break;
                case "created-at-desc":
                    query = query.OrderByDescending(us => us.CreatedAt);
                    break;
            }

            return query;
        }

        public void create(User user)
        {
            foreach (Role role in user.Roles)
                context.Entry(role).State = EntityState.Unchanged;

            context.Users.Add(user);
        }

        public void update(User user)
        {
            context.Users.Attach(user);                
        }

        public void remove(Guid userId)
        {
            var user = findOne(userId);
            user.Roles.Clear();
            context.Users.Remove(user);
        }

        public void saveChanges() {
            context.SaveChanges();
        }

        public void deactivate(Guid userId)
        {
            var user = findOne(userId);
            user.Enabled = false;
        }
    }
}
