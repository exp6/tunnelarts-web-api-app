﻿using System;
using System.Collections.Generic;
using TunelAPI.repository;
using TunelAPI.repository.model;

namespace TunnelAPI.repository.user
{
    public interface IUserRepository
    {
        User findOne(string username);
        User findOne(Guid userId);
        ICollection<User> find(UserFilter filter);
        int findAmount(UserFilter filter);        
        void create(User user);
        void update(User user);
        void remove(Guid userId);
        void deactivate(Guid userId);
        void saveChanges();
    }
}
