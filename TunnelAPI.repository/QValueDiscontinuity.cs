//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TunelAPI.repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class QValueDiscontinuity
    {
        public System.Guid id { get; set; }
        public Nullable<System.Guid> IdQValue { get; set; }
        public Nullable<int> Discontinuity { get; set; }
        public Nullable<double> Jr { get; set; }
        public Nullable<double> Ja { get; set; }
        public Nullable<bool> Completed { get; set; }
        public Nullable<bool> deleted { get; set; }
        public byte[] version { get; set; }
        public System.DateTimeOffset createdAt { get; set; }
        public System.DateTimeOffset updatedAt { get; set; }
        public Nullable<System.Guid> JrSelection { get; set; }
        public Nullable<System.Guid> JaSelection { get; set; }
    
        public virtual QValue QValue { get; set; }
    }
}
