//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TunelAPI.repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserProject
    {
        public System.Guid IdUser { get; set; }
        public System.Guid IdProject { get; set; }
        public Nullable<bool> deleted { get; set; }
        public byte[] version { get; set; }
        public System.DateTimeOffset createdAt { get; set; }
        public System.DateTimeOffset updatedAt { get; set; }
        public Nullable<System.Guid> id { get; set; }
    
        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
    }
}
