﻿namespace TunelAPI.repository.model
{
    public class ProjectFilter
    {
        public string query { get; set; } = null;
        public int start { get; set; } = 0;
        public int range { get; set; } = 0;
        public string sortBy { get; set; } = "code-asc";
    }
}
