﻿namespace TunnelAPI.repository.model
{
    public class MappingFilter
    {
        public string userId { get; set; } = null;
        public int start { get; set; } = 0;
        public int range { get; set; } = 15;
        public string sortBy { get; set; } = "created-at-desc";
    }
}
