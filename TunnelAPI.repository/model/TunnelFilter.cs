﻿using System;

namespace TunelAPI.repository.model
{
    public class TunnelFilter
    {
        public string query { get; set; } = null;
        public Guid projectId { get; set; } = Guid.Empty;
        public int range { get; set; } = 0;
        public int start { get; set; } = 0;
        public string sortBy { get; set; } = "project-asc";
        public string clientId { get; set; } = null;
    }
}
