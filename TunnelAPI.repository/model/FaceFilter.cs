﻿using System;

namespace TunelAPI.repository.model
{
    public class FaceFilter
    {
        public string query { get; set; } = null;
        public Guid tunnelId { get; set; } = Guid.Empty;
        public int range { get; set; } = 15;
        public int start { get; set; } = 0;
        public string sortBy { get; set; } = "project-asc";
        public string clientId { get; set; } = null;
    }
}
