﻿using System;

namespace TunelAPI.repository.model
{
    public class UserFilter
    {
        public string query { get; set; } = null;
        public Guid clientId { get; set; } = Guid.Empty;
        public int range { get; set; } = 15;
        public int start { get; set; } = 0;
        public string sortBy { get; set; } = "username-asc";
    }
}
