﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TunelAPI.repository.model
{
    public class ProspectionHoleGroupFilter
    {
        public string userId { get; set; } = null;
        public int start { get; set; } = 0;
        public int range { get; set; } = 15;
        public string sortBy { get; set; } = "created-at-asc";
    }
}
