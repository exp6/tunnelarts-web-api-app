//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TunelAPI.repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            this.Projects = new HashSet<Project>();
            this.Users = new HashSet<User>();
            this.ClientSubscriptions = new HashSet<ClientSubscription>();
        }
    
        public System.Guid id { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string LogoUrl { get; set; }
        public string Description { get; set; }
        public Nullable<System.Guid> IdClientType { get; set; }
        public string HexBgColor { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedAt { get; set; }
        public Nullable<bool> deleted { get; set; }
        public byte[] version { get; set; }
        public System.DateTimeOffset updatedAt { get; set; }
    
        public virtual ClientType ClientType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Project> Projects { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClientSubscription> ClientSubscriptions { get; set; }
    }
}
