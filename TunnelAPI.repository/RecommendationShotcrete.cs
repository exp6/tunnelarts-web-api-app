//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TunelAPI.repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class RecommendationShotcrete
    {
        public System.Guid id { get; set; }
        public Nullable<int> Position { get; set; }
        public Nullable<double> Thickness { get; set; }
        public Nullable<int> FiberReinforced { get; set; }
        public Nullable<double> Pk { get; set; }
        public Nullable<int> Location { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
        public Nullable<System.Guid> IdMapping { get; set; }
        public Nullable<bool> Completed { get; set; }
        public Nullable<bool> deleted { get; set; }
        public byte[] version { get; set; }
        public System.DateTimeOffset createdAt { get; set; }
        public System.DateTimeOffset updatedAt { get; set; }
    
        public virtual Mapping Mapping { get; set; }
    }
}
