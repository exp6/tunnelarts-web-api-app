﻿using System;
using System.Security.Principal;

namespace TunelAPI.Filters
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public string password { get; set; }
        public string username { get; set; }
        public Guid userId { get; set; }
        public Guid clientId { get; set; }

        public BasicAuthenticationIdentity(string username, string password)
            : base(username, "Basic")
        {
            this.username = username;
            this.password = password;            
        }            
    }
}