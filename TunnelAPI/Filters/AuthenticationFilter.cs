﻿using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using TunnelAPI.services.services.user;

namespace TunelAPI.Filters
{
    public class AuthenticationFilter : GenericBasicAuthenticationFilter
    {
        public AuthenticationFilter()
        {
        }

        public AuthenticationFilter(bool isActive) : base(isActive)
        {
        }

        protected override bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {
            var provider = actionContext.ControllerContext
                .Configuration
                .DependencyResolver
                .GetService(typeof(IUserService)) as IUserService;

            if (provider != null)
            {
                var isValid = provider.validateCredential(username, password);                
                if (isValid)
                {
                    var user = provider.findOne(username);
                    var identity = new BasicAuthenticationIdentity(username, password);

                    identity.userId = user.IdUser;
                    identity.clientId = user.IdClient;

                    var principal = new GenericPrincipal(identity, null);
                    Thread.CurrentPrincipal = principal;

                    if (HttpContext.Current != null)
                        HttpContext.Current.User = principal;                    
                      
                return true;
                }
            }

            return false;
            
        }
    }
}