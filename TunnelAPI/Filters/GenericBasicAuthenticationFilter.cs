﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace TunelAPI.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class GenericBasicAuthenticationFilter : AuthorizationFilterAttribute
    {
        private readonly bool isActive = false;

        public GenericBasicAuthenticationFilter()
        {
        }

        public GenericBasicAuthenticationFilter(bool isActive)
        {
            this.isActive = isActive;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {

            if (isActive)
            {
                var identity = ParseAutherizationHeader(actionContext);
                if (identity == null)
                {
                    Challange(actionContext);
                    return;
                }

                if (!OnAuthorizeUser(identity.Name, identity.password, actionContext))
                {
                    Challange(actionContext);
                    return;
                }
                
                base.OnAuthorization(actionContext);
            }            
        }

        protected virtual bool OnAuthorizeUser(string username, string password, HttpActionContext actionContext)
        {            
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;
            return true;
        }

        protected virtual BasicAuthenticationIdentity ParseAutherizationHeader(HttpActionContext actionContext)
        {
            string authHeader = null;

            var auth = actionContext.Request.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
                authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader))
                return null;

            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));

            var credentials = authHeader.Split(':');
            if (credentials.Length < 2)
                return null;
                        
            return new BasicAuthenticationIdentity(credentials[0], credentials[1]);
        }       

        void Challange(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}