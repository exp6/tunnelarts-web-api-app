﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunelAPI.Models
{
    public class Credential
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}