﻿using System.Web.Http;
using TunelAPI.App_Start;

namespace TunelAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {            
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(UnityConfig.Register);            
        }
    }
}
