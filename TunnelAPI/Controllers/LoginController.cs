﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Models;
using TunnelAPI.services.services.user;

namespace TunelAPI.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private readonly IUserService userService;        

        public LoginController(IUserService userService)
        {
            this.userService = userService;            
        }
        
        [HttpPost]        
        public HttpResponseMessage validateCredential(Credential credential)
        {
            var isValid = userService.validateCredential(credential.username, credential.password);
                                             
            if (isValid)
            {
                var user = userService.findOne(credential.username);

                if (!user.Enabled)
                {
                    var error1 = new { msg = "The user has been disabled. Contact the administrator for more information." };
                    return Request.CreateResponse(HttpStatusCode.NotFound, error1);
                }

                return Request.CreateResponse(HttpStatusCode.OK, user);
            }

            var error = new { msg = "The username or password provided is incorrect" };
            return Request.CreateResponse(HttpStatusCode.NotFound, error);
            /*var user = userService.findOne(credential.username);
            return Request.CreateResponse(HttpStatusCode.OK, user);*/
        }
    }
}
