﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.user;

namespace TunelAPI.Controllers
{
    [RoutePrefix("api")]
    public class UsersController : ApiController
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [Route("users")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find([FromUri] UserFilter filter)
        {
            try
            {
                var identity = User.Identity as BasicAuthenticationIdentity;

                filter = (filter == null) ? new UserFilter() : filter;
                filter.clientId = identity.clientId;

                var users = userService.find(filter);
                var total = userService.findAmount(filter);

                var data = new { total = total, users = users };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("users")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage create(UserDTO user)
        {
            try
            {
                var identity = User.Identity as BasicAuthenticationIdentity;
                user.CreatedBy = identity.username;
                user.IdClient = identity.clientId;
                user.IdUser = Guid.NewGuid();

                userService.create(user);

                return Request.CreateResponse(HttpStatusCode.Created, userService.findOne(user.IdUser));
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        [HttpPut]
        [Route("users/{userId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(Guid userId, UserDTO user)
        {
            user.IdUser = userId;
            userService.update(user);

            return findOne(userId);
        }

        [HttpPut]
        [Route("users/personalInformation")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage updatePersonalInformation(UserDTO user)
        {
            var identity = User.Identity as BasicAuthenticationIdentity;
            user.IdUser = identity.userId;
            userService.update(user);

            return findOne(user.IdUser);
        }

        [HttpGet]
        [Route("users/{userId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(Guid userId)
        {
            var user = userService.findOne(userId);
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        [HttpDelete]
        [Route("users/{userId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage remove(Guid userId)
        {
            try
            {
                userService.remove(userId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        [HttpPut]
        [Route("users/status")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage updateStatus(UserDTO user)
        {
            try
            {
                userService.update(user);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode httpCode, string message)
        {
            var data = new { msg = message };
            return Request.CreateResponse(httpCode, data);
        }
    }
}
