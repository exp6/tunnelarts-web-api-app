﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.tunnel;

namespace TunelAPI.Controllers
{       
    [RoutePrefix("api")]
    public class TunnelsController : ApiController
    {
        private readonly ITunnelService tunnelService;

        public TunnelsController(ITunnelService tunnelService)
        {
            this.tunnelService = tunnelService;
        }

        [HttpGet]
        [Route("tunnels")]
        [AuthenticationFilter(true)]     
        public HttpResponseMessage find([FromUri] TunnelFilter filter)
        {            
            if (filter == null)
                filter = new TunnelFilter();                              

            var tunnels = tunnelService.find(filter);
            var total = tunnelService.findAmount(filter);

            var data = new { total = total, tunnels = tunnels };
            return Request.CreateResponse(HttpStatusCode.OK, data);            
        }

        [HttpGet]
        [Route("tunnels/progress")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findProgress(string projectId)
        {
            //OBTENER PROGRESOS DE TUNELES A PARTIR DE FACES.
            var tunnels = tunnelService.find(filter);
            var total = tunnelService.findAmount(filter);

            var data = new { total = total, tunnels = tunnels };
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("tunnels")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage create(TunnelDTO tunnel)
        {
            var identity = User.Identity as BasicAuthenticationIdentity;
            tunnel.CreatedBy = identity.username;
            tunnel.id = Guid.NewGuid();
            tunnelService.create(tunnel);

            return findOne(tunnel.id);
        }

        [HttpPut]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(Guid tunnelId, TunnelDTO tunnel)
        {
            tunnel.id = tunnelId;
            tunnelService.update(tunnel);

            return findOne(tunnelId);
        }

        [HttpGet]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(Guid tunnelId)
        {
            var tunnel = tunnelService.findOne(tunnelId);
            return Request.CreateResponse(HttpStatusCode.OK, tunnel);
        }

        [HttpDelete]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage remove(Guid tunnelId)
        {
            try { 
                tunnelService.remove(tunnelId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode httpCode, string message)
        {
            var data = new { msg = message };
            return Request.CreateResponse(httpCode, data);
        }
    }
}
