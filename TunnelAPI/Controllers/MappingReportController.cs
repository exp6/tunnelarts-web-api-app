﻿using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TunnelAPI.Models;

namespace TunnelAPI.Controllers
{
    public class MappingReportController : Controller
    {
        public ActionResult GenerateReport(MappingReportViewModel oModel)
        {
            return View(oModel);
        }

        public byte[] GetPdfBytesFormView(MappingReportViewModel oModel)
        {
            var actionPDF = new Rotativa.ActionAsPdf("GenerateReport", oModel)
            {
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageMargins = { Left = 6, Right = 7 }
            };

            byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);
            return applicationPDFData;
        }
    }
}