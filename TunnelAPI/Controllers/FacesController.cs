﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.face;

namespace TunelAPI.Controllers
{
    [RoutePrefix("api")]
    public class FacesController : ApiController
    {
        private readonly IFaceService faceService;

        public FacesController(IFaceService faceService)
        {
            this.faceService = faceService;
        }

        [HttpGet]
        [Route("faces")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find([FromUri] FaceFilter filter)
        {
            try
            {
                if (filter == null)

                    filter = new FaceFilter();

                var faces = faceService.find(filter);
                var total = faceService.findAmount(filter);

                var data = new { total = total, faces = faces };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("faces")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage create(FaceDTO face)
        {
            var identity = User.Identity as BasicAuthenticationIdentity;
            face.CreatedBy = identity.username;
            face.id = Guid.NewGuid();
            faceService.create(face);

            return findOne(face.id);
        }

        [HttpPut]
        [Route("faces/{faceId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(Guid faceId, FaceDTO face)
        {
            face.id = faceId;
            faceService.update(face);

            return findOne(faceId);
        }

        [HttpGet]
        [Route("faces/{faceId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(Guid faceId)
        {
            var face = faceService.findOne(faceId);
            return Request.CreateResponse(HttpStatusCode.OK, face);
        }

        [HttpDelete]
        [Route("faces/{faceId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage remove(Guid faceId)
        {
            try
            {
                faceService.remove(faceId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode httpCode, string message)
        {
            var data = new { msg = message };
            return Request.CreateResponse(httpCode, data);
        }
    }
}
