﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunnelAPI.services.services.mapping;

namespace TunelAPI.Controllers
{
    [RoutePrefix("api/characterizations")]
    public class CharacterizationsController : ApiController
    { 
        private readonly ICharacterizationService service;

        public CharacterizationsController(ICharacterizationService service)
        {
            this.service = service;
        }

        [HttpGet]
        public HttpResponseMessage find()
        {
            var characterization = service.find();
            return Request.CreateResponse(HttpStatusCode.OK, characterization);
        }
    }
}
