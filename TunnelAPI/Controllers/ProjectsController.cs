﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.project;

namespace TunelAPI.Controllers
{    
    [RoutePrefix("api")]    
    public class ProjectsController : ApiController
    {
        private readonly IProjectService projectService;

        public ProjectsController(IProjectService projectService)
        {
            this.projectService = projectService;
        }

        [HttpGet]
        [Route("projects/{mappingId}/findFormationUnits")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findFormationUnits(Guid mappingId)
        {
            try
            {
                var formationUnits = projectService.findFormationUnits(mappingId);
                return Request.CreateResponse(HttpStatusCode.OK, formationUnits);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("projects")]
        [AuthenticationFilter(true)]        
        public HttpResponseMessage find([FromUri] ProjectFilter filter)
        {
            try
            {
                if (filter == null)
                    filter = new ProjectFilter();

                var identity = User.Identity as BasicAuthenticationIdentity;

                var projects = projectService.find(identity.clientId, filter);
                var total = projectService.findAmount(identity.clientId, filter);

                var data = new { total = total, projects = projects };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }          
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("projects")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage create(ProjectDTO project)
        {
            try
            {
                var identity = User.Identity as BasicAuthenticationIdentity;

                project.CreatedBy = identity.username;
                project.IdClient = identity.clientId;
                project.id = Guid.NewGuid();              
                projectService.create(project);
                
                return Request.CreateResponse(HttpStatusCode.Created, project);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPut]
        [Route("projects/{projectId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(string projectId, ProjectDTO project)
        {
            try
            {                                
                project.id = Guid.Parse(projectId);
                projectService.update(project);

                return Request.CreateResponse(HttpStatusCode.OK, project);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("projects/{projectId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(Guid projectId)
        {
            try
            {                
                var project = projectService.findOne(projectId);
                return Request.CreateResponse(HttpStatusCode.OK, project);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpDelete]
        [Route("projects/{projectId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage remove(Guid projectId)
        {
            try
            {
                projectService.remove(projectId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        [HttpPut]
        [Route("projects/{projectId}/rock-qualities")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage updateRockQualities(string projectId, ProjectDTO project)
        {
            try
            {
                project.id = Guid.Parse(projectId);
                projectService.updateRockQualities(project);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPut]
        [Route("projects/{projectId}/formation-unit")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage updateFormationUnit(string projectId, ProjectDTO project)
        {
            try
            {
                project.id = Guid.Parse(projectId);
                projectService.updateFormationUnit(project);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet]
        [Route("subscription/{clientId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find(string clientId)
        {
            var subscription = projectService.findSubscription(Guid.Parse(clientId));

            var data = new { subscription = subscription };
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpGet]
        [Route("projects/{projectId}/mapping-options")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findMappingInputs()
        {
            try
            {
                var mappingOptions = projectService.findMappingOptions();
                return Request.CreateResponse(HttpStatusCode.OK, mappingOptions);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPut]
        [Route("projects/{projectId}/update-mapping-options")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage updateMappingOptions(string projectId, ProjectDTO project)
        {
            try
            {
                project.id = Guid.Parse(projectId);
                projectService.updateMappingOptions(project);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode code, string message)
        {
            var data = new { msg = message };
            return Request.CreateResponse(code, data);
        }
    }
}
