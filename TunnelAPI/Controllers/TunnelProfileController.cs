﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunnelAPI.services.services.tunnelprofile;

namespace TunnelAPI.Controllers
{
    [RoutePrefix("api")]
    public class TunnelProfileController : ApiController
    {
        private readonly ITunnelProfileService tunnelProfileService;

        public TunnelProfileController(ITunnelProfileService tunnelProfileService)
        {
            this.tunnelProfileService = tunnelProfileService;
        }

        [HttpGet]
        [Route("tunnelprofile")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find()
        {
            var tunnelProfiles = tunnelProfileService.find();
            var data = new { tunnelProfiles = tunnelProfiles };
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
    }
}
