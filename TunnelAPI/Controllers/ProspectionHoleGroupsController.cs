﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunnelAPI.services.services.prospection;
using TunelAPI.repository.model;
using TunelAPI.Filters;
using System.Net.Http.Headers;

namespace TunelAPI.Controllers
{
    public class ProspectionHoleGroupsController : ApiController
    {
        private readonly IProspectionHoleGroupService prospectionHoleGroupService;
        private readonly IProspectionHoleGroupExporterService prospectionHoleGroupExporterService;

        public ProspectionHoleGroupsController(IProspectionHoleGroupService prospectionHoleGroupService, IProspectionHoleGroupExporterService prospectionHoleGroupExporterService)
        {
            this.prospectionHoleGroupService = prospectionHoleGroupService;
            this.prospectionHoleGroupExporterService = prospectionHoleGroupExporterService;
        }

        [HttpGet]
        [Route("api/clients/{clientId}/faces/{faceId}/phgs")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find(string clientId, string faceId, [FromUri] ProspectionHoleGroupFilter filter)
        {
            try
            {
                var faceGuid = Guid.Parse(faceId);
                var prospectionHoleGroups = prospectionHoleGroupService.find(faceGuid, filter);
                var total = prospectionHoleGroupService.findAmount(faceGuid, filter);
                var data = new { total = total, prospectionHoleGroups = prospectionHoleGroups };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch(FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, "Incorrect parameters", ex.Message);
            }
        }

        [HttpGet]
        [Route("api/clients/{clientId}/faces/{faceId}/phgscalc")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findCalculations(string clientId, string faceId)
        {
            try
            {
                var faceGuid = Guid.Parse(faceId);
                var forecastCalculations = prospectionHoleGroupService.getCalculations(faceGuid);
                var data = new { forecastCalculations = forecastCalculations };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, "Incorrect parameters", ex.Message);
            }
        }

        [HttpGet]
        [Route("api/clients/{clientId}/faces/{faceId}/phgexcel")]
        public HttpResponseMessage excel(string clientId, Guid faceId)
        {
            var memoryStream = prospectionHoleGroupExporterService.toExcel(faceId);
            var response = new HttpResponseMessage(HttpStatusCode.OK);

            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = "forecast.xlsx";

            return response;
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode code, string message, string description)
        {
            var data = new { msg = message, description = description };
            return Request.CreateResponse(code, data);
        }
    }
}
