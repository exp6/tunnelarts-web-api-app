﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using System.Web.Http;
using TunelAPI.Filters;
using TunelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.tunnel;
using System.Web;
using TunnelAPI.services.services.tunnel.aggregator;
using System.Collections.Generic;
using TunnelAPI.services.services.mapping;
using TunnelAPI.services.services.face;
using AutoMapper;
using TunelAPI.repository;
using TunnelAPI.services.services.tunnel.aggregator.export;
using TunnelAPI.services.mapper;
using TunnelAPI.repository.model;
using TunnelAPI.services.services.prospection.aggregator;
using TunnelAPI.services.services.prospection.aggregator.exporter;
using TunnelAPI.services.services.prospection;

namespace TunelAPI.Controllers
{       
    [RoutePrefix("api")]
    public class TunnelsController : ApiController
    {
        private readonly ITunnelService tunnelService;
        private readonly IMappingService mappingService;
        private readonly IFaceService faceService;
        private readonly IProspectionHoleGroupService prospectionHoleGroupService;

        public TunnelsController(ITunnelService tunnelService, IFaceService faceService, IMappingService mappingService, IProspectionHoleGroupService prospectionHoleGroupService)
        {
            this.tunnelService = tunnelService;
            this.faceService = faceService;
            this.mappingService = mappingService;
            this.prospectionHoleGroupService = prospectionHoleGroupService;
        }

        [HttpGet]
        [Route("tunnels")]
        [AuthenticationFilter(true)]     
        public HttpResponseMessage find([FromUri] TunnelFilter filter)
        {            
            if (filter == null)
                filter = new TunnelFilter();                              

            var tunnels = tunnelService.find(filter);
            var total = tunnelService.findAmount(filter);

            var data = new { total = total, tunnels = tunnels };
            return Request.CreateResponse(HttpStatusCode.OK, data);            
        }

        [HttpGet]
        [Route("subscription/progress/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findProgress(string tunnelId)
        {
            var progress = tunnelService.findTunnelProgress(Guid.Parse(tunnelId));
            var data = new { progress = progress };
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }

        [HttpPost]
        [Route("tunnels")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage create(TunnelDTO tunnel)
        {
            var identity = User.Identity as BasicAuthenticationIdentity;
            tunnel.CreatedBy = identity.username;
            tunnel.id = Guid.NewGuid();
            tunnelService.create(tunnel);

            return findOne(tunnel.id);
        }

        [HttpPut]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(Guid tunnelId, TunnelDTO tunnel)
        {
            tunnel.id = tunnelId;
            tunnelService.update(tunnel);

            return findOne(tunnelId);
        }

        [HttpGet]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(Guid tunnelId)
        {
            var tunnel = tunnelService.findOne(tunnelId);
            return Request.CreateResponse(HttpStatusCode.OK, tunnel);
        }

        [HttpDelete]
        [Route("tunnels/{tunnelId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage remove(Guid tunnelId)
        {
            try { 
                tunnelService.remove(tunnelId);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (InvalidOperationException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.Conflict, ex.Message);
            }
        }

        //upload-rocktype-offer
        [System.Web.Http.HttpPost]
        [Route("tunnels/{tunnelId}/upload-rocktype-offer")]
        [AuthenticationFilter(true)]
        public async Task<IHttpActionResult> Upload(Guid tunnelId)
        {
            bool success = false;

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return Ok(generateBadRequestMessage(HttpStatusCode.InternalServerError, ""));
            }

            var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());
            await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(task =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    throw task.Exception;
                }
                
                foreach (var fileData in provider.FileData)
                {
                    using (var fs = File.OpenRead(fileData.LocalFileName))
                    {
                        success = tunnelService.saveOfferRockTypes(tunnelId, fs);
                    }

                    File.Delete(fileData.LocalFileName);
                }
            });
            
            if (success) return Ok();
            else return Ok(generateBadRequestMessage(HttpStatusCode.InternalServerError, ""));
        }

        [System.Web.Http.HttpGet]
        [Route("tunnels/offer-template")]
        public HttpResponseMessage excel()
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);

            string templatePath = @"~\scripts\templates\offer_file_template.xlsx";
            byte[] fileBytes = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath(templatePath));
            Stream stream = new MemoryStream(fileBytes);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = "offer_file_template.xlsx";

            return response;
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode httpCode, string message)
        {
            var data = new { msg = message };
            return Request.CreateResponse(httpCode, data);
        }

        [System.Web.Http.HttpGet]
        [Route("tunnels/{tunnelId}/rock-type-statistics/{typeOfData:regex(^(offer|real)$)}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage getRockTypeStatistics(Guid tunnelId, string typeOfData) {
            IRockTypeAggregator rockTypeAggregator = createRockTypeAggregator(tunnelId, typeOfData);
            string exportedData = JsonRockTypeExporter.serialize(rockTypeAggregator);
            return CreateJSONResponse(exportedData);
        }

        private IRockTypeAggregator createRockTypeAggregator(Guid tunnelId, string type) {
            InitializeMapper();

            switch (type) {
                case "offer":
                    return aggregateOfferRockType(tunnelId);
                case "real":
                    return aggregateRealRockType(tunnelId);
                default:
                    throw new ArgumentException("El tipo " + type + " no es un tipo válido de agregador.");
            }
        }

        private void InitializeMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new AutoMapperConfiguration());
            });
        }

        private IRockTypeAggregator aggregateOfferRockType(Guid tunnelId) {
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            ICollection<RockQualityDTO> rockQualitiesDTO = findRockQualityOfProject(tunnelDTO.Project);

            Tunnel tunnel = Mapper.Map<TunnelDTO, Tunnel>(tunnelDTO);
            ICollection<RockQuality> rockQualities = Mapper.Map<ICollection<RockQualityDTO>, ICollection<RockQuality>>(rockQualitiesDTO);
            return new OfferRockTypeAggregator(tunnel, rockQualities);
        }

        private ICollection<RockQualityDTO> findRockQualityOfProject(ProjectDTO project) {
            return project.RockQualities;
        }

        private IRockTypeAggregator aggregateRealRockType(Guid tunnelId) {
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            ICollection<RockQualityDTO> rockQualitiesDTO = findRockQualityOfProject(tunnelDTO.Project);
            ICollection<QValueDTO> qValuesDTO = createQValuesDTO(tunnelId);

            ICollection<RockQuality> rockQualities = Mapper.Map<ICollection<RockQualityDTO>, ICollection<RockQuality>>(rockQualitiesDTO);
            ICollection<QValue> qValues = Mapper.Map<ICollection<QValueDTO>, ICollection<QValue>>(qValuesDTO);

            return new RealRockTypeAggregator(rockQualities, qValues);
        }

        private ICollection<QValueDTO> createQValuesDTO(Guid tunnelId) {
            ICollection<FaceDTO> facesDTO = findAllFacesFromTunnel(tunnelId);
            return findAllQValuesOfFaces(facesDTO);
        }

        private ICollection<FaceDTO> findAllFacesFromTunnel(Guid tunnelId) {
            return faceService.find(new FaceFilter()
            {
                tunnelId = tunnelId,
                range = 0
            });
        }

        private ICollection<QValueDTO> findAllQValuesOfFaces(ICollection<FaceDTO> facesDTO) {
            ICollection<QValueDTO> qValuesDTO = new List<QValueDTO>();
            foreach (FaceDTO faceDTO in facesDTO)
            {
                IEnumerator<MappingDTO> mappingEnumerator = getEnumeratorOfMappingsForFace(faceDTO);
                while (mappingEnumerator.MoveNext())
                {
                    QValueDTO currentQValue = getQValueFromMappingDTO(mappingEnumerator.Current);
                    if (currentQValue == null) {
                        continue;
                    }
                    qValuesDTO.Add(currentQValue);
                }
            }
            return qValuesDTO;
        }

        private IEnumerator<MappingDTO> getEnumeratorOfMappingsForFace(FaceDTO faceDTO) {
            IEnumerable<MappingDTO> mappingsOfFace = findAllMappingsOfFace(faceDTO); mappingService.find(faceDTO.id, new MappingFilter()
            {
                range = 0
            });
            return mappingsOfFace.GetEnumerator();
        }

        private IEnumerable<MappingDTO> findAllMappingsOfFace(FaceDTO faceDTO) {
            return mappingService.find(faceDTO.id, new MappingFilter()
            {
                range = 0
            });
        }

        private QValueDTO getQValueFromMappingDTO(MappingDTO mappingDTO) {
            ICollection<QValueDTO> qValuesDTO = mappingDTO.QValues;
            IEnumerator<QValueDTO> qValueDTOEnumerator = qValuesDTO.GetEnumerator();
            qValueDTOEnumerator.MoveNext();
            return qValueDTOEnumerator.Current;
        }

        private HttpResponseMessage CreateJSONResponse(string jsonString) {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonString)
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return response;
        }
        
        [System.Web.Http.HttpGet]
        [Route("tunnels/{tunnelId}/rock-performance-statistics/{typeOfData:regex(^(offer-offer|real-(offer|real))$)}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage getRockPerformanceStatistics(Guid tunnelId, string typeOfData)
        {
            IPerformanceAggregator performanceAggregator = createPerformanceAggregator(tunnelId, typeOfData);
            string exportedData = JsonPerformanceExporter.serialize(performanceAggregator);
            return CreateJSONResponse(exportedData);
        }

        private IPerformanceAggregator createPerformanceAggregator(Guid tunnelId, string typeOfData) {
            InitializeMapper();

            switch (typeOfData) {
                case "offer-offer":
                    return aggregateOfferOfferPerformance(tunnelId);
                case "real-offer":
                    return aggregateRealOfferPerformance(tunnelId);
                case "real-real":
                    return aggregateRealRealPerformance(tunnelId);
                default:
                    throw new ArgumentException("El tipo " + typeOfData + " no es un tipo válido de agregador.");
            }
        }

        private IPerformanceAggregator aggregateOfferOfferPerformance(Guid tunnelId) {
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            ProjectDTO projectDTO = tunnelDTO.Project;

            Project project = Mapper.Map<ProjectDTO, Project>(projectDTO);
            Tunnel tunnel = Mapper.Map<TunnelDTO, Tunnel>(tunnelDTO);

            return new PerformanceOfferOfferAggregator(project, tunnel);
        }

        private IPerformanceAggregator aggregateRealOfferPerformance(Guid tunnelId) {
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            ProjectDTO projectDTO = tunnelDTO.Project;
            ICollection<MappingDTO> mappingsDTO = findAllMappingsOfTunnel(tunnelDTO);

            Project project = Mapper.Map<ProjectDTO, Project>(projectDTO);
            ICollection<Mapping> mappings = Mapper.Map<ICollection<MappingDTO>, ICollection<Mapping>>(mappingsDTO);

            return new PerformanceRealOfferAggregator(project, mappings);
        }

        private ICollection<MappingDTO> findAllMappingsOfTunnel(TunnelDTO tunnelDTO) {
            ICollection<FaceDTO> facesDTO = findAllFacesFromTunnel(tunnelDTO.id);
            ICollection<MappingDTO> mappingsReturn = new List<MappingDTO>();
            foreach (FaceDTO faceDTO in facesDTO) {
                IEnumerable<MappingDTO> mappingsDTO = findAllMappingsOfFace(faceDTO);
                IEnumerator<MappingDTO> mappingsDTOEnumerator = mappingsDTO.GetEnumerator();
                while (mappingsDTOEnumerator.MoveNext()) {
                    mappingsReturn.Add(mappingsDTOEnumerator.Current);
                }
            }
            return mappingsReturn;
        }

        private IPerformanceAggregator aggregateRealRealPerformance(Guid tunnelId)
        {
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            ICollection<MappingDTO> mappingsDTO = findAllMappingsOfTunnel(tunnelDTO);

            ICollection<Mapping> mappings = Mapper.Map<ICollection<MappingDTO>, ICollection<Mapping>>(mappingsDTO);

            return new PerformanceRealRealAggregator(mappings);
        }

        [System.Web.Http.HttpGet]
        [Route("tunnels/{tunnelId}/prospection-statistics")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage getProspectionStatistics(Guid tunnelId)
        {
            InitializeMapper();
            Tunnel tunnel = createTunnel(tunnelId);
            ProspectionAggregator prospectionAggregator = new ProspectionAggregator(tunnel);
            string exportedData = JsonProspectionExporter.serialize(prospectionAggregator);
            return CreateJSONResponse(exportedData);
        }

        private Tunnel createTunnel(Guid tunnelId) {
            TunnelLoader tunnelBuilder = createTunnelBuilder();
            TunnelDTO tunnelDTO = tunnelService.findOne(tunnelId);
            Tunnel tunnel = Mapper.Map<TunnelDTO, Tunnel>(tunnelDTO);
            return tunnelBuilder.buildTunnel(tunnel);
        }

        private TunnelLoader createTunnelBuilder() {
            TunnelLoaderBuilder tunnelLoaderBuilder = new TunnelLoaderBuilder() {
                mapper = Mapper.Instance,
                prospectionHoleGroupService = prospectionHoleGroupService,
                mappingService = mappingService,
                faceService = faceService
            };
            return tunnelLoaderBuilder.buildTunnelLoader();
        }

        [System.Web.Http.HttpGet]
        [Route("tunnels/{tunnelId}/aggregation-statistics")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage aggregateTunnelStatistics(Guid tunnelId)
        {
            InitializeMapper();
            Tunnel tunnel = createTunnel(tunnelId);
            StatisticsAggregator statisticsAggregator = buildStatisticsAggregator(tunnel);
            JsonStatisticsExporter exporter = new JsonStatisticsExporter(statisticsAggregator);
            string exportedData = exporter.serialize();
            return CreateJSONResponse(exportedData);
        }

        private StatisticsAggregator buildStatisticsAggregator(Tunnel tunnel) {
            StatisticsAggregatorBuilder builder = new StatisticsAggregatorBuilder()
            {
                OfferRockType = aggregateOfferRockType(tunnel.id),
                RealRockType = aggregateRealRockType(tunnel.id),
                OfferOfferPerformance = aggregateOfferOfferPerformance(tunnel.id),
                RealOfferPerformance = aggregateRealOfferPerformance(tunnel.id),
                RealRealPerformance = aggregateRealRealPerformance(tunnel.id),
                Prospection = new ProspectionAggregator(tunnel)
            };
            return builder.build();
        }
    }
}
