﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunnelAPI.services.services.esr;

namespace TunelAPI.Controllers
{    
    [RoutePrefix("api")]    
    public class EsrsController : ApiController
    {
        private readonly IEsrService esrService;

        public EsrsController(IEsrService esrService)
        {
            this.esrService = esrService;
        }

        [HttpGet]
        [Route("esrs")]
        [AuthenticationFilter(true)]        
        public HttpResponseMessage find()
        {
            var esrs = esrService.find();                
            var data = new { esrs = esrs };
            return Request.CreateResponse(HttpStatusCode.OK, data);            
        }        
    }
}
