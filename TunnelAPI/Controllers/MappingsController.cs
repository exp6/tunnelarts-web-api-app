﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TunelAPI.Filters;
using TunnelAPI.repository.model;
using TunnelAPI.services.model;
using TunnelAPI.services.services.mapping;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.Text;
using System.Web.Mvc;
using TunnelAPI.Models;
using TunnelAPI.Controllers;

namespace TunelAPI.Controllers
{    
    public class MappingsController : ApiController
    {
        private readonly IMappingService mappingService;
        private readonly IMappingExporterService mappingExporterService;

        public MappingsController(IMappingService mappingService, IMappingExporterService mappingExporterService)
        {
            this.mappingService = mappingService;
            this.mappingExporterService = mappingExporterService;
        }

        [System.Web.Http.HttpGet]
        [Route("api/clients/{clientId}/faces/{faceId}/mappings")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage find(string clientId, string faceId, [FromUri] MappingFilter mappingFilter)
        {
            try
            {
                var mappingGuid = Guid.Parse(faceId);

                var mappings = mappingService.find(mappingGuid, mappingFilter);
                var total = mappingService.findAmount(mappingGuid, mappingFilter);

                var data = new { total = total, mappings = mappings };
                return Request.CreateResponse(HttpStatusCode.OK, data);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, "Incorrect parameters", ex.Message);
            }
        }

        [System.Web.Http.HttpPut]
        [Route("api/clients/{clientId}/mappings/{mappingId}/status")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage changeStatus(string clientId, Guid mappingId, MappingDTO mapping)
        {
            try
            {
                mapping.id = mappingId;
                mappingService.changeStatus(mapping);

                return findOne(clientId, mappingId);
            }
            catch (FormatException ex)
            {
                return generateBadRequestMessage(HttpStatusCode.BadRequest, "Incorrect parameters", ex.Message);
            }            
        }


        [System.Web.Http.HttpPut]
        [Route("api/clients/{clientId}/mappings/{mappingId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage update(string clientId, Guid mappingId, MappingDTO mapping)
        {
            mapping.id = mappingId;
            mappingService.update(mapping);

            return findOne(clientId, mappingId);                        
        }

        [System.Web.Http.HttpGet]
        [Route("api/clients/{clientId}/mappings/{mappingId}")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findOne(string clientId, Guid mappingId)
        {
            var mapping = mappingService.findOne(mappingId);
            return Request.CreateResponse(HttpStatusCode.OK, mapping);
        }

        [System.Web.Http.HttpGet]
        [Route("api/clients/{clientId}/mappings/{mappingId}/pdf")]
        public HttpResponse pdf(string clientId, Guid mappingId)
        {
            HttpResponse response = HttpContext.Current.Response;
            MappingReportController _Report = new MappingReportController();

            MappingReportViewModel oModel = new MappingReportViewModel();

            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.Buffer = true;
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment;filename=xyz.pdf");
            response.BinaryWrite(_Report.GetPdfBytesFormView(oModel));
            response.End();

            return response;
        }
        
        [System.Web.Http.HttpGet]
        [Route("api/clients/{clientId}/mappings/{mappingId}/excel")]
        public HttpResponseMessage excel(string clientId, Guid mappingId)
        {
            var memoryStream = mappingExporterService.toExcel(mappingId);        
            var response = new HttpResponseMessage(HttpStatusCode.OK);
                        
            response.Content = new StreamContent(memoryStream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = "mapping.xlsx";
            
            return response;
        }

        [System.Web.Http.HttpPost]
        [Route("api/clients/{clientId}/mappings/{mappingId}/external-picture")]
        [AuthenticationFilter(true)]
        public async Task<IHttpActionResult> Upload(string clientId, Guid mappingId, int sideCode)
        {
            bool success = false;

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return Ok(generateBadRequestMessage(HttpStatusCode.InternalServerError, "", ""));
            }

            var provider = new MultipartFormDataStreamProvider(Path.GetTempPath());
            await Request.Content.ReadAsMultipartAsync(provider).ContinueWith(task =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    throw task.Exception;
                }

                string sideName;
                string extension = ".jpg";

                switch (sideCode)
                {
                    case 1:
                        sideName = "left-wall";
                        break;
                    case 2:
                        sideName = "right-wall";
                        break;
                    case 3:
                        sideName = "roof";
                        break;
                    default:
                        sideName = "face";
                        break;
                }

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                foreach (var fileData in provider.FileData)
                {
                    CloudBlobContainer container = blobClient.GetContainerReference(mappingId.ToString().ToLower());
                    container.CreateIfNotExists();

                    container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                    var blob = container.GetBlockBlobReference(sideName + extension);
                    blob.Properties.ContentType = fileData.Headers.ContentType.MediaType;
                    var blockBlobOriginal = container.GetBlockBlobReference(sideName + "-original" + extension);
                    blockBlobOriginal.Properties.ContentType = fileData.Headers.ContentType.MediaType;

                    using (var fs = File.OpenRead(fileData.LocalFileName))
                    {
                        blob.UploadFromStream(fs);
                    }

                    using (var fs = File.OpenRead(fileData.LocalFileName))
                    {
                        blockBlobOriginal.UploadFromStream(fs);
                    }

                    File.Delete(fileData.LocalFileName);

                    //Persist new data on DB
                    string remoteUri = blob.StorageUri.PrimaryUri.ToString();
                    string remoteUriOriginal = blockBlobOriginal.StorageUri.PrimaryUri.ToString();

                    mappingService.updatePicturesRemoteUri(mappingId, sideCode, remoteUri, remoteUriOriginal);

                    success = true;
                }
            });

           
            if(success) return Ok();
            else return Ok(generateBadRequestMessage(HttpStatusCode.InternalServerError, "", ""));
        }

        [System.Web.Http.HttpGet]
        [Route("api/clients/{clientId}/mappings/{mappingId}/pictures")]
        [AuthenticationFilter(true)]
        public HttpResponseMessage findPictures(string clientId, Guid mappingId)
        {
            var pictures = mappingService.findPictures(mappingId);
            return Request.CreateResponse(HttpStatusCode.OK, pictures);
        }

        private HttpResponseMessage generateBadRequestMessage(HttpStatusCode code, string message, string description)
        {
            var data = new { msg = message, description = description };
            return Request.CreateResponse(code, data);
        }


    }
}
