﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using TunelAPI.Filters;
using TunnelAPI.services.services.role;

namespace TunelAPI.Controllers
{    
    [RoutePrefix("api")]    
    public class RolesController : ApiController
    {
        private readonly IRoleService roleService;

        public RolesController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

        [HttpGet]
        [Route("roles")]
        [AuthenticationFilter(true)]        
        public HttpResponseMessage find()
        {
            var roles = roleService.find();                
            var data = new { roles = roles };
            return Request.CreateResponse(HttpStatusCode.OK, data);            
        }        
    }
}
