using Microsoft.Practices.Unity;
using TunnelAPI.services.services.user;
using TunnelAPI.services.services.security;
using TunnelAPI.services.services.project;
using TunnelAPI.services.services.tunnel;
using TunnelAPI.services.services.face;
using TunnelAPI.services.services.mapping;
using TunnelAPI.services.services.prospection;
using System.Web.Http;
using TunelAPI.repository;
using TunnelAPI.repository.mapping;
using AutoMapper;
using TunnelAPI.services.mapper;
using TunnelAPI.repository.user;
using TunnelAPI.repository.project;
using TunnelAPI.repository.tunnel;
using TunnelAPI.services.services.esr;
using TunnelAPI.repository.esr;
using TunnelAPI.repository.role;
using TunnelAPI.repository.face;
using TunelAPI.repository.repository.prospectionholegroup;
using TunnelAPI.services.services.role;
using TunnelAPI.services.services.tunnelprofile;
using TunelAPI.repository.repository.tunnelprofile;

namespace TunelAPI.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    static public class UnityConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var AutomapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperConfiguration());
            });

            var container = new UnityContainer();
            // Services
            container.RegisterType<IUserService, UserService>(new HierarchicalLifetimeManager())
                .RegisterType<IMembershipService, MembershipService>(new HierarchicalLifetimeManager())
                .RegisterType<IProjectService, ProjectService>(new HierarchicalLifetimeManager())
                .RegisterType<ITunnelService, TunnelService>(new HierarchicalLifetimeManager())
                .RegisterType<IFaceService, FaceService>(new HierarchicalLifetimeManager())
                .RegisterType<IMappingService, MappingService>(new HierarchicalLifetimeManager())
                .RegisterType<ICharacterizationService, CharacterizationService>(new HierarchicalLifetimeManager())
                .RegisterType<IEsrService, EsrService>(new HierarchicalLifetimeManager())
                .RegisterType<IRoleService, RoleService>(new HierarchicalLifetimeManager())
                .RegisterType<IMappingExporterService, MappingExporterService>(new HierarchicalLifetimeManager())
                .RegisterType<IProspectionHoleGroupService, ProspectionHoleGroupService>(new HierarchicalLifetimeManager())
                .RegisterType<IProspectionHoleGroupExporterService, ProspectionHoleGroupExporterService>(new HierarchicalLifetimeManager())
                .RegisterType<ITunnelProfileService, TunnelProfileService>(new HierarchicalLifetimeManager())
            // Repositories
                .RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IMappingRepository, MappingRepository>(new HierarchicalLifetimeManager())
                .RegisterType<ICharacterizationRepository, CharacterizationRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IProjectRepository, ProjectRepository>(new HierarchicalLifetimeManager())
                .RegisterType<ITunnelRepository, TunnelRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IFaceRepository, FaceRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IEsrRepository, EsrRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IRoleRepository, RoleRepository>(new HierarchicalLifetimeManager())
                .RegisterType<IProspectionHoleGroupRepository, ProspectionHoleGroupRepository>(new HierarchicalLifetimeManager())
                .RegisterType<ITunnelProfileRepository, TunnelProfileRepository>(new HierarchicalLifetimeManager())
                .RegisterType<TunnelArtsEntities>(new HierarchicalLifetimeManager())                
                .RegisterInstance<IMapper>(AutomapperConfig.CreateMapper()); ;

            config.DependencyResolver = new UnityResolver(container);

        }
    }
}
