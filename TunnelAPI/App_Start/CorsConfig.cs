﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace TunelAPI { 
    static public class CorsConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
        }
    }
}