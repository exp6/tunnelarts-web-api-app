﻿using System.Web.Http;

namespace TunelAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            CorsConfig.Register(config);
            RouteConfig.Register(config);
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}
